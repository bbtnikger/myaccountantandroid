function func() {

}

function resetAccounts(){

    clearAccounts();
    setupAccounts();
}

function clearAccounts(){

    listModel.clear();
}

function processAccount(record_){

    record_.accounttypename = record_.account_type.name;

    listModel.append(record_);
}

function setupAccounts(){

    var profile_id = ProfileUtility.currentProfile;
    var list = AccountUtility.getAccounts(profile_id);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];

        processAccount(record_);
    }
}
