import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "PayeesView.js" as PayeesViewScript
import "dialogs_forms"

Item {
    id: root

    PayeeDialog{
        id: payeeDialog
        visible: false
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    RowLayout{
        anchors.fill: parent
        spacing: 1

        PayeesViewList{
            id: payeesViewList
            Layout.minimumWidth: 200
//            Layout.maximumWidth: 300
//            Layout.preferredWidth: 200
            Layout.fillWidth: true
            Layout.fillHeight: true
//            onCurrentpayeeindexChanged: payeesViewDashboard.payeedata = model.get(currentpayeeindex)
            onAddPayee: PayeesViewScript.startAddPayeeDialog()
            onEditPayee: PayeesViewScript.startUpdatePayeeDialog();
            onDeletePayee: PayeesViewScript.startDeletePayee()
        }

//        PayeesViewDashboard{
//            id: payeesViewDashboard
//            anchors.top: parent.top
//            anchors.bottom: parent.bottom
//            anchors.topMargin: 1
//            anchors.bottomMargin: 1
//            Layout.fillWidth: true
//        }
    }
}

