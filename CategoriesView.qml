import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "CategoriesView.js" as CategoriesViewScript
import "dialogs_forms"

Item {
    id: root

    property variant currentcategorylistitem
    property alias currentcategoryindex: grid.currentIndex

    function resetPage(){

        CategoriesViewScript.resetCategories();
    }

    CategoryDialog{
        id: categoryDialog
    }

    ExpenseIncomeDialog{
        id: expenseIncomeDialog
    }

    ListModel{
        id: listModel
    }

    Component {
        id: categoryDelegate

        Item {
            width: grid.cellWidth; height: grid.cellHeight

            Rectangle {
                anchors.fill: parent
                anchors.margins: 12
                color: "#000000FF"
                border.color: "dimgrey"
                radius: 6

                ColumnLayout{
                    anchors.fill: parent
                    anchors.margins: 3

                    Item{
                        Layout.fillHeight: true
                        anchors.left: parent.left
                        anchors.right: parent.right

                        Label {
                            text: name;
                            font.pointSize: 12;
                            anchors.centerIn: parent
                        }
                    }

//                    RowLayout{
//                        anchors.left: parent.left
//                        anchors.right: parent.right
//                        spacing: 6

//                        Item{
//                            Layout.fillWidth: true
//                        }

//                        Image {
//                            id: imgstartexpenseincome
//                            fillMode: Image.PreserveAspectFit
//                            anchors.verticalCenter: parent.verticalCenter
//                            source: "qrc:/images/gla_expensesincome_inverted -24x24.png"
//                            opacity: mareastartexpenseincome.pressed? 0.6 : 1.0


//                            MouseArea{
//                                id: mareastartexpenseincome
//                                anchors.fill: parent
//                                onClicked: { grid.currentIndex = index; grid.startExpenseIncomeProcess(); }
//                            }
//                        }

//                        Image {
//                            id: img
//                            fillMode: Image.PreserveAspectFit
//                            anchors.verticalCenter: parent.verticalCenter
//                            source: "qrc:/images/ic_edit - 24x24.png"
//                            opacity: marea.pressed? 0.6 : 1.0


//                            MouseArea{
//                                id: marea
//                                anchors.fill: parent
//                                onClicked: { grid.currentIndex = index; grid.startEditProcess(); }
//                            }
//                        }

//                        Image {
//                            id: imgdel
//                            fillMode: Image.PreserveAspectFit
//                            anchors.verticalCenter: parent.verticalCenter
//                            source: "qrc:/images/ic_delete - 24x24.png"
//                            opacity: mareadel.pressed? 0.6 : 1.0


//                            MouseArea{
//                                id: mareadel
//                                anchors.fill: parent
//                                onClicked: { grid.currentIndex = index; grid.startDeleteProcess(); }
//                            }
//                        }

//                        Item{
//                            Layout.fillWidth: true
//                        }
//                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: { grid.currentIndex = index; grid.startExpenseIncomeProcess(); }
                    onPressAndHold: { grid.currentIndex = index; menu.popup(); }
                }
            }
        }
    }

    Component{
        id: highlightDelegate
        Item{
            width: grid.cellWidth; height: grid.cellHeight
            x: grid.currentItem.x
            y: grid.currentItem.y
            Rectangle {
                anchors.fill: parent
                anchors.margins: 13
                radius: 6
                color: "lightsteelblue"
            }
        }
    }

    Menu{
        id: menu
        MenuItem{
            text: qsTr("Edit")
            onTriggered: grid.startEditProcess()
        }
        MenuItem{
            text: qsTr("Delete")
            onTriggered: grid.startDeleteProcess()
        }
    }

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 1
        spacing: 6

        GridView{
            id: grid
            model: listModel
            Layout.fillHeight: true
            anchors.left: parent.left
            anchors.right: parent.right
            cellWidth: 200;
            cellHeight: 200
            delegate: categoryDelegate
            highlight: highlightDelegate
            highlightMoveDuration: 0

            onCurrentIndexChanged: {

                currentcategorylistitem = listModel.get(currentIndex);
            }

            function startEditProcess(){

                CategoriesViewScript.startUpdateCategoryDialog();
            }

            function startDeleteProcess(){

                CategoriesViewScript.startDeleteCategory();
            }

            function startExpenseIncomeProcess(){

                CategoriesViewScript.startExpenseIncomeDialog();
            }
        }

        ToolBar {
            id: toolbar
            anchors.left: parent.left
            anchors.right: parent.right
            RowLayout {
                anchors.fill: parent
                ToolButton {
                    tooltip: qsTr("Add Category")
                    iconSource: "qrc:/images/ic_add_64x64.png"
                    onClicked: CategoriesViewScript.startAddCategoryDialog()
                }
            }
        }
    }

    Component.onCompleted: CategoriesViewScript.resetCategories()
}

