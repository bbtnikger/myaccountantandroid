import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import org.qtproject.example 1.0

Item {
    id: root
    width: 100
    height: 62

    signal addTransaction()
    signal editTransaction()
    signal deleteTransaction()
    signal openTransaction()
    signal openDetails()

    property variant currenthistorylistitem
//    property alias currenthistoryindex: tableView.currentRow
    property alias currenthistoryindex: listView.currentIndex
    property alias model: listModel
    property alias proxyModel: proxyModel
//    property alias tableView: tableView
    property alias listView: listView

    ListModel{
        id: listModel
    }

    SortFilterProxyModel {
        id: proxyModel
        source: listModel.count > 0 ? listModel : null

        sortOrder: Qt.AscendingOrder
        sortCaseSensitivity: Qt.CaseInsensitive
//                sortRole: historyModel.count > 0 ? tableView.getColumn(tableView.sortIndicatorColumn).role : ""
        sortRole: listModel.count > 0 ? "date" : ""

//                filterString: "*" + searchBox.text + "*"
//                filterSyntax: SortFilterProxyModel.Wildcard
//                filterCaseSensitivity: Qt.CaseInsensitive
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    Component{
        id: itemDelegate
        Item{
            width: listView.width
            height: 100

            Rectangle{
                anchors.fill: parent
                color: "#000000FF"
                border.color: "grey"
            }

            ColumnLayout{
                anchors.fill: parent
                spacing: 3

                RowLayout{
                    Layout.preferredHeight: parent.height / 1.7
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.leftMargin: 3
                    anchors.rightMargin: 3

                    Label{
                        anchors.verticalCenter: parent.verticalCenter
                        text: accountname
                        Layout.fillWidth: true
                        font.pointSize: 15
                    }

                    Label{
                        anchors.bottom: parent.bottom
                        horizontalAlignment: Qt.AlignRight
                        text: type
                        font.pointSize: 11
                    }
                }

                Label{
                    text: datename + ',' +  amounttext + ',' + description
                    font.pointSize: 13
                }
            }

            MouseArea{
                anchors.fill: parent
                onPressAndHold: { listView.currentIndex = index; listView.startMenu(); }
                onClicked: { listView.currentIndex = index; listView.openDetailsPage(); }
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: listView.width-2; height: 100
            y: listView.currentItem.y
            x: 1
            color: "lightsteelblue";
        }
    }

    Component {
        id: headerComponent

        Item {
            width: listView.width
            height: 40
            y: ListView.view.y-height

            Label{
                text: qsTr("Account History")
                font.pointSize: 13
                font.bold: true
                font.italic: true
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 12

            }
        }
    }

    Menu{
        id: menu
        MenuItem{
            text: qsTr("Edit")
            onTriggered: listView.startEditProcess()
        }
        MenuItem{
            text: qsTr("Delete")
            onTriggered: listView.startDeleteProcess()
        }
    }

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 6

//        ToolBar {

//            anchors.left: parent.left
//            anchors.right: parent.right

//            TextField {
//                id: searchBox
//                anchors.left: parent.left
//                anchors.right: parent.right
//                placeholderText: "Search..."
//                anchors.verticalCenter: parent.verticalCenter
//                inputMethodHints: Qt.ImhNoPredictiveText

//                //                anchors.verticalCenter: parent.verticalCenter
//            }
//        }

        ListView{
            id: listView
            anchors.left: parent.left
            anchors.right: parent.right
            Layout.fillHeight: true
            delegate: itemDelegate
            highlight: highlight
            header: headerComponent
            highlightFollowsCurrentItem: false
            focus: true
            onCountChanged: {

//                currenthistorylistitem = proxyModel.get(currentRow);
                currenthistorylistitem = proxyModel.get(currentIndex);
            }

            onCurrentIndexChanged: {

                currenthistorylistitem = proxyModel.get(currentIndex);
//                currenthistorylistitem = historyModel.get(currentRow);
            }

            function startEditProcess(){

                root.editTransaction();
            }

            function startDeleteProcess(){

                root.deleteTransaction();
            }

            function startOpenProcess(){

                root.openTransaction();
            }

            function openDetailsPage(){

                root.openDetails()
            }
        }

//        ToolBar {
//            id: toolbar
//            anchors.left: parent.left
//            anchors.right: parent.right
//            RowLayout {
//                anchors.fill: parent
//                ToolButton {
//                    tooltip: qsTr("Start Transaction")
//                    iconSource: "qrc:/images/ic_add_64x64.png"
//                    onClicked: addTransaction()
//                }
//            }
//        }
    }
}

