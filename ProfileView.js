function func() {

}

function onCurrentTabIndexChanged(currenttabindex){

    busyIndicator.running = true;

    loader.sourceComponent = undefined;

    if(currenttabindex === 0) { // accounts

        loader.source = "AccountsView.qml";

    }else if(currenttabindex === 1){ // categories

        loader.source = "CategoriesView.qml";

    }else if(currenttabindex === 2){ // history

        loader.source = "HistoryView.qml";

    }else if(currenttabindex === 3){ // summaries

        loader.source = "SummariesView.qml";

    }else if(currenttabindex === 4){ // manage types

        loader.source = "ManageTypesView.qml";

    }else if(currenttabindex === 5){ // tools

        loader.source = "ToolsView.qml";
    }

    busyIndicator.running = false;
}
