import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "AccountsView.js" as AccountsViewScript
import "dialogs_forms"

Item {
    id: root

    function resetPage(){

        accountsViewList.resetPage();
    }

    AccountDialog{
        id: accountDialog
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    Component{
        id: accountsViewDashboardComponent
        AccountsViewDashboard{ }
    }

    RowLayout{
        anchors.fill: parent
        spacing: 1
        anchors.topMargin: 1
        anchors.bottomMargin: 1

        AccountsViewList{
            id: accountsViewList
//            Layout.minimumWidth: 300
//            Layout.maximumWidth: 400
//            Layout.preferredWidth: 300
            Layout.fillWidth: true
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            onAddAccount: AccountsViewScript.startAddAccountDialog()
            onEditAccount: AccountsViewScript.startUpdateAccountDialog()
            onDeleteAccount: AccountsViewScript.startDeleteAccount()
            onOpenDashboard: AccountsViewScript.startOpenDashboard();
        }

//        AccountsViewDashboard{
//            id: accountsViewDashboard
//            anchors.top: parent.top
//            anchors.bottom: parent.bottom
//            Layout.fillWidth: true
//            accountlistitem: accountsViewList.currentaccountlistitem
//        }
    }
}

