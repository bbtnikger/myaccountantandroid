function func() {

}

function onAddaccountTypeDialogAccepted(){

    busyIndicator.running = true;

    var accounttypedata = accountTypeDialog.getAccountTypeDetails();
    var profile_id = ProfileUtility.currentProfile;
    accounttypedata.profile_id = profile_id;
    var accounttypedatanew = AccountTypeUtility.addAccountType(accounttypedata);

    if(accounttypedatanew !== null){

        accountTypesViewList.model.append(accounttypedatanew);

        showInformationMessage(qsTr("Account Type added successfully"));
    }

    busyIndicator.running = false;
}

function onUpdateaccountTypeDialogAccepted(){

    busyIndicator.running = true;

    var accounttypedetails = accountTypeDialog.getAccountTypeDetails();
    var _id = accountTypesViewList.currentaccounttypelistitem._id;
    var accounttypedatanew = AccountTypeUtility.updateAccountType(_id,accounttypedetails);

    if(accounttypedatanew !== null){

        accountTypesViewList.model.set(accountTypesViewList.currentaccounttypeindex,accounttypedatanew);

        showInformationMessage(qsTr("Account Type updated successfully"));
    }

    busyIndicator.running = false;
}

function startAddAccountTypeDialog(){

    accountTypeDialog.contentheight = stackView.height - 100;
    accountTypeDialog.contentwidth = stackView.width - 100;
    accountTypeDialog.accounttypedata = undefined;
    accountTypeDialog.resetDefaults();
    accountTypeDialog.title = qsTr("New AccountType Dialog");
    accountTypeDialog.accepted.disconnect(onAddaccountTypeDialogAccepted);
    accountTypeDialog.accepted.disconnect(onUpdateaccountTypeDialogAccepted);
    accountTypeDialog.accepted.connect(onAddaccountTypeDialogAccepted);
    accountTypeDialog.open();
}

//var AccountTypeData = function(accounttypelistitem){

//    this.
//}

function startUpdateAccountTypeDialog(){

//    var accounttypedata = new AccountTypeData(accounttypesViewList.currentaccounttypelistitem);
//    accountTypeDialog.accounttypedata

    accountTypeDialog.contentheight = stackView.height-100;
    accountTypeDialog.contentwidth = stackView.width-100;
    accountTypeDialog.accounttypedata = accountTypesViewList.currentaccounttypelistitem
    accountTypeDialog.resetDefaults();
    accountTypeDialog.title = qsTr("Update AccountType Dialog");
    accountTypeDialog.accepted.disconnect(onAddaccountTypeDialogAccepted);
    accountTypeDialog.accepted.disconnect(onUpdateaccountTypeDialogAccepted);
    accountTypeDialog.accepted.connect(onUpdateaccountTypeDialogAccepted);
    accountTypeDialog.open();
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = accountTypesViewList.currentaccounttypelistitem;
    var _id = recorddata._id;
    var res = AccountTypeUtility.deleteAccountType(_id);

    if(res){

        accountTypesViewList.model.remove(accountTypesViewList.currentaccounttypeindex);

        showInformationMessage(qsTr("Account Type deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeleteAccountType(){

    var title_ = accountTypesViewList.currentaccounttypelistitem.name;
    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete ("+title_+'). Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.');
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}
