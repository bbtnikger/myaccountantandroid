function func() {

}

function resetCategories(){

    clearCategories();
    setupCategories();
}

function clearCategories(){

    listModel.clear();
}

function setupCategories(){

    var list = CategoryUtility.getCategories(ProfileUtility.currentProfile);
    if(list === undefined || list === null)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        listModel.append(record_);
    }
}

function onCurrentIndexChanged(currentIndex){

    console.log('[Account ListView]-Current Index('+currentIndex+')');

    currentcategorylistitem = listModel.get(currentIndex);
    currentcategoryindex = currentIndex;
}

function onAddCategoryDialogAccepted(){

    busyIndicator.running = true;

    var categorydata = categoryDialog.getCategoryDetails();
    var profile_id = ProfileUtility.currentProfile;
    categorydata.profile_id = profile_id;
    var categorydatanew = CategoryUtility.addCategory(categorydata);

    if(categorydatanew !== null){

        listModel.append(categorydatanew);

        showInformationMessage(qsTr("Category added successfully"));
    }

    busyIndicator.running = false;
}

function onUpdateCategoryDialogAccepted(){

    busyIndicator.running = true;

    var categorydetails = categoryDialog.getCategoryDetails();
    var _id = currentcategorylistitem._id;
    var categorydatanew = CategoryUtility.updateCategory(_id,categorydetails);

    if(categorydatanew !== null){

        listModel.set(currentcategoryindex,categorydatanew);

        showInformationMessage(qsTr("Category updated successfully"));
    }

    busyIndicator.running = false;
}

function startAddCategoryDialog(){

    categoryDialog.contentheight = stackView.height - 100;
    categoryDialog.contentwidth = stackView.width - 100;
    categoryDialog.categorydata = undefined;
    categoryDialog.resetDefaults();
    categoryDialog.title = qsTr("New Category Dialog");
    categoryDialog.accepted.disconnect(onAddCategoryDialogAccepted);
    categoryDialog.accepted.disconnect(onUpdateCategoryDialogAccepted);
    categoryDialog.accepted.connect(onAddCategoryDialogAccepted);
    categoryDialog.open();
}

function startUpdateCategoryDialog(){

    categoryDialog.contentheight = stackView.height - 100;
    categoryDialog.contentwidth = stackView.width - 100;
    categoryDialog.categorydata = currentcategorylistitem;
    categoryDialog.resetDefaults();
    categoryDialog.title = qsTr("Update Category Dialog");
    categoryDialog.accepted.disconnect(onAddCategoryDialogAccepted);
    categoryDialog.accepted.disconnect(onUpdateCategoryDialogAccepted);
    categoryDialog.accepted.connect(onUpdateCategoryDialogAccepted);
    categoryDialog.open();
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = currentcategorylistitem;
    var _id = recorddata._id;
    var res = CategoryUtility.deleteCategory(_id);

    if(res){

        listModel.remove(currentcategoryindex);

        showInformationMessage(qsTr("Category deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeleteCategory(){

    var title_ = currentcategorylistitem.name;
    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete ("+title_+'). Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.');
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}

function onAddExpenseIncomeDialogAccepted(){

    busyIndicator.running = true;

    var expenseincomedata = expenseIncomeDialog.getExpenseIncomeDetails();
    var profile_id = ProfileUtility.currentProfile;
    expenseincomedata.profile_id = profile_id;
    var eitype = expenseIncomeDialog.eitype;
    var expenseincomedatanew = null;
    if(eitype === 1){ // expense

        expenseincomedatanew = ExpenseUtility.addExpense(expenseincomedata);

    }else{ // income

        expenseincomedatanew = IncomeUtility.addIncome(expenseincomedata);

    }

    if(expenseincomedatanew !== null){

        showInformationMessage(qsTr("Transaction added successfully"));
    }

    busyIndicator.running = false;
}

function startExpenseIncomeDialog(){

    busyIndicator.running = true;

    expenseIncomeDialog.contentheight = stackView.height - 100;
    expenseIncomeDialog.contentwidth = stackView.width - 100;
    expenseIncomeDialog.resetDefaults();
    expenseIncomeDialog.selectCategory(currentcategorylistitem._id);
    expenseIncomeDialog.title = qsTr("Add Expense/Income Dialog");
    expenseIncomeDialog.accepted.disconnect(onAddExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.connect(onAddExpenseIncomeDialogAccepted);

//    stackView.push(expenseIncomeDialog);
    expenseIncomeDialog.open();

    busyIndicator.running = false;
}
