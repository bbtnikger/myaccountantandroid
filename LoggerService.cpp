/*
 * LoggerService.cpp
 *
 *  Created on: Jan 19, 2015
 *      Author: Nick
 */

#include "LoggerService.h"

#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include <QDateTime>
#include <QThread>
#include <QFuture>
#include <QtConcurrent/QtConcurrentRun>

#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

namespace LoggingService {

const QString logprepend_shutdown 	= QLatin1String("Log type: Shutdown, ");
const QString logprepend_critical 	= QLatin1String("Log type: Critical, ");
const QString logprepend_error 		= QLatin1String("Log type: Error, ");
const QString logprepend_warning 	= QLatin1String("Log type: Warning, ");
const QString logprepend_notice 	= QLatin1String("Log type: Notice, ");
const QString logprepend_info 		= QLatin1String("Log type: Info, ");
const QString logprepend_debug1 	= QLatin1String("Log type: Debug1, ");
const QString logprepend_debug2 	= QLatin1String("Log type: Debug2, ");


LoggerService* LoggerService::m_pInstance = NULL;

LoggerService* LoggerService::instance(){

	if(m_pInstance == NULL){

		m_pInstance = new LoggerService();
	}

	return m_pInstance;
}

void LoggerService::deleteInstance(){

	if(m_pInstance != NULL){

		delete m_pInstance;
		m_pInstance = NULL;
	}
}

LoggerService::LoggerService(QObject *parent)
	: QObject(parent) {
	// TODO Auto-generated constructor stub


    initialize();
}

LoggerService::~LoggerService() {
	// TODO Auto-generated destructor stub
}

void LoggerService::initialize(){

    QString appFolder = QDir::currentPath();
    QString databaseFileName = appFolder.append(QLatin1String("/logs/logging.db"));

    bool res = QFile::exists(databaseFileName);
    if(!res){

        qDebug() << "file does not exist, should close the app or not ? probably not";
        return;
    }

    Q_UNUSED(res);

    mDbNameWithPath = databaseFileName;
    qDebug() << "databaseFileName: " << databaseFileName;


	// Open the data base to enable edit/add/remove via SQL queries, using non default
	// connection to not conflict with the database connection set up by SqlDataAccess.
	if (QSqlDatabase::contains(QString::fromLatin1("logging_connection"))){

        m_db = QSqlDatabase::database(QLatin1String("logging_connection"));

	} else {

        m_db = QSqlDatabase::addDatabase(QLatin1String("QSQLITE"), QLatin1String("logging_connection"));

		m_db.setDatabaseName(mDbNameWithPath);

		if (!m_db.isValid()) {

            m_lastError = QLatin1String("Could not set data base name probably due to invalid driver.");
		}

		bool success = m_db.open();

		if (!success) {

            m_lastError = QLatin1String("Could not open database we are in trouble");

		} else {

			qDebug() << "Database opened successfully.., database path" << mDbNameWithPath;
		}
	}

	if(!clearOldMessages())
		qDebug() << "could not clear the old messages: " << m_lastError;
}

bool LoggerService::clearOldMessages(){

	QDateTime dt = QDateTime::currentDateTime();
	dt = dt.addDays(-3);
	QString lg_timestamp = dt.toString(Qt::ISODate);

	m_db.transaction();
	QSqlQuery query(m_db);
	query.prepare(QLatin1String("DELETE FROM logging WHERE datetime(lg_timestamp) < :lg_timestamp"));
	query.bindValue(QLatin1String(":lg_timestamp"),lg_timestamp);
	if(!query.exec()){

		m_lastError = query.lastError().text();
		m_db.rollback();
		return false;
	}

	m_db.commit();
	return true;
}

void insertLogMessageToDB(const QString &timestamp, const QString &msg, unsigned int lt, unsigned int code){

//	QDateTime dt = QDateTime::currentDateTime();
//	QString lg_timestamp = dt.toString(Qt::ISODate);

    QSqlDatabase db = QSqlDatabase::database(QLatin1String("logging_connection"));
    if(!db.transaction())
        return;

	QSqlQuery query(db);
	query.prepare(QLatin1String("INSERT INTO logging(lg_timestamp,lg_type,lg_message,lg_code) VALUES(:lg_timestamp,:lg_type,:lg_message,:lg_code)"));
    query.bindValue(QLatin1String(":lg_timestamp"),timestamp);
    query.bindValue(QLatin1String(":lg_type"),lt);
    query.bindValue(QLatin1String(":lg_message"),msg);
    query.bindValue(QLatin1String(":lg_code"),code);
	if(!query.exec()){

        QMap<QString,QVariant> bv = query.boundValues();
        QMap<QString,QVariant>::const_iterator i;
        QStringList bvlist;
        for(i = bv.constBegin(); i != bv.constEnd(); i++){

            QString key_ = i.key();
            QVariant var = i.value();

            bvlist.append(QString::fromLatin1("%1 - %2").arg(key_).arg(var.toString()));

        }
        qDebug() << query.lastError().text() << ", bound values: " << bvlist.join(QLatin1String("|"));
		db.rollback();
		return;
	}

	db.commit();
}

void LoggerService::logMessage(const QString &msg, LOGGTYPE lt, unsigned int code){

    QDateTime dt = QDateTime::currentDateTime();
    QString lg_timestamp = dt.toString(Qt::ISODate);

    QFuture<void> future = QtConcurrent::run(insertLogMessageToDB,lg_timestamp,msg,(unsigned int)lt,code);
}

QString LoggerService::lineFromSqlRecord(const QSqlRecord &record) const {

	QString line;

	QString lg_timestamp = record.value(0).toString();
	int lg_type = record.value(1).toInt();
	QString lg_message = record.value(2).toString();
	int lg_code = record.value(3).toInt();

	QString message = lg_message;
	switch(lg_type){

		case LOGGTYPE_CRITICAL:
			message.prepend(logprepend_critical);
			break;
		case LOGGTYPE_DEBUG1:
			message.prepend(logprepend_debug1);
			break;
		case LOGGTYPE_DEBUG2:
			message.prepend(logprepend_debug2);
			break;
		case LOGGTYPE_ERROR:
			message.prepend(logprepend_error);
			break;
		case LOGGTYPE_INFO:
			message.prepend(logprepend_info);
			break;
		case LOGGTYPE_NOTICE:
			message.prepend(logprepend_notice);
			break;
		case LOGGTYPE_SHUTDOWN:
			message.prepend(logprepend_shutdown);
			break;
		case LOGGTYPE_WARNING:
			message.prepend(logprepend_warning);
			break;
		default:
			break;
	}

	QStringList list;
	list << lg_timestamp << QString::number(lg_type) << message << QString::number(lg_code);
	line = list.join(QLatin1String("\t"));

	return line;
}

QString LoggerService::logMessagesText() const {

	QString text;

    QStringList lines;
	QSqlQuery query(m_db);
	query.prepare(QLatin1String("SELECT lg_timestamp,lg_type,lg_message,lg_code FROM logging"));
	if(query.exec()){

		while(query.next()){

			QSqlRecord record = query.record();
			QString line = lineFromSqlRecord(record);

			lines << line;
		}
	}

	text = lines.join(QLatin1String("\n"));
	return text;
}

QString LoggerService::mapToString(const QVariantMap &map) const{

	QString text;
	QStringList list;
	QVariantMap::const_iterator i;
	for(i = map.constBegin(); i != map.constEnd(); i++){

		QString key = i.key();
		QString val = i.value().toString();

		QString keyValString = QString::fromUtf8("%1:%2").arg(key).arg(val);
		list.append(keyValString);
	}

	text = list.join(QLatin1String(","));
	return text;
}

} /* namespace LoggingService */
