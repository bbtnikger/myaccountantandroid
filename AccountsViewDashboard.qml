import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "dialogs_forms"
import "AccountsViewDashboard.js" as AccountsViewDashboardScript

Item {
    id: root
    width: 800
    height: 662

    property string typeExpenses: qsTr("Expense")
    property string typeIncome: qsTr("Income")
    property string typeTransfers: qsTr("Transfer")
    property string typeDeposit: qsTr("Deposit")

    property variant accountlistitem
    property string account_id
    property string accountcurrency
//    property alias historyModel: accountsViewDashboardLastTransactions.model

    property variant history

    onAccountlistitemChanged: {

        if(accountlistitem !== null && accountlistitem !== undefined){

            busyIndicator.running = true;

            account_id = accountlistitem._id;
            accountcurrency = accountlistitem.currency;

            AccountsViewDashboardScript.resetHistory();
//            AccountsViewDashboardScript.resetBalance();

            busyIndicator.running = false;
        }
    }

    ExpenseIncomeDialog{
        id: expenseIncomeDialog
        visible: false
    }

    TransferDialog{
        id: transferDialog
        visible: false

    }

    DepositDialog{
        id: depositDialog
        visible: false
    }

    SelectTransactionTypeDialog{
        id: selectTransationTypeDialog
        visible: false
        onAccepted: AccountsViewDashboardScript.onTransactionTypeSelected()
    }

    Component{
        id: accountHistoryComponent
        AccountHistory{ }
    }

    ColumnLayout{
        id: columnLayout
        anchors.fill: parent
        anchors.margins: 2

//        AccountsViewDashboardDetails{
//            id: accountsViewDashboardDetails
//            anchors.left: parent.left
//            anchors.right: parent.right
//            Layout.fillHeight: true
//        }

        AccountsViewDashboardBalance{
            id: accountsViewDashboardBalance
            anchors.left: parent.left
            anchors.right: parent.right
            Layout.fillHeight: true
            Layout.preferredHeight: 200
            currency: accountcurrency
        }

        ToolBar {
            id: toolbar
            anchors.left: parent.left
            anchors.right: parent.right
            RowLayout {
                spacing: 6
                anchors.fill: parent

                ToolButton{
                    tooltip: qsTr("Back")
                    iconSource: "qrc:/images/ic_previous_64x64.png"
                    onClicked: stackView.pop()
                }

                ToolButton {
                    tooltip: qsTr("History")
                    iconSource: "qrc:/images/gla_history_64x64.png"
                    onClicked: AccountsViewDashboardScript.openHistory()
                }
            }
        }

//        AccountsViewDashboardLastTransactions{
//            id: accountsViewDashboardLastTransactions
//            anchors.left: parent.left
//            anchors.right: parent.right
//            Layout.fillHeight: true
//            onAddTransaction: AccountsViewDashboardScript.startAddTransaction()
//            onEditTransaction: AccountsViewDashboardScript.startEditTransaction()
//            onOpenTransaction: console.log('show details dialog')
//            onDeleteTransaction: AccountsViewDashboardScript.startDeleteTransaction()
//        }
    }

    Component.onCompleted: AccountsViewDashboardScript.resetHistory()
}

