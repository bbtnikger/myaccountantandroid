function func() {

}

function clearTabs(){

    listModel.clear();
}

function setupTabs(){

    console.log('setupTabs');

    listModel.append({"name":qsTr("Accounts")});
    listModel.append({"name":qsTr("Categories")});
    listModel.append({"name":qsTr("History")});
    listModel.append({"name":qsTr("Summaries")});
    listModel.append({"name":qsTr("Manage Types")});
    listModel.append({"name":qsTr("Tools")});
    listModel.append({"name":qsTr("Settings")});
}
