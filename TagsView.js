function func() {

}

function onAddTagDialogAccepted(){

    busyIndicator.running = true;

    var tagdata = tagDialog.getTagDetails();
    var profile_id = ProfileUtility.currentProfile;
    tagdata.profile_id = profile_id;
    var tagdatanew = TagUtility.addTag(tagdata);

    if(tagdatanew !== null){

        tagsViewList.model.append(tagdatanew);

        showInformationMessage(qsTr("Tag added successfully"));
    }

    busyIndicator.running = false;
}

function onUpdateTagDialogAccepted(){

    busyIndicator.running = true;

    var tagdetails = tagDialog.getTagDetails();
    var _id = tagsViewList.currenttaglistitem._id;
    var tagdatanew = TagUtility.updateTag(_id,tagdetails);

    if(tagdatanew !== null){

        tagsViewList.model.set(tagsViewList.currenttagindex,tagdatanew);

        showInformationMessage(qsTr("Tag updated successfully"));
    }

    busyIndicator.running = false;
}

function startAddTagDialog(){

    tagDialog.contentheight = stackView.height-100;
    tagDialog.contentwidth = stackView.width-100;
    tagDialog.tagdata = undefined;
    tagDialog.resetDefaults();
    tagDialog.title = qsTr("New Tag Dialog");
    tagDialog.accepted.disconnect(onAddTagDialogAccepted);
    tagDialog.accepted.disconnect(onUpdateTagDialogAccepted);
    tagDialog.accepted.connect(onAddTagDialogAccepted);
    tagDialog.open();
}

//var TagData = function(taglistitem){

//    this.
//}

function startUpdateTagDialog(){

//    var tagdata = new TagData(tagsViewList.currenttaglistitem);
//    tagDialog.tagdata

    tagDialog.contentheight = stackView.height-100;
    tagDialog.contentwidth = stackView.width-100;
    tagDialog.tagdata = tagsViewList.currenttaglistitem;
    tagDialog.resetDefaults();
    tagDialog.title = qsTr("Update Tag Dialog");
    tagDialog.accepted.disconnect(onAddTagDialogAccepted);
    tagDialog.accepted.disconnect(onUpdateTagDialogAccepted);
    tagDialog.accepted.connect(onUpdateTagDialogAccepted);
    tagDialog.open();
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = tagsViewList.currenttaglistitem;
    var _id = recorddata._id;
    var res = TagUtility.deleteTag(_id);

    if(res){

        tagsViewList.model.remove(tagsViewList.currenttagindex);

        showInformationMessage(qsTr("Tag deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeleteTag(){

    var title_ = tagsViewList.currenttaglistitem.name;
    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete ("+title_+'). Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.');
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}
