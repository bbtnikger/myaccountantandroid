function func() {

}

function resetPaymentMethods(){

    columnLayout.visible = false;

    clearPaymentMethods();
    setupPaymentMethods();

    columnLayout.visible = true;
}

function clearPaymentMethods(){

    listModel.clear();
}

function setupPaymentMethods(){

    var list = PaymentMethodUtility.getPaymentMethods(ProfileUtility.currentProfile);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        listModel.append(record_);
    }
}

function onCurrentIndexChanged(currentIndex){

    console.log('[PaymentMethod ListView]-Current Index('+currentIndex+')');
}
