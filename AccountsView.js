function func() {

}


function onAddAccountDialogAccepted(){

    busyIndicator.running = true;

    var accountdata = accountDialog.getAccountDetails();
    var profile_id = ProfileUtility.currentProfile;
    accountdata.profile_id = profile_id;
    var accountdatanew = AccountUtility.addAccount(accountdata);

    if(accountdatanew !== null){

        accountsViewList.model.append(accountdatanew);

        showInformationMessage(qsTr("Account added successfully"));
    }

    busyIndicator.running = false;
}

function onUpdateAccountDialogAccepted(){

    busyIndicator.running = true;

    var accountdetails = accountDialog.getAccountDetails();
    var _id = accountsViewList.currentaccountlistitem._id;
    var accountdatanew = AccountUtility.updateAccount(_id,accountdetails);

    if(accountdatanew !== null && accountdatanew !== undefined){

        accountsViewList.model.set(accountsViewList.currentaccountindex,accountdatanew);

        showInformationMessage(qsTr("Account updated successfully"));
    }

    busyIndicator.running = false;
}

function startAddAccountDialog(){

    busyIndicator.running = true;

    accountDialog.contentheight = stackView.height-100;
    accountDialog.contentwidth = stackView.width-100;
    accountDialog.accountdata = undefined;
    accountDialog.resetDefaults();
    accountDialog.title = qsTr("New Account Dialog");
    accountDialog.accepted.disconnect(onAddAccountDialogAccepted);
    accountDialog.accepted.disconnect(onUpdateAccountDialogAccepted);
    accountDialog.accepted.connect(onAddAccountDialogAccepted);

    busyIndicator.running = false;
    accountDialog.open();
}

function startUpdateAccountDialog(){

    busyIndicator.running = true;

    accountDialog.contentheight = stackView.height-100;
    accountDialog.contentwidth = stackView.width-100;
    accountDialog.accountdata = accountsViewList.currentaccountlistitem;
    accountDialog.resetDefaults();
    accountDialog.title = qsTr("Update Account Dialog");
    accountDialog.accepted.disconnect(onAddAccountDialogAccepted);
    accountDialog.accepted.disconnect(onUpdateAccountDialogAccepted);
    accountDialog.accepted.connect(onUpdateAccountDialogAccepted);

    busyIndicator.running = false;
    accountDialog.open();
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = accountsViewList.currentaccountlistitem;
    var _id = recorddata._id;
    var res = AccountUtility.deleteAccount(_id);

    if(res){

        accountsViewList.model.remove(accountsViewList.currentaccountindex);

        showInformationMessage(qsTr("Account deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeleteAccount(){

    var title_ = accountsViewList.currentaccountlistitem.name;
    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete ("+title_+'). Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.');
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}

function startOpenDashboard(){

    busyIndicator.running = true;
    stackView.push({item:accountsViewDashboardComponent,properties:{account_id:accountsViewList.currentaccountlistitem._id}});
    busyIndicator.running = false;
}
