import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import "ManageTypesViewList.js" as ManageTypesViewListScript

Item {
    id: root

    property variant currenttypelistitem
    property alias currenttypeindex: listView.currentIndex

    signal clicked()

    ListModel{
        id: listModel
    }

    Component{
        id: tabDelegate
        Item{
            width: ListView.view.width
            height: 120

            Rectangle{
                anchors.fill: parent
            }

            RowLayout{
                anchors.fill: parent
                anchors.margins: 6

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: name
//                    font.pointSize: 13
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: { listView.currentIndex = index; listView.openPage(); }
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: listView.width-2; height: 120
            y: listView.currentItem.y
            x: 1
            color: "lightsteelblue";
        }
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    ColumnLayout{
        anchors.fill: parent

        ListView{
            id: listView
            model: listModel
            delegate: tabDelegate
            highlight: highlight
            Layout.fillHeight: true
            anchors.left: parent.left
            anchors.right: parent.right
            highlightFollowsCurrentItem: false
            focus: true

            function openPage(){

                clicked();
            }

            onCurrentIndexChanged: {

                currenttypelistitem = listModel.get(currentIndex);
            }
        }
    }

    Component.onCompleted: ManageTypesViewListScript.resetTypes()
}

