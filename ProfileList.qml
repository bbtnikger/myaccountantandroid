import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import "ProfileList.js" as ProfileListScript

Item {
    id: root
    width: 100
    height: 62

    property variant currentprofilelistitem
    property string currentprofile_id

    property string tenant_name: qsTr("Tenant")

    property alias model: listModel

    signal addProfile()
    signal editProfile()
    signal deleteProfile()

    function onLoginSuccess(){

        ProfileListScript.resetProfiles();
        tenant_name = LocalUserUtility.organizationId;
    }

    ListModel{
        id: listModel
    }

    Rectangle{
        color: "darkslategrey"
        anchors.fill: parent
    }

    Component{
        id: profileDelegate
        Item{
            width: listView.width
            height: 80

            MouseArea {
                anchors.fill: parent
                onClicked: listView.currentIndex = index
            }

            RowLayout{
                anchors.fill: parent
                anchors.margins: 6

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: name
                    font.pointSize: 13
                    Layout.fillWidth: true
                    color: "honeydew"
                }

                Image {
                    id: img
                    fillMode: Image.PreserveAspectFit
                    anchors.verticalCenter: parent.verticalCenter
                    source: "qrc:/images/ic_edit - 24x24 - inverted.png"
                    opacity: marea.pressed? 0.6 : 1.0


                    MouseArea{
                        id: marea
                        anchors.fill: parent
                        onClicked: { listView.currentIndex = index; listView.startEditProcess(); }
                    }
                }
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: listView.width; height: 80
            y: listView.currentItem.y
            color: "#101010";
        }
    }

    Component {
        id: headerComponent

        Item {
            width: listView.width
            height: 100
            y: ListView.view.y-height

            ColumnLayout{
                anchors.fill: parent
                anchors.margins: 6

                Label{
                    id: labelTenantName
                    text: tenant_name
                    font.pointSize: 15
                    color: "whitesmoke"
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Label{
                    text: qsTr("Profiles")
                    font.pointSize: 9
                    color: "whitesmoke"
                }
            }
        }
    }

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 1
        spacing: 6

        ListView{
            id: listView
            model: listModel
            delegate: profileDelegate
            highlight: highlight
            header: headerComponent
            Layout.fillHeight: true
            anchors.left: parent.left
            anchors.right: parent.right

            onCurrentIndexChanged: {

                currentprofilelistitem = listModel.get(currentIndex);
            }

            function startEditProcess(){

                editProfile();
            }

            function startDeleteProcess(){

                deleteProfile();
            }
        }

        ToolBar {
            id: toolbar
            anchors.left: parent.left
            anchors.right: parent.right

            RowLayout {
                anchors.fill: parent

                ToolButton {
                    tooltip: qsTr("Add Profile")
                    iconSource: "qrc:/images/ic_add_64x64.png"
                    onClicked: console.log('add profile')
                }
            }
        }
    }

    Component.onCompleted: {

        LoginUtility.loginSuccess.connect(onLoginSuccess);
    }
}

