TEMPLATE = app

QT += qml quick sql
CONFIG += c++11

SOURCES += main.cpp \
    LoggerService.cpp \
    helper_classes/accounttypeutility.cpp \
    helper_classes/accountutility.cpp \
    helper_classes/authorizationutility.cpp \
    helper_classes/categoryutility.cpp \
    helper_classes/depositutility.cpp \
    helper_classes/expenseutility.cpp \
    helper_classes/historyutility.cpp \
    helper_classes/incomeutility.cpp \
    helper_classes/localuserutility.cpp \
    helper_classes/loginutility.cpp \
    helper_classes/networkutility.cpp \
    helper_classes/payeeutility.cpp \
    helper_classes/paymentmethodutility.cpp \
    helper_classes/profileutility.cpp \
    helper_classes/reconcileutility.cpp \
    helper_classes/settingsmanager.cpp \
    helper_classes/sortfilterproxymodel.cpp \
    helper_classes/tagutility.cpp \
    helper_classes/transactionutility.cpp \
    helper_classes/transferutility.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    AccountsView.js \
    AccountsViewDashboard.js \
    AccountsViewList.js \
    AccountTypesView.js \
    AccountTypesViewList.js \
    CategoriesView.js \
    HistoryView.js \
    LoginScript.js \
    Main.js \
    ManageTypesView.js \
    ManageTypesViewList.js \
    PayeesView.js \
    PayeesViewList.js \
    PaymentMethodsView.js \
    PaymentMethodsViewList.js \
    ProfileList.js \
    ProfileView.js \
    ProfileViewTabList.js \
    SummariesView.js \
    TagsView.js \
    TagsViewList.js \
    ToolsView.js \
    LoginDialogContentForm.ui.qml \
    LoginScreenForm.ui.qml \
    AccountsView.qml \
    AccountsViewDashboard.qml \
    AccountsViewDashboardBalance.qml \
    AccountsViewDashboardDetails.qml \
    AccountsViewDashboardLastTransactions.qml \
    AccountsViewList.qml \
    AccountTypesView.qml \
    AccountTypesViewDashboard.qml \
    AccountTypesViewList.qml \
    CategoriesView.qml \
    HistoryView.qml \
    LoginDialogContent.qml \
    LoginScreen.qml \
    ManageTypesView.qml \
    ManageTypesViewList.qml \
    PayeesView.qml \
    PayeesViewDashboard.qml \
    PayeesViewList.qml \
    PaymentMethodsView.qml \
    PaymentMethodsViewDashboard.qml \
    PaymentMethodsViewList.qml \
    ProfileList.qml \
    ProfileView.qml \
    ProfileViewTabList.qml \
    SummariesView.qml \
    TagsView.qml \
    TagsViewDashboard.qml \
    TagsViewList.qml \
    ToastMessage.qml \
    ToolsView.qml \
    AccountHistory.qml \
    AccountHistory.js \
    ViewExpenseIncomeDetails.qml \
    ViewTransferDetails.qml \
    ViewDepositDetails.qml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

HEADERS += \
    LoggerService.h \
    helper_classes/accounttypeutility.h \
    helper_classes/accountutility.h \
    helper_classes/authorizationutility.h \
    helper_classes/categoryutility.h \
    helper_classes/depositutility.h \
    helper_classes/expenseutility.h \
    helper_classes/historyutility.h \
    helper_classes/incomeutility.h \
    helper_classes/localuserutility.h \
    helper_classes/loginutility.h \
    helper_classes/networkutility.h \
    helper_classes/payeeutility.h \
    helper_classes/paymentmethodutility.h \
    helper_classes/profileutility.h \
    helper_classes/reconcileutility.h \
    helper_classes/settingsmanager.h \
    helper_classes/sortfilterproxymodel.h \
    helper_classes/tagutility.h \
    helper_classes/transactionutility.h \
    helper_classes/transferutility.h

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

