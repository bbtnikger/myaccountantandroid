import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

Component {

    Window{
        id: root
        width: 250
        height: 62
        visible: false
        opacity: 0.0
        flags: Qt.ToolTip
//        title: title_
        color: backgroundcolor_

        property string body_: qsTr("Message")
        property string title_: qsTr("Information")
        property int duration_: 2 // in seconds
        property string backgroundcolor_: "#414141"
        property string bordercolor_: "#F1F1F1"
        property int backgroundradius_: 6
        property real backgroundopacity_: 0.4

        function show(){

            sequentialAnimation.start();
        }

        SequentialAnimation{
            id: sequentialAnimation

            ScriptAction{
                script: root.visible = true
            }

            NumberAnimation{ target: root; properties: "opacity"; duration: 200; to: 0.9 }
            NumberAnimation{ target: root; properties: "opacity"; duration: duration_*1500; from: 0.9; to: 1 }
            NumberAnimation{ target: root; properties: "opacity"; duration: 200; to: 0 }

            ScriptAction{
                script: root.visible = false
            }
        }

        Item{
            id: containerItem
            anchors.fill: parent
            anchors.margins: 6

            Label{
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                wrapMode: Text.WordWrap
                text: body_
                color: bordercolor_
                horizontalAlignment: Qt.AlignHCenter
            }
        }

        Component.onCompleted: show()
    }
}
