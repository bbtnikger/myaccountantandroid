import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "AccountTypesView.js" as AccountTypesViewScript
import "dialogs_forms"

Item {
    id: root

    AccountTypeDialog{
        id: accountTypeDialog
        visible: false
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    RowLayout{
        anchors.fill: parent
        spacing: 1

        AccountTypesViewList{
            id: accountTypesViewList
            Layout.minimumWidth: 200
            Layout.fillWidth: true
            Layout.fillHeight: true
            onAddAccountType: AccountTypesViewScript.startAddAccountTypeDialog()
            onEditAccountType: AccountTypesViewScript.startUpdateAccountTypeDialog();
            onDeleteAccountType: AccountTypesViewScript.startDeleteAccountType()
        }

//        AccountTypesViewDashboard{
//            anchors.top: parent.top
//            anchors.bottom: parent.bottom
//            anchors.topMargin: 1
//            anchors.bottomMargin: 1
//            Layout.fillWidth: true
//        }
    }
}

