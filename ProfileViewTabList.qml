import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import "ProfileViewTabList.js" as ProfileViewTabListScript

Item {
    id: root

    property variant profilelistitem
    property string profile_id

    property variant currenttablistitem
    property alias currenttabindex: listView.currentIndex

    function resetTabs(){

        console.log('[ProfileViewTabList]-resetTabs profilelistitem('+profilelistitem+') profile_id('+profile_id+')');

        ProfileViewTabListScript.clearTabs();

        if(profilelistitem !== undefined)
            ProfileViewTabListScript.setupTabs();
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    ListModel{
        id: listModel
    }

    Component{
        id: tabDelegate
        Item{
            width: ListView.view.width
            height: 60

            RowLayout{
                anchors.fill: parent
                anchors.margins: 6

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: name
                    font.pointSize: 16
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: listView.currentIndex = index
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: listView.width-2; height: 60
            y: listView.currentItem.y
            x: 1
            color: "lightsteelblue";
        }
    }

    ColumnLayout{
        anchors.fill: parent

        ListView{
            id: listView
            model: listModel
            delegate: tabDelegate
            highlight: highlight
            Layout.fillHeight: true
            anchors.left: parent.left
            anchors.right: parent.right
            highlightFollowsCurrentItem: false
            focus: true

            onCurrentIndexChanged: {

                currenttablistitem = listModel.get(currentIndex);
            }
        }
    }
}
