import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "AccountsViewList.js" as AccountsViewListScript
import "dialogs_forms"

Item {
    id: root

    property variant currentaccountlistitem
    property alias currentaccountindex: listView.currentIndex
    property alias model: listModel

    signal addAccount()
    signal editAccount()
    signal deleteAccount()
    signal openDashboard()

    function resetPage(){

        AccountsViewListScript.resetAccounts();
    }

    ListModel{
        id: listModel
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    Component{
        id: itemDelegate
        Item{
            width: listView.width
            height: 110

//            MouseArea {
//                anchors.fill: parent
//                onClicked: { listView.currentIndex = index; listView.startOpenDashboard(); }
//            }

            Rectangle{
                anchors.fill: parent
                color: "#000000FF"
                border.color: "grey"
            }

            RowLayout{
                anchors.fill: parent
                anchors.margins: 6

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: name + ' (' + currency + ' )'
                    Layout.fillWidth: true
                }
            }

            MouseArea{
                anchors.fill: parent
                onPressAndHold: { listView.currentIndex = index; listView.startMenu(); }
                onClicked: { listView.currentIndex = index; listView.startOpenDashboard(); }
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: listView.width-2; height: 110
            y: listView.currentItem.y
            x: 1
            color: "lightsteelblue";
        }
    }

    // The delegate for each section header
    Component {
        id: sectionHeading
        Rectangle {
            width: listView.width
            height: 30
            color: "lightgrey"

            Label {
                text: section
                font.pointSize: 10
                anchors.left: parent.left
                anchors.leftMargin: 3
            }
        }
    }

    Menu{
        id: menu
        MenuItem{
            text: qsTr("Edit")
            onTriggered: listView.startEditProcess()
        }
        MenuItem{
            text: qsTr("Delete")
            onTriggered: listView.startDeleteProcess()
        }
    }

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 1

        ListView{
            id: listView
            model: listModel
            delegate: itemDelegate
            highlight: highlight
            Layout.fillHeight: true
            anchors.left: parent.left
            anchors.right: parent.right
            highlightFollowsCurrentItem: false
            focus: true
            section.property: "accounttypename"
            section.criteria: ViewSection.FullString
            section.delegate: sectionHeading

            onCurrentIndexChanged: {

                currentaccountlistitem = listModel.get(currentIndex);
            }

            function startEditProcess(){

                editAccount()
            }

            function startDeleteProcess(){

                deleteAccount();
            }

            function startMenu(){

                menu.popup();
            }

            function startOpenDashboard(){

                openDashboard();
            }
        }

        ToolBar {
            id: toolbar
            anchors.left: parent.left
            anchors.right: parent.right
            RowLayout {
                spacing: 6
                anchors.fill: parent

                ToolButton {
                    tooltip: qsTr("Add Account")
                    iconSource: "qrc:/images/ic_add_64x64.png"
                    onClicked: addAccount()
                }
            }
        }
    }

    Component.onCompleted: AccountsViewListScript.resetAccounts()
}

