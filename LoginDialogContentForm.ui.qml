import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2

Item {
    anchors.fill: parent
    anchors.margins: 6
    property alias username: textFieldUsername.text
    property alias password: textFieldPassword.text
    property alias organizationId: textFieldOrganizationId.text
    property alias buttonOK: buttonOK

    GroupBox {
        id: groupBox1
        anchors.fill: parent
        title: qsTr("Login Details")

        ColumnLayout {
            id: columnLayout1
            anchors.fill: parent

            Item {
                id: item2
                Layout.fillHeight: true
            }

            Label {
                id: labelUsername
                text: qsTr("Username")
            }

            TextField {
                id: textFieldUsername
                Layout.fillWidth: true
                placeholderText: qsTr("ex. 'john.doe@example.com'")
                inputMethodHints: Qt.ImhLowercaseOnly
            }

            Label {
                id: labelPassword
                text: qsTr("Password")
            }

            TextField {
                id: textFieldPassword
                echoMode: TextInput.Password
                Layout.fillWidth: true
                placeholderText: qsTr("password")
            }

            Label {
                id: labelOrganizationId
                text: qsTr("Organization ID")
            }

            TextField {
                id: textFieldOrganizationId
                echoMode: 0
                Layout.fillWidth: true
                placeholderText: qsTr("my-oganization-id")
            }

            Item {
                id: item1
                Layout.fillHeight: true
            }

            RowLayout{
                layoutDirection: Qt.RightToLeft
                Layout.preferredHeight: 32
                spacing: 9

                Item{
                    Layout.fillWidth: true
                }

                Button{
                    id: buttonOK
                    text: "OK"
                }
            }
        }

    }
}

