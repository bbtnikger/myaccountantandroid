import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "TagsView.js" as TagsViewScript
import "dialogs_forms"

Item {
    id: root

    TagDialog{
        id: tagDialog
        visible: false
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    RowLayout{
        anchors.fill: parent
        spacing: 1

        TagsViewList{
            id: tagsViewList
            Layout.minimumWidth: 200
//            Layout.maximumWidth: 300
//            Layout.preferredWidth: 200
            Layout.fillWidth: true
            Layout.fillHeight: true
            onAddTag: TagsViewScript.startAddTagDialog()
            onEditTag: TagsViewScript.startUpdateTagDialog();
            onDeleteTag: TagsViewScript.startDeleteTag()
        }

//        TagsViewDashboard{
//            anchors.top: parent.top
//            anchors.bottom: parent.bottom
//            anchors.topMargin: 1
//            anchors.bottomMargin: 1
//            Layout.fillWidth: true
//        }
    }
}

