import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "AccountHistory.js" as AccountHistoryScript
import "dialogs_forms"

Item {
    id: root

    property string typeExpenses: qsTr("Expense")
    property string typeIncome: qsTr("Income")
    property string typeTransfers: qsTr("Transfer")
    property string typeDeposit: qsTr("Deposit")

    property variant accountlistitem
    property string account_id
    property string accountcurrency
    property alias historyModel: accountsViewDashboardLastTransactions.model

    property variant history

    onAccountlistitemChanged: {

        if(accountlistitem !== null && accountlistitem !== undefined){

            busyIndicator.running = true;

            account_id = accountlistitem._id;
            accountcurrency = accountlistitem.currency;

            AccountHistoryScript.resetHistory();
            AccountHistoryScript.resetBalance();

            busyIndicator.running = false;
        }
    }

    ExpenseIncomeDialog{
        id: expenseIncomeDialog
        visible: false
    }

    TransferDialog{
        id: transferDialog
        visible: false

    }

    DepositDialog{
        id: depositDialog
        visible: false
    }

    SelectTransactionTypeDialog{
        id: selectTransationTypeDialog
        visible: false
        onAccepted: AccountHistoryScript.onTransactionTypeSelected()
    }

    Component{
        id: expenseIncomeComponent
        ViewExpenseIncomeDetails{ }
    }

    Component{
        id: depositComponent
        ViewDepositDetails{ }
    }

    Component{
        id: transferComponent
        ViewTransferDetails{ }
    }

    ColumnLayout{
        id: columnLayout
        anchors.fill: parent
        spacing: 3

        AccountsViewDashboardLastTransactions{
            id: accountsViewDashboardLastTransactions
            anchors.left: parent.left
            anchors.right: parent.right
            Layout.fillHeight: true
            onAddTransaction: AccountHistoryScript.startAddTransaction()
            onEditTransaction: AccountHistoryScript.startEditTransaction()
            onOpenTransaction: console.log('show details dialog')
            onDeleteTransaction: AccountHistoryScript.startDeleteTransaction()
            onOpenDetails: AccountHistoryScript.openDetailsPage()
        }

        ToolBar {
            id: toolbar
            anchors.left: parent.left
            anchors.right: parent.right
            RowLayout {
                spacing: 6
                anchors.fill: parent

                ToolButton{
                    tooltip: qsTr("Back")
                    iconSource: "qrc:/images/ic_previous_64x64.png"
                    onClicked: stackView.pop()
                }

                ToolButton {
                    tooltip: qsTr("History")
                    iconSource: "qrc:/images/ic_add_64x64.png"
                    onClicked: AccountHistoryScript.startAddTransaction()
                }
            }
        }
    }

    Component.onCompleted: AccountHistoryScript.resetHistory()
}

