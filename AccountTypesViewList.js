function func() {

}

function resetAccountTypes(){

    columnLayout.visible = false;

    clearAccountTypes();
    setupAccountTypes();

    columnLayout.visible = true;
}

function clearAccountTypes(){

    listModel.clear();
}

function setupAccountTypes(){

    var list = AccountTypeUtility.getAccountTypes(ProfileUtility.currentProfile);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        listModel.append(record_);
    }
}

function onCurrentIndexChanged(currentIndex){

    console.log('[Account Types ListView]-Current Index('+currentIndex+')');
}
