import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "dialogs_forms"

Item {
    id: root

    property variant transferdata

    function setup(){

        var datestring = LocalUserUtility.localizedDateFromString(transferdata.date);
        var amount = transferdata.amount;
        var description = transferdata.description;
        var cleared = ReconcileUtility.clearedName(transferdata.cleared);
        var accountfromname = transferdata.origin.name;
        var accounttoname = transferdata.destination.name;
        var amountnum = LocalUserUtility.localizedNumber(transferdata.amount);

        var basecurrency = "";
        var countercurrency = "";

        basecurrency = transferdata.origin.currency;
        accountfromname = accountfromname + ' (' + basecurrency + ')';
        countercurrency = transferdata.destination.currency;
        accounttoname = accounttoname + ' (' + countercurrency + ')';

        dateLabel.text = datestring;
        amountLabel.text = amount + ' ' + countercurrency;
        transferStatusLabel.text = cleared;
        accountFromLabel.text = accountfromname;
        accountToLabel.text = accounttoname;
    }

    function onUpdateTransferDialogAccepted(){

        busyIndicator.running = true;

        var transferdata_ = transferDialog.getTransferDetails();
        var _id = transferdata._id;
        var profile_id = ProfileUtility.currentProfile;
        transferdata_.profile_id = profile_id;
        var transferdatanew = TransferUtility.updateTransfer(_id,transferdata_);

        if(transferdatanew !== null && transferdatanew !== undefined){

            showInformationMessage(qsTr("Transfer updated successfully"));
            console.log(qsTr("Transfer updated successfully"));

            transferdata = transferdatanew;

            setup();

            transferDialog.close();

//            if(transferdatanew.origin._id === account_id || transferdatanew.destination._id === account_id)
//                updateTransferRecordToModel(transferdatanew);
//            else{

//                var idx = findHistoryIndex(record_._id);
//                if(idx !== -1)
//                    historyModel.remove(idx);
//            }
        }

        busyIndicator.running = false;
    }

    function openEditDialog(){

        busyIndicator.running = true;

        transferDialog.contentheight = root.height-100;
        transferDialog.contentwidth = root.width-100;
        transferDialog.transferdata = transferdata;
        transferDialog.resetDefaults();
        transferDialog.open();

        busyIndicator.running = false;
    }

    TransferDialog{
        id: transferDialog
        title: qsTr("Transfer Dialog");

        onAccepted: onUpdateTransferDialogAccepted()
    }

    ColumnLayout{
        anchors.top: parent.top
        anchors.bottom: toolbar.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 9
        spacing: 6

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Date")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: dateLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Account Origin")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: accountFromLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Account Destination")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: accountToLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Amount")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: amountLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Currency quotation value")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: currencyValueLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Transfer status")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: transferStatusLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Description")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: descriptionLabel
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 10
        }
    }

    ToolBar {
        id: toolbar
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        RowLayout {
            spacing: 6
            anchors.fill: parent

            ToolButton{
                tooltip: qsTr("Back")
                iconSource: "qrc:/images/ic_previous_64x64.png"
                onClicked: stackView.pop()
            }

            Item{
                Layout.fillWidth: true
                Layout.preferredHeight: 10
            }

            ToolButton{
                tooltip: qsTr("Edit")
                iconSource: "qrc:/images/ic_edit_64x64.png"
                onClicked: openEditDialog()
            }

            Item{
                Layout.fillWidth: true
                Layout.preferredHeight: 10
            }
        }
    }

    Component.onCompleted: setup()
}

