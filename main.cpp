#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml/qqml.h>

#include "helper_classes/loginutility.h"
#include "helper_classes/networkutility.h"
#include "helper_classes/localuserutility.h"
#include "helper_classes/categoryutility.h"
#include "helper_classes/accounttypeutility.h"
#include "helper_classes/accountutility.h"
#include "helper_classes/payeeutility.h"
#include "helper_classes/paymentmethodutility.h"
#include "helper_classes/tagutility.h"
#include "helper_classes/expenseutility.h"
#include "helper_classes/incomeutility.h"
#include "helper_classes/depositutility.h"
#include "helper_classes/transferutility.h"
#include "helper_classes/authorizationutility.h"
#include "helper_classes/profileutility.h"
#include "helper_classes/settingsmanager.h"
#include "helper_classes/historyutility.h"
#include "helper_classes/reconcileutility.h"
#include "helper_classes/sortfilterproxymodel.h"
#include "LoggerService.h"

using namespace MyAccountantCloud::HelperClasses;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QCoreApplication::setOrganizationName("IDEAL APPS");
    QCoreApplication::setOrganizationDomain("myaccountantapp.com");
    QCoreApplication::setApplicationName("MyAccountant Cloud");

    qmlRegisterType<SortFilterProxyModel>("org.qtproject.example", 1, 0, "SortFilterProxyModel");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QLatin1String("LoginUtility"),LOGINUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("LocalUserUtility"),LOCALUSERUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("CategoryUtility"),CATEGORYUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("AccountUtility"),ACCOUNTUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("AccountTypeUtility"),ACCOUNTTYPEUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("PayeeUtility"),PAYEEUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("PaymentMethodUtility"),PAYMENTMETHODUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("TagUtility"),TAGUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("ExpenseUtility"),EXPENSEUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("IncomeUtility"),INCOMEUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("DepositUtility"),DEPOSITUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("TransferUtility"),TRANSFERUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("ProfileUtility"),PROFILEUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("HistoryUtility"),HISTORYUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("AuthorizationUtility"),AUTHORIZATIONUTILITY);
    engine.rootContext()->setContextProperty(QLatin1String("LoggerService"),LOGGERSERVICE);
    engine.rootContext()->setContextProperty(QLatin1String("SettingsManager"),SETTINGSMANAGER);
    engine.rootContext()->setContextProperty(QLatin1String("ReconcileUtility"),RECONCILEUTILITY);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QObject::connect(&app,SIGNAL(aboutToQuit()),LOGINUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),NETWORKUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),LOCALUSERUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),CATEGORYUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),ACCOUNTUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),ACCOUNTTYPEUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),PAYMENTMETHODUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),TAGUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),EXPENSEUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),INCOMEUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),DEPOSITUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),TRANSFERUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),AUTHORIZATIONUTILITY,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),LOGGERSERVICE,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),SETTINGSMANAGER,SLOT(clearInstance()));
    QObject::connect(&app,SIGNAL(aboutToQuit()),RECONCILEUTILITY,SLOT(clearInstance()));

    return app.exec();
}

