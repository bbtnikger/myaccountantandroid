import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "TransferDialogScript.js" as TransferDialogScript
import "../styles"

Dialog{
    id: root
    visible: false
    title: qsTr("Transfer Dialog")

    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant transferdata
    property alias originAccountModel: transferDialogContainer.originAccountModel
    property alias destinationAccountModel: transferDialogContainer.destinationAccountModel
    property int contentwidth: 400
    property int contentheight: 400


    function getTransferDetails(){

        var transferdetails = new TransferDialogScript.TransferDetails();
        return transferdetails;
    }

    function setupData(){

        transferDialogContainer.textFieldAmount.text = LocalUserUtility.localizedNumber(transferdata.amount);
        transferDialogContainer.textFieldRate.text = LocalUserUtility.localizedNumber(transferdata.rate,6);
        transferDialogContainer.buttonDate.text = LocalUserUtility.localizedDate(transferdata.date);
        transferDialogContainer.calendar.selectedDate = transferdata.date;

        TransferDialogScript.selectAccountOrigin(transferdata.origin__id);
        TransferDialogScript.selectAccountDestination(transferdata.destination__id);

    }

    function resetDefaults(){

        TransferDialogScript.resetAccounts();

        console.log('reset defaults');

        transferDialogContainer.textFieldAmount.text = "";
        transferDialogContainer.textFieldRate.text = "1.0";
        var dt = new Date();
        transferDialogContainer.calendar.selectedDate = dt;
        transferDialogContainer.originAccountCurrentIndex = 0;
        transferDialogContainer.destinationAccountCurrentIndex = 0;
        transferDialogContainer.buttonDate.text = calendar.selectedDate.toLocaleDateString()

        if(transferdata !== null && transferdata !== undefined)
            setupData();
    }

    TransferDialogContainer{
        id: transferDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight

        property alias calendar: calendar

        buttonDate.onClicked: calendar.visible = !calendar.visible

        Calendar{
            id: calendar
            visible: false
            x: transferDialogContainer.mapFromItem(transferDialogContainer.rowLayoutDate,transferDialogContainer.buttonDate.x,transferDialogContainer.buttonDate.y).x
            y: transferDialogContainer.mapFromItem(transferDialogContainer.columnLayout,transferDialogContainer.rowLayoutDate.x,transferDialogContainer.rowLayoutDate.y).y+transferDialogContainer.buttonDate.height+6
            style: MACalendarStyle{ }
            onSelectedDateChanged: transferDialogContainer.buttonDate.text = calendar.selectedDate.toLocaleDateString()
            onClicked: {

                transferDialogContainer.buttonDate.text = selectedDate.toLocaleDateString();
                visible = false;
            }
        }
    }

//    // Component.onCompleted: resetDefaults()
}

