function func() {

}

var ExpenseIncomeDetails = function(){

    this.amount = parseFloat(expenseIncomeDialogContainer.textFieldAmount.text);
    var dt = calendar.selectedDate;
    this.date = dt;
    this.account = accountModel.get(expenseIncomeDialogContainer.accountCurrentIndex)._id;
    if(eitype == 1)
        this.payee = payeeModel.get(expenseIncomeDialogContainer.payeeCurrentIndex)._id;
    else
        this.payer = payeeModel.get(expenseIncomeDialogContainer.payeeCurrentIndex)._id;
    this.category = categoryModel.get(expenseIncomeDialogContainer.categoryCurrentIndex)._id;
    this.payment_method = paymentMethodModel.get(expenseIncomeDialogContainer.paymentMethodCurrentIndex)._id;
    this.tag = tagModel.get(expenseIncomeDialogContainer.tagCurrentIndex)._id;
}

function clearCurrencies(){

    currencyModel.clear();
}

function setupCurrencies(){

    var currencylist = SettingsManager.currencyList;
//    console.log('currencylist:'+currencylist);
    if(currencylist === undefined)
        return;

    var len = currencylist.length;
    for(var i = 0; i < len; i++){

        var record_ = currencylist[i];
        currencyModel.append(record_);
    }

    expenseIncomeDialogContainer.currencyModel = currencyModel;
}

function clearAccountTypes(){

    accountTypeModel.clear();
}

function setupAccountTypes(){

    var list = AccountTypeUtility.getAccountTypes(ProfileUtility.currentProfile);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        accountTypeModel.append(record_);
    }

    expenseIncomeDialogContainer.accountTypeModel = accountTypeModel;
}

function selectCurrency(code){

    for(var i = 0; i < currencyModel.count; i++){

        var _code = currencyModel.get(i).code;

        if(code === _code){

            expenseIncomeDialogContainer.currencyCurrentIndex = i;
            break;
        }
    }
}

function selectAccount(account_id){

    for(var i = 0; i < accountModel.count; i++){

        var _account_id = accountModel.get(i)._id;

        if(account_id === _account_id){

            expenseIncomeDialogContainer.accountCurrentIndex = i;
            break;
        }
    }
}

function selectCategory(category_id){

    for(var i = 0; i < categoryModel.count; i++){

        var _category_id = categoryModel.get(i)._id;

        if(category_id === _category_id){

            expenseIncomeDialogContainer.categoryCurrentIndex = i;
            break;
        }
    }
}

function selectPayee(payee_id){

    for(var i = 0; i < payeeModel.count; i++){

        var _payee_id = payeeModel.get(i)._id;

        if(payee_id === _payee_id){

            expenseIncomeDialogContainer.payeeCurrentIndex = i;
            break;
        }
    }
}


function selectPayer(payer_id){

    for(var i = 0; i < payeeModel.count; i++){

        var _payer_id = payeeModel.get(i)._id;

        if(payer_id === _payer_id){

            expenseIncomeDialogContainer.payeeCurrentIndex = i;
            break;
        }
    }
}
function selectPaymentMethod(paymentmethod_id){

    for(var i = 0; i < paymentMethodModel.count; i++){

        var _paymentmethod_id = paymentMethodModel.get(i)._id;

        if(paymentmethod_id === _paymentmethod_id){

            expenseIncomeDialogContainer.paymentMethodCurrentIndex = i;
            break;
        }
    }
}

function selectTag(tag_id){

    console.log('selectTag :'+tag_id);

    for(var i = 0; i < tagModel.count; i++){

        var _tag_id = tagModel.get(i)._id;

        if(tag_id === _tag_id){

            expenseIncomeDialogContainer.tagCurrentIndex = i;
            break;
        }
    }
}

function resetAccounts(list){

    accountModel.clear();

//    var list = AccountUtility.getAccounts(profile_id);
    if(list === undefined){

        console.log('list is undefined');
        return;
    }

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        accountModel.append(record_);
    }
}

function resetPayees(list){

    payeeModel.clear();

//    var list = PayeeUtility.getPayees(profile_id);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        payeeModel.append(record_);
    }
}

function resetPaymentMethods(list){

    paymentMethodModel.clear();

//    var list = PaymentMethodUtility.getPaymentMethods(profile_id);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        paymentMethodModel.append(record_);
    }
}

function resetTags(list){

    tagModel.clear();

//    var list = TagUtility.getTags(profile_id);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        tagModel.append(record_);
    }
}

function resetCategories(list){

    categoryModel.clear();

//    var list = CategoryUtility.getCategories(profile_id);
    if(list === undefined || list === null)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        categoryModel.append(record_);
    }
}


function resetDefaults(){

    var expenseProperties = ExpenseUtility.getExpenseProperties(ProfileUtility.currentProfile);

    if(expenseProperties === undefined)
        return;

    var accounts = expenseProperties.accounts;
    resetAccounts(accounts);

    var payees = expenseProperties.payees;
    resetPayees(payees);

    var paymentmethods = expenseProperties.paymentmethods;
    resetPaymentMethods(paymentmethods);

    var categories = expenseProperties.categories;
    resetCategories(categories);

    var tags = expenseProperties.tags;
    resetTags(tags);
}
