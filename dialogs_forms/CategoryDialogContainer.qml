import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GroupBox {
    id: root
    title: qsTr("Category Details")

    property alias textField: textField

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 9

        Label{
            text: qsTr("Name")
            font.pointSize: 9
        }

        TextField{
            id: textField
            placeholderText: qsTr("Enter name here")
            anchors.left: parent.left
            anchors.right: parent.right
        }

        Item{
            Layout.fillHeight: true
            width: 100
        }
    }
}

