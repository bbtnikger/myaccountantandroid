import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "AccountTypeDialogScript.js" as AccountTypeDialogScript

Dialog{
    id: root
    visible: false
    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant accounttypedata
    property int contentwidth: 400
    property int contentheight: 400

    function getAccountTypeDetails(){

        var accounttypedetails = new AccountTypeDialogScript.AccountTypeDetails();
        return accounttypedetails;
    }

    function setupData(){

        accountTypeDialogContainer.textField.text = accounttypedata.name;
    }

    function resetDefaults(){

        accountTypeDialogContainer.textField.text = "";

        if(accounttypedata !== null && accounttypedata !== undefined)
            setupData();
    }

    AccountTypeDialogContainer{
        id: accountTypeDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight
    }

    // Component.onCompleted: resetDefaults()
}

