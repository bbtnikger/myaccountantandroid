import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GroupBox {
    id: root
    title: qsTr("Deposit Details")

    property alias accountModel: accountModel
    property alias payeeModel: payeeModel
    property alias categoryModel: categoryModel
    property alias paymentMethodModel: paymentMethodModel
    property alias tagModel: tagModel
    property alias accountCurrentIndex: comboBoxAccount.currentIndex
    property alias payeeCurrentIndex: comboBoxPayee.currentIndex
    property alias categoryCurrentIndex: comboBoxCategory.currentIndex
    property alias paymentMethodCurrentIndex: comboBoxPaymentMethod.currentIndex
    property alias tagCurrentIndex: comboBoxTag.currentIndex
    property alias textFieldAmount: textFieldAmount
    property alias buttonDate: buttonDate
    property alias columnLayout: columnLayout
    property alias rowLayoutDate: rowLayoutDate
    property int eitype: radioButtonExpense.checked? 1 : 2
    property alias radioButtonExpense: radioButtonExpense
    property alias radioButtonIncome: radioButtonIncome

    function selectCategory(category_id){

        for(var i = 0; i < comboBoxCategory.count; i++){

            var category = categoryModel.get(i);

            if(category._id === category_id){

                comboBoxCategory.currentIndex = i;
                break;
            }
        }
    }

    ListModel{
        id: accountModel
    }

    ListModel{
        id: payeeModel
    }

    ListModel{
        id: categoryModel
    }

    ListModel{
        id: paymentMethodModel
    }

    ListModel{
        id: tagModel
    }

    ColumnLayout{
        id: columnLayout
        anchors.fill: parent
        anchors.margins: 15
        spacing: 6

        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 6

            ExclusiveGroup { id: expenseORIncomeGroup; current: radioButtonExpense }
            RadioButton {
                id: radioButtonExpense
                text: "Expense"
                checked: true
                exclusiveGroup: expenseORIncomeGroup
            }
            RadioButton {
                id: radioButtonIncome
                text: "Income"
                exclusiveGroup: expenseORIncomeGroup
            }
        }

        ColumnLayout{
//            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Account")
                Layout.preferredWidth: 110
//                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 9
            }

            ComboBox{
                id: comboBoxAccount
                model: accountModel
//                Layout.fillWidth: true
//                Layout.preferredWidth: 150
                anchors.left: parent.left
                anchors.right: parent.right
                textRole: "name"
            }
        }

        ColumnLayout{
            id: rowLayoutDate
//            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Date")
                Layout.preferredWidth: 80
//                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 9
            }

            Button{
                id: buttonDate
                anchors.left: parent.left
                anchors.right: parent.right
//                Layout.fillWidth: true
//                Layout.preferredWidth: 150
            }
        }

        ColumnLayout{
//            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Amount")
                Layout.preferredWidth: 110
//                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 9
            }

            TextField{
                id: textFieldAmount
                validator: DoubleValidator{ bottom: 0.0; decimals: 2}
                placeholderText: qsTr("Enter amount here")
                anchors.left: parent.left
                anchors.right: parent.right
//                Layout.fillWidth: true
//                Layout.preferredWidth: 150
            }
        }

        ColumnLayout{
//            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: radioButtonExpense.checked? qsTr("Payee") : qsTr("Payer")
                Layout.preferredWidth: 110
//                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 9
            }

            ComboBox{
                id: comboBoxPayee
                model: payeeModel
                anchors.left: parent.left
                anchors.right: parent.right
//                Layout.fillWidth: true
//                Layout.preferredWidth: 150
                textRole: "name"
            }
        }

        ColumnLayout{
//            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Category")
                Layout.preferredWidth: 110
//                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 9
            }

            ComboBox{
                id: comboBoxCategory
                model: categoryModel
//                Layout.fillWidth: true
                anchors.left: parent.left
                anchors.right: parent.right
//                Layout.preferredWidth: 150
                textRole: "name"
            }
        }

        ColumnLayout{
//            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 6

            Label{
                text: qsTr("Payment Method")
//                Layout.preferredWidth: 110
                anchors.left: parent.left
                anchors.right: parent.right
//                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 9
            }

            ComboBox{
                id: comboBoxPaymentMethod
                model: paymentMethodModel
                anchors.left: parent.left
                anchors.right: parent.right
//                Layout.fillWidth: true
//                Layout.preferredWidth: 150
                textRole: "name"
            }
        }

        ColumnLayout{
//            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 6

            Label{
                text: qsTr("Tag")
                Layout.preferredWidth: 110
                font.pointSize: 9
//                anchors.verticalCenter: parent.verticalCenter
            }

            ComboBox{
                id: comboBoxTag
                model: tagModel
                anchors.left: parent.left
                anchors.right: parent.right
//                Layout.fillWidth: true
//                Layout.preferredWidth: 150
                textRole: "name"
            }
        }

        Item{
            Layout.fillHeight: true
        }
    }
}

