import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "TagDialogScript.js" as TagDialogScript

Dialog{
    id: root
    visible: false
    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant tagdata
    property int contentwidth: 400
    property int contentheight: 400

    function getTagDetails(){

        var tagdetails = new TagDialogScript.TagDetails();
        return tagdetails;
    }

    function setupData(){

        tagDialogContainer.textField.text = tagdata.name;
    }

    function resetDefaults(){

        tagDialogContainer.textField.text = "";

        if(tagdata !== null && tagdata !== undefined)
            setupData();
    }

    TagDialogContainer{
        id: tagDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight
    }

    // Component.onCompleted: resetDefaults()
}

