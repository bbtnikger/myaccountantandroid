import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GroupBox {
    title: qsTr("Account Type Details")
    property alias textField: textField

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 15

        Label{
            text: qsTr("Name")
        }

        TextField{
            id: textField
            placeholderText: qsTr("Enter name here")
            anchors.left: parent.left
            anchors.right: parent.right
        }

        Item{
            Layout.fillHeight: true
        }
    }
}

