import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "SelectTransactionType.js" as SelectTransactionTypeScript
import "../styles"

Dialog{
    id: root
    visible: false
    title: qsTr("Select Transaction Type")

    property int contentwidth: 400
    property int contentheight: 400

    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    function getSelectionOfTransactionTypeDetails(){

        var selection = new SelectTransactionTypeScript.SelectionDetails();
        return selection;
    }

    function resetDefaults(){

//        selectTransactionTypeContainer.
    }

    SelectTransactionTypeContainer{
        id: selectTransactionTypeContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight
    }

//    // Component.onCompleted: resetDefaults()
}

