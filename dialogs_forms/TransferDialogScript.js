function func() {

}

var TransferDetails = function(){

    this.amount = parseFloat(transferDialogContainer.textFieldAmount.text);
    this.rate = parseFloat(transferDialogContainer.textFieldRate.text);
    var dt = calendar.selectedDate;
    this.date = dt;
    this.origin = originAccountModel.get(transferDialogContainer.originAccountCurrentIndex)._id;
    this.destination = destinationAccountModel.get(transferDialogContainer.destinationAccountCurrentIndex)._id;
}

function resetAccounts(){

    originAccountModel.clear();
    destinationAccountModel.clear();

    var list = AccountUtility.getAccounts(ProfileUtility.currentProfile);
    if(list === undefined){

        console.log('list is undefined');
        return;
    }

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];

        originAccountModel.append(record_);
        destinationAccountModel.append(record_);
    }
}

function selectAccountOrigin(origin_id){

    for(var i = 0; i < originAccountModel.count; i++){

        var _id = originAccountModel.get(i)._id;

        if(_id === origin_id){

            transferDialogContainer.originAccountCurrentIndex = i;

            break;
        }
    }
}

function selectAccountDestination(destination_id){

    for(var i = 0; i < destinationAccountModel.count; i++){

        var _id = destinationAccountModel.get(i)._id;

        if(_id === destination_id){

            transferDialogContainer.destinationAccountCurrentIndex = i;

            break;
        }
    }
}
