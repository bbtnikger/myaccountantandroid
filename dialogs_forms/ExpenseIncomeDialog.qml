import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
import "ExpenseIncomeDialogScript.js" as ExpenseIncomeDialogScript
import "../styles"

Dialog{
    id: root
    visible: false

    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property int contentwidth: 400
    property int contentheight: 400

    property variant expenseincomedata
    property alias eitype: expenseIncomeDialogContainer.eitype
    property alias accountModel: expenseIncomeDialogContainer.accountModel
    property alias payeeModel: expenseIncomeDialogContainer.payeeModel
    property alias categoryModel: expenseIncomeDialogContainer.categoryModel
    property alias paymentMethodModel: expenseIncomeDialogContainer.paymentMethodModel
    property alias tagModel: expenseIncomeDialogContainer.tagModel


    function getExpenseIncomeDetails(){

        var expenseincomedetails = new ExpenseIncomeDialogScript.ExpenseIncomeDetails();
        return expenseincomedetails;
    }

    function setupData(){

        expenseIncomeDialogContainer.textFieldAmount.text = LocalUserUtility.localizedNumber(expenseincomedata.amount);
        expenseIncomeDialogContainer.buttonDate.text = LocalUserUtility.localizedDate(expenseincomedata.date);
        expenseIncomeDialogContainer.calendar.selectedDate = expenseincomedata.date;

        var ttype = parseInt(expenseincomedata.ttype);
        if(ttype === 1) { // expense

            expenseIncomeDialogContainer.radioButtonExpense.checked = true;
            console.log('payee('+expenseincomedata.payee._id+')');
            ExpenseIncomeDialogScript.selectPayee(expenseincomedata.payee._id);

        } else{

            expenseIncomeDialogContainer.radioButtonIncome.checked = true;
            console.log('payer('+expenseincomedata.payer._id+')');
            ExpenseIncomeDialogScript.selectPayer(expenseincomedata.payer._id);
        }

        ExpenseIncomeDialogScript.selectAccount(expenseincomedata.account._id);
        ExpenseIncomeDialogScript.selectCategory(expenseincomedata.category._id);
        ExpenseIncomeDialogScript.selectPaymentMethod(expenseincomedata.payment_method._id);
        if(expenseincomedata.tag__id !== undefined)
            ExpenseIncomeDialogScript.selectTag(expenseincomedata.tag._id);
    }

    function resetCurrencies(){

        ExpenseIncomeDialogScript.clearCurrencies();
        ExpenseIncomeDialogScript.setupCurrencies();
    }

    function resetExpenseIncomeTypes(){

        ExpenseIncomeDialogScript.clearExpenseIncomeTypes();
        ExpenseIncomeDialogScript.setupExpenseIncomeTypes();
    }

    function resetDefaults(){

        ExpenseIncomeDialogScript.resetDefaults();

        console.log('reset defaults');

        expenseIncomeDialogContainer.textFieldAmount.text = "";
        var dt = new Date();
        expenseIncomeDialogContainer.calendar.selectedDate = dt;
        expenseIncomeDialogContainer.accountCurrentIndex = 0;
        expenseIncomeDialogContainer.buttonDate.text = calendar.selectedDate.toLocaleDateString()

        if(categoryModel.count > 0)
            expenseIncomeDialogContainer.categoryCurrentIndex = 0;
        if(payeeModel.count > 0)
            expenseIncomeDialogContainer.payeeCurrentIndex = 0;
        if(paymentMethodModel.count > 0)
            expenseIncomeDialogContainer.paymentMethodCurrentIndex = 0;
        if(tagModel.count > 0)
            expenseIncomeDialogContainer.tagCurrentIndex = 0;

        if(expenseincomedata !== null && expenseincomedata !== undefined)
            setupData();
    }

    function selectCategory(category_id){

        expenseIncomeDialogContainer.selectCategory(category_id);
    }

    ExpenseIncomeDialogContainer{
        id: expenseIncomeDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight

        property alias calendar: calendar

        buttonDate.onClicked: calendar.visible = !calendar.visible

        Calendar{
            id: calendar
            visible: false
            x: expenseIncomeDialogContainer.mapFromItem(expenseIncomeDialogContainer.rowLayoutDate,expenseIncomeDialogContainer.buttonDate.x,expenseIncomeDialogContainer.buttonDate.y).x
            y: expenseIncomeDialogContainer.mapFromItem(expenseIncomeDialogContainer.columnLayout,expenseIncomeDialogContainer.rowLayoutDate.x,expenseIncomeDialogContainer.rowLayoutDate.y).y+expenseIncomeDialogContainer.buttonDate.height+6
            style: MACalendarStyle{ }
            onSelectedDateChanged: expenseIncomeDialogContainer.buttonDate.text = calendar.selectedDate.toLocaleDateString()
            onClicked: {

                expenseIncomeDialogContainer.buttonDate.text = selectedDate.toLocaleDateString();
                visible = false;
            }
        }

        Keys.onPressed: if (event.key === Qt.Key_R && (event.modifiers & Qt.ControlModifier)) expenseIncomeDialogContainer.click(StandardButton.Retry)
        Keys.onEnterPressed: expenseIncomeDialogContainer.accept()
        Keys.onEscapePressed: expenseIncomeDialogContainer.reject()
        Keys.onBackPressed: expenseIncomeDialogContainer.reject() // especially necessary on Android
    }

//    // Component.onCompleted: resetDefaults()
}

