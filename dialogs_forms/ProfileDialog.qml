import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "ProfileDialogScript.js" as ProfileDialogScript

Dialog{
    id: root
    visible: false
    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant profiledata
    property int contentwidth: 400
    property int contentheight: 400

    function getProfileDetails(){

        var profiledetails = new ProfileDialogScript.ProfileDetails();
        return profiledetails;
    }

    function setupData(){

        profileDialogContainer.textField.text = profiledata.name;
    }

    function resetDefaults(){

        profileDialogContainer.textField.text = "";

        if(profiledata !== null && profiledata !== undefined)
            setupData();
    }

    ProfileDialogContainer{
        id: profileDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight
    }

    // Component.onCompleted: resetDefaults()
}

