import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GroupBox {
    id: root
    title: qsTr("Select Transaction Type")

    function getTransactionType(){

        if(transactionTypeGroup.current == radioButtonExpenseIncome){

            return 1;

        }else if(transactionTypeGroup.current == radioButtonDeposit){

            return 2;

        }else{

            return 3;
        }
    }

    ColumnLayout{
        id: columnLayout
        anchors.fill: parent
        anchors.margins: 6
        spacing: 6

        Item{
            Layout.fillHeight: true
        }

        ExclusiveGroup { id: transactionTypeGroup; current: radioButtonExpenseIncome }
        RadioButton {
            id: radioButtonExpenseIncome
            text: "Expense / Income"
            checked: true
            exclusiveGroup: transactionTypeGroup
        }
        RadioButton {
            id: radioButtonDeposit
            text: "Deposit"
            exclusiveGroup: transactionTypeGroup
        }
        RadioButton {
            id: radioButtonTransfer
            text: "Transfer"
            exclusiveGroup: transactionTypeGroup
        }

        Item{
            Layout.fillHeight: true
        }
    }
}

