import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GroupBox {
    title: qsTr("Account Details")
    property alias textField: textField
    property ListModel currencyModel
    property alias currencyCurrentIndex: comboBoxCurrency.currentIndex
    property ListModel accountTypeModel
    property alias accountTypeCurrentIndex: comboBoxAccountType.currentIndex

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 15

        ColumnLayout{
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Name")
                Layout.preferredWidth: 80
                font.pointSize: 9
            }

            TextField{
                id: textField
                placeholderText: qsTr("Enter name here")
                anchors.left: parent.left
                anchors.right: parent.right
            }
        }

        ColumnLayout{
            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Account Type")
                Layout.preferredWidth: 80
                font.pointSize: 9
            }

            ComboBox{
                id: comboBoxAccountType
                model: accountTypeModel
                anchors.left: parent.left
                anchors.right: parent.right
                textRole: "name"
            }
        }

        ColumnLayout{
            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Currency")
                Layout.preferredWidth: 80
                font.pointSize: 9
            }

            ColumnLayout{
                spacing: 3
                anchors.left: parent.left
                anchors.right: parent.right

                ComboBox{
                    id: comboBoxCurrency
                    model: currencyModel
                    anchors.left: parent.left
                    anchors.right: parent.right
                    textRole: "code"
                    onCurrentIndexChanged: {

                        if(currencyModel != null)
                            labelCurrencyCountry.text = currencyModel.get(currentIndex).country;
                    }
                }

                Label{
                    id: labelCurrencyCountry
                    color: "green"
                    font.pointSize: 11
                }
            }
        }

        Item{
            Layout.fillHeight: true
        }
    }
}

