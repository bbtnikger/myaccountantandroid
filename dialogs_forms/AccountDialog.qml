import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "AccountDialogScript.js" as AccountDialogScript

Dialog{
    id: root
    visible: false

    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant accountdata
    property int contentwidth: 400
    property int contentheight: 400

    function getAccountDetails(){

        var accountdetails = new AccountDialogScript.AccountDetails();
        return accountdetails;
    }

    function setupData(){

        accountDialogContainer.textField.text = accountdata.name;

        AccountDialogScript.selectCurrency(accountdata.currency);

        AccountDialogScript.selectAccountType(accountdata.account_type);
    }

    function resetCurrencies(){

        AccountDialogScript.clearCurrencies();
        AccountDialogScript.setupCurrencies();
    }

    function resetAccountTypes(){

        AccountDialogScript.clearAccountTypes();
        AccountDialogScript.setupAccountTypes();
    }

    function resetDefaults(){

        accountDialogContainer.textField.text = "";
        accountDialogContainer.currencyCurrentIndex = 0;
        accountDialogContainer.accountTypeCurrentIndex = 0;

        if(accountdata !== null && accountdata !== undefined)
            setupData();

        resetCurrencies();
        resetAccountTypes();
    }

    ListModel{
        id: currencyModel
    }

    ListModel{
        id: accountTypeModel
    }

    AccountDialogContainer{
        id: accountDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight
    }

    // Component.onCompleted: resetDefaults()
}

