import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GroupBox {
    id: root
    title: qsTr("Deposit Details")

    property alias accountModel: accountModel
    property alias textFieldAmount: textFieldAmount
    property alias buttonDate: buttonDate
    property alias columnLayout: columnLayout
    property alias rowLayoutDate: rowLayoutDate
    property alias accountCurrentIndex: comboBoxAccount.currentIndex

    ListModel{
        id: accountModel
    }

    ColumnLayout{
        id: columnLayout
        anchors.fill: parent
        anchors.margins: 9
        spacing: 9

        ColumnLayout{
            spacing: 3
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Account")
            }

            ComboBox{
                id: comboBoxAccount
                model: accountModel
                anchors.left: parent.left
                anchors.right: parent.right
                textRole: "name"
            }
        }

        ColumnLayout{
            id: rowLayoutDate
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Date")
            }

            Button{
                id: buttonDate
                anchors.left: parent.left
                anchors.right: parent.right
            }
        }

        ColumnLayout{
            spacing: 3
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Amount")
            }

            TextField{
                id: textFieldAmount
                validator: DoubleValidator{ bottom: 0.0; decimals: 2}
                placeholderText: qsTr("Enter amount here")
                anchors.left: parent.left
                anchors.right: parent.right
                inputMethodHints: Qt.ImhFormattedNumbersOnly
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 100
        }
    }
}

