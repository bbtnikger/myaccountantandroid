import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GroupBox {
    title: qsTr("Profile Details")
    property alias textField: textField

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 15

        Label{
            text: qsTr("Name")
            font.pointSize: 9
        }

        TextField{
            id: textField
            placeholderText: qsTr("Enter name here")
        }

        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 100
        }
    }
}

