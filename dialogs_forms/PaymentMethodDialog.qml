import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "PaymentMethodDialogScript.js" as PaymentMethodDialogScript

Dialog{
    id: root
    visible: false
    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant paymentmethoddata
    property int contentwidth: 400
    property int contentheight: 400

    function getPaymentMethodDetails(){

        var paymentmethoddetails = new PaymentMethodDialogScript.PaymentMethodDetails();
        return paymentmethoddetails;
    }

    function setupData(){

        paymentMethodDialogContainer.textField.text = paymentmethoddata.name;
    }

    function resetDefaults(){

        paymentMethodDialogContainer.textField.text = "";

        if(paymentmethoddata !== null && paymentmethoddata !== undefined)
            setupData();
    }

    PaymentMethodDialogContainer{
        id: paymentMethodDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight
    }

    // Component.onCompleted: resetDefaults()
}

