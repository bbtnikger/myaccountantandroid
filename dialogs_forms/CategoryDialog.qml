import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "CategoryDialogScript.js" as CategoryDialogScript

Dialog{
    id: root
    visible: false
    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant categorydata
    property int contentwidth: 400
    property int contentheight: 400

    function getCategoryDetails(){

        var categorydetails = new CategoryDialogScript.CategoryDetails();
        return categorydetails;
    }

    function setupData(){

        categoryDialogContainer.textField.text = categorydata.name;
    }

    function resetDefaults(){

        categoryDialogContainer.textField.text = "";

        if(categorydata !== null && categorydata !== undefined)
            setupData();
    }

    CategoryDialogContainer{
        id: categoryDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight
    }

    // Component.onCompleted: resetDefaults()
}

