function func() {

}

var AccountDetails = function(){

    this.name = accountDialogContainer.textField.text;
    this.currency = currencyModel.get(accountDialogContainer.currencyCurrentIndex).code;
    this.account_type = accountTypeModel.get(accountDialogContainer.accountTypeCurrentIndex)._id;
}

function clearCurrencies(){

    currencyModel.clear();
}

function setupCurrencies(){

    var currencylist = SettingsManager.currencyList;
//    console.log('currencylist:'+currencylist);
    if(currencylist === undefined)
        return;

    var len = currencylist.length;
    for(var i = 0; i < len; i++){

        var record_ = currencylist[i];
        currencyModel.append(record_);
    }

    accountDialogContainer.currencyModel = currencyModel;
}

function clearAccountTypes(){

    accountTypeModel.clear();
}

function setupAccountTypes(){

    var list = AccountTypeUtility.getAccountTypes(ProfileUtility.currentProfile);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        accountTypeModel.append(record_);
    }

    accountDialogContainer.accountTypeModel = accountTypeModel;
}

function selectCurrency(code){

    for(var i = 0; i < currencyModel.count; i++){

        var _code = currencyModel.get(i).code;

        if(code === _code){

            accountDialogContainer.currencyCurrentIndex = i;
            break;
        }
    }
}

function selectAccountType(account_type){

    var account_type_id = account_type._id;

    for(var i = 0; i < accountTypeModel.count; i++){

        var _accounttype_id = accountTypeModel.get(i)._id;

        if(account_type_id === _accounttype_id){

            accountDialogContainer.accountTypeCurrentIndex = i;
            break;
        }
    }
}
