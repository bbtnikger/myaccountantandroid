import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "PayeeDialogScript.js" as PayeeDialogScript

Dialog{
    id: root
    visible: false
    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant payeedata
    property int contentwidth: 400
    property int contentheight: 400

    function getPayeeDetails(){

        var payeedetails = new PayeeDialogScript.PayeeDetails();
        return payeedetails;
    }

    function setupData(){

        payeeDialogContainer.textField.text = payeedata.name;
    }

    function resetDefaults(){

        payeeDialogContainer.textField.text = "";

        if(payeedata !== null && payeedata !== undefined)
            setupData();
    }

    PayeeDialogContainer{
        id: payeeDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight
    }

    // Component.onCompleted: resetDefaults()
}

