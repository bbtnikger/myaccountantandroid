import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GroupBox {
    id: root
    title: qsTr("Transfer Details")

    property alias originAccountModel: originAccountModel
    property alias destinationAccountModel: destinationAccountModel
    property alias textFieldAmount: textFieldAmount
    property alias textFieldRate: textFieldRate
    property alias buttonDate: buttonDate
    property alias columnLayout: columnLayout
    property alias rowLayoutDate: rowLayoutDate
    property alias originAccountCurrentIndex: comboBoxOriginAccount.currentIndex
    property alias destinationAccountCurrentIndex: comboBoxDestinationAccount.currentIndex

    ListModel{
        id: originAccountModel
    }

    ListModel{
        id: destinationAccountModel
    }

    ColumnLayout{
        id: columnLayout
        anchors.fill: parent
        anchors.margins: 15
        spacing: 6

        ColumnLayout{
            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Origin Account")
                Layout.preferredWidth: 110
                font.pointSize: 9
            }

            ComboBox{
                id: comboBoxOriginAccount
                model: originAccountModel
                anchors.left: parent.left
                anchors.right: parent.right
                textRole: "name"
            }
        }

        ColumnLayout{
            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Destination Account")
                Layout.preferredWidth: 110
                font.pointSize: 9
            }

            ComboBox{
                id: comboBoxDestinationAccount
                model: destinationAccountModel
                anchors.left: parent.left
                anchors.right: parent.right
                textRole: "name"
            }
        }

        ColumnLayout{
            id: rowLayoutDate
            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Date")
                Layout.preferredWidth: 110
                font.pointSize: 9
            }

            Button{
                id: buttonDate
                anchors.left: parent.left
                anchors.right: parent.right
            }
        }

        ColumnLayout{
            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Amount")
                Layout.preferredWidth: 110
                font.pointSize: 9
            }

            TextField{
                id: textFieldAmount
                validator: DoubleValidator{ bottom: 0.0; decimals: 2}
                placeholderText: qsTr("Enter amount here")
                anchors.left: parent.left
                anchors.right: parent.right
            }
        }

        ColumnLayout{
            Layout.preferredHeight: 40
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            Label{
                text: qsTr("Rate")
                Layout.preferredWidth: 110
                font.pointSize: 9
            }

            TextField{
                id: textFieldRate
                validator: DoubleValidator{ bottom: 0.0; decimals: 6}
                placeholderText: qsTr("Enter transfer rate here")
                anchors.left: parent.left
                anchors.right: parent.right
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 100
        }
    }
}

