import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "DepositDialogScript.js" as DepositDialogScript
import "../styles"

Dialog{
    id: root
    visible: false
    title: qsTr("Deposit Dialog")

    modality: Qt.ApplicationModal
    standardButtons: StandardButton.Save | StandardButton.Cancel

    property variant depositdata
    property alias accountModel: depositDialogContainer.accountModel
    property int contentwidth: 400
    property int contentheight: 400


    function getDepositDetails(){

        var depositdetails = new DepositDialogScript.DepositDetails();
        return depositdetails;
    }

    function setupData(){

        depositDialogContainer.textFieldAmount.text = LocalUserUtility.localizedNumber(depositdata.amount);
        calendar.selectedDate = depositdata.date;
        depositDialogContainer.buttonDate.text = LocalUserUtility.localizedDate(depositdata.date);

        DepositDialogScript.selectAccount(depositdata.account__id);

    }

    function resetDefaults(){

        console.log('reset defaults');

        DepositDialogScript.resetAccounts();

        depositDialogContainer.textFieldAmount.text = "";
        var dt = new Date();
        depositDialogContainer.calendar.selectedDate = dt;
        depositDialogContainer.accountCurrentIndex = 0;
        depositDialogContainer.buttonDate.text = calendar.selectedDate.toLocaleDateString()

        if(depositdata !== null && depositdata !== undefined)
            setupData();
    }

    DepositDialogContainer{
        id: depositDialogContainer
        implicitWidth: contentwidth
        implicitHeight: contentheight

        property alias calendar: calendar

        buttonDate.onClicked: calendar.visible = !calendar.visible

        Calendar{
            id: calendar
            visible: false
            x: depositDialogContainer.mapFromItem(depositDialogContainer.rowLayoutDate,depositDialogContainer.buttonDate.x,depositDialogContainer.buttonDate.y).x
            y: depositDialogContainer.mapFromItem(depositDialogContainer.columnLayout,depositDialogContainer.rowLayoutDate.x,depositDialogContainer.rowLayoutDate.y).y+depositDialogContainer.buttonDate.height+6
            style: MACalendarStyle{ }
            onSelectedDateChanged: depositDialogContainer.buttonDate.text = calendar.selectedDate.toLocaleDateString()
            onClicked: {

                depositDialogContainer.buttonDate.text = selectedDate.toLocaleDateString();
                visible = false;
            }
        }
    }

//    // Component.onCompleted: resetDefaults()
}

