function func() {

}

var DepositDetails = function(){

    this.amount = parseFloat(depositDialogContainer.textFieldAmount.text);
    var dt = calendar.selectedDate;
    this.date = dt;
    this.account = accountModel.get(depositDialogContainer.accountCurrentIndex)._id;
}

function resetAccounts(){

    accountModel.clear();

    var list = AccountUtility.getAccounts(ProfileUtility.currentProfile);
    if(list === undefined){

        console.log('list is undefined');
        return;
    }

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];

        accountModel.append(record_);
    }
}

function selectAccount(account_id){

    for(var i = 0; i < accountModel.count; i++){

        var _id = accountModel.get(i)._id;

        if(_id === account_id){

            depositDialogContainer.accountCurrentIndex = i;

            break;
        }
    }
}
