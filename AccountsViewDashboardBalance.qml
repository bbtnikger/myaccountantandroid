import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import org.qtproject.example 1.0

Item {
    id: root
    width: 100
    height: 62

    property variant balancedata
    property real balanceval: 0.0
    property real expensetotal: 0.0
    property real incometotal: 0.0
    property real deposittotal: 0.0
    property real origintransfertotal: 0.0
    property real destinationtransfertotal: 0.0
    property string currency

    function resetDefaults(){

        balanceval = 0.0;
        expensetotal = 0.0;
        incometotal = 0.0;
        deposittotal = 0.0;
        origintransfertotal = 0.0;
        destinationtransfertotal = 0.0;

        labelDepositTotal.text = '';
        labelDestinationTransferTotal.text = '';
        labelExpenseTotal.text = '';
        labelIncomeTotal.text = '';
        labelTotalBalance.text = '';
        labelOriginTransferTotal.text = '';
    }

    onBalancedataChanged: {

        if(balancedata.expensetotal.length > 0){

            expensetotal = parseFloat(balancedata.expensetotal[0].total);
            balanceval = expensetotal;
        }
        labelExpenseTotal.text = LocalUserUtility.localizedNumber(expensetotal) + " " + currency;

        if(balancedata.incometotal.length > 0){

            incometotal = parseFloat(balancedata.incometotal[0].total);
            balanceval = balanceval + incometotal;
        }
        labelIncomeTotal.text = LocalUserUtility.localizedNumber(incometotal) + " " + currency;

        if(balancedata.deposittotal.length > 0){

            deposittotal = parseFloat(balancedata.deposittotal[0].total);
            balanceval = balanceval + deposittotal;
        }
        labelDepositTotal.text = LocalUserUtility.localizedNumber(deposittotal) + " " + currency;

        if(balancedata.origintransfertotal.length > 0){

            origintransfertotal = parseFloat(balancedata.origintransfertotal[0].total);
            balanceval = balanceval - origintransfertotal;
        }
        labelOriginTransferTotal.text = LocalUserUtility.localizedNumber(origintransfertotal) + " " + currency;

        if(balancedata.destinationtransfertotal.length > 0){

            destinationtransfertotal = parseFloat(balancedata.destinationtransfertotal[0].total);
            balanceval = balanceval + destinationtransfertotal;
        }
        labelDestinationTransferTotal.text = LocalUserUtility.localizedNumber(destinationtransfertotal) + " " + currency;

        labelTotalBalance.text = LocalUserUtility.localizedNumber(balanceval) + " " + currency;
    }

    function getBalanceColor(balance){

        if(balance > 0.0)
            return "green";
        else if (balance < 0.0)
            return "red";
        else
            return "black";
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 9
        spacing: 9

        Label{
            text: qsTr("Totals")
//                font.pointSize: 13
            font.bold: true
        }

        RowLayout{
            spacing: 6
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Expenses")
                width: parent.width / 2
            }

            Label{
                id: labelExpenseTotal
                color: getBalanceColor(expensetotal)
                font.pointSize: 15
                Layout.fillWidth: true
                horizontalAlignment: Qt.AlignRight
            }
        }

        RowLayout{
            spacing: 6
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Incomes")
                width: parent.width / 2
            }

            Label{
                id: labelIncomeTotal
                color: getBalanceColor(incometotal)
                font.pointSize: 15
                Layout.fillWidth: true
                horizontalAlignment: Qt.AlignRight
            }
        }

        RowLayout{
            spacing: 6
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Deposits")
                width: parent.width / 2
            }

            Label{
                id: labelDepositTotal
                color: getBalanceColor(deposittotal)
                font.pointSize: 15
                Layout.fillWidth: true
                horizontalAlignment: Qt.AlignRight
            }
        }

        RowLayout{
            spacing: 6
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Transfers to this account")
                width: parent.width / 2
            }

            Label{
                id: labelDestinationTransferTotal
                color: getBalanceColor(destinationtransfertotal)
                font.pointSize: 15
                Layout.fillWidth: true
                horizontalAlignment: Qt.AlignRight
            }
        }

        Rectangle{
            height: 1
            anchors.left: parent.left
            anchors.right: parent.right
            color: "grey"
        }

        RowLayout{
            spacing: 6
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Transfers from this account")
                width: parent.width / 2
            }

            Label{
                id: labelOriginTransferTotal
                color: getBalanceColor(origintransfertotal*-1)
                font.pointSize: 15
                Layout.fillWidth: true
                horizontalAlignment: Qt.AlignRight
            }
        }

        Rectangle{
            height: 1
            color: "grey"
            anchors.left: parent.left
            anchors.right: parent.right
        }

        RowLayout{
            spacing: 6
            anchors.left: parent.left
            anchors.right: parent.right

            Label{
                text: qsTr("Account balance")
                width: parent.width / 2
            }

            Label{
                id: labelTotalBalance
                font.pointSize: 15
                color: getBalanceColor(balanceval)
                Layout.fillWidth: true
                horizontalAlignment: Qt.AlignRight
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 100
        }
    }
}

