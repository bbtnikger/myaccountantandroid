function func() {

}

function resetHistory(){

//    proxyModel.source = null;
    listView.model = null;

    clearHistory();
    setupHistory();

//    tableView.model = proxyModel;
    listView.model = proxyModel;

//    proxyModel.source = historyModel;
}

function clearHistory(){

    historyModel.clear();
}
function addExpenseIncomeRecordToModel(record_,ttypestring,ttype){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    var acnane = record_.account.name;
    record_.accountname = acnane;
    record_.type = ttypestring;
    var catname = record_.category.name;
    record_.description = catname;
    var accountcurrency = record_.account.currency;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.ttype = ttype;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    historyModel.append(record_);
}

function updateExpenseIncomeRecordToModel(record_,ttypestring,ttype){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    record_.accountname = record_.account.name;
    record_.type = ttypestring;
    record_.transactionname = record_.destination.name;
    record_.ttype = ttype;
    var accountcurrency = record_.account.currency;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + record_.account.currency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    var idx = findHistoryIndex(record_._id);
    if(idx !== -1)
        historyModel.set(idx,record_);
}

function addTransferRecordToModel(record_){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    var acname = record_.origin.name;
    record_.accountname = acname;
    record_.type = typeTransfers;
    record_.ttype = 4;
    var destname = record_.destination.name;
    record_.description = destname;
    var accountcurrency = record_.origin.currency;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    historyModel.append(record_);
}

function updateTransferRecordToModel(record_){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    record_.accountname = record_.origin.name;
    record_.type = typeTransfers;
    record_.transactionname = record_.destination.name;
    record_.ttype = 4;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + record_.origin.currency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    var idx = findHistoryIndex(record_._id);
    if(idx !== -1)
        historyModel.set(idx,record_);
}

function addDepositRecordToModel(record_){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    record_.accountname = record_.account.name;
    record_.type = typeDeposit;
    record_.ttype = 3;
    record_.description = record_.memo;
    var accountcurrency = record_.account.currency;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    historyModel.append(record_);
}

function updateDepositRecordToModel(record_){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    record_.accountname = record_.account.name;
    record_.type = typeDeposit;
    record_.transactionname = record_.memo;
    record_.ttype = 3;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + record_.account.currency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    var idx = findHistoryIndex(record_._id);
    if(idx !== -1)
        historyModel.set(idx,record_);
}

function processExpenses(expenses){

    var len = expenses.length;
    for(var i = 0; i < len; i++){

        var record_ = expenses[i];

        addExpenseIncomeRecordToModel(record_,typeExpenses,1);
    }
}

function processIncomes(incomes){

    var len = incomes.length;
    for(var i = 0; i < len; i++){

        var record_ = incomes[i];

        addExpenseIncomeRecordToModel(record_,typeIncome,2);
    }
}

function processTransfers(transfers){

    var len = transfers.length;
    for(var i = 0; i < len; i++){

        var record_ = transfers[i];

        addTransferRecordToModel(record_);
    }
}

function processDeposits(deposits){

    var len = deposits.length;
    for(var i = 0; i < len; i++){

        var record_ = deposits[i];

        addDepositRecordToModel(record_);
    }
}

function setupAccountHistory(accounthistory){

    var transactions = accounthistory.transactions;

    listView.model = null;

    var expenses = transactions.expenses;
    if(expenses !== undefined)
        processExpenses(expenses);

    var incomes = transactions.incomes;
    if(incomes !== undefined)
        processIncomes(incomes);

    var deposits = transactions.deposits;
    if(deposits !== undefined)
        processDeposits(deposits);

    var transfers = transactions.transfers;
    if(transfers !== undefined){

        var origintransfers = transfers.origin;
        var destinationtransfers = transfers.destination;

        processTransfers(origintransfers);
        processTransfers(destinationtransfers);
    }

    listView.model = proxyModel;
}

function calculateBalance(balancedata){

    var expensetotal = 0.0;
    var accountbalanceval = 0.0
    if(balancedata.expensetotal.length > 0){

        expensetotal = parseFloat(balancedata.expensetotal[0].total);
        accountbalanceval = expensetotal;
    }

    var incometotal = 0.0;
    if(balancedata.incometotal.length > 0){

        incometotal = parseFloat(balancedata.incometotal[0].total);
        accountbalanceval = accountbalanceval + incometotal;
    }

    var deposittotal = 0.0;
    if(balancedata.deposittotal.length > 0){

        deposittotal = parseFloat(balancedata.deposittotal[0].total);
        accountbalanceval = accountbalanceval + deposittotal;
    }

    var origintransfertotal = 0.0;
    if(balancedata.origintransfertotal.length > 0){

        origintransfertotal = parseFloat(balancedata.origintransfertotal[0].total);
        accountbalanceval = accountbalanceval - origintransfertotal;
    }

    var destinationtransfertotal = 0.0;
    if(balancedata.destinationtransfertotal.length > 0){

        destinationtransfertotal = parseFloat(balancedata.destinationtransfertotal[0].total);
        accountbalanceval = accountbalanceval + destinationtransfertotal;
    }

    return accountbalanceval;
}

function setupHistory(){

    var historyobject = HistoryUtility.getHistory(ProfileUtility.currentProfile);
    if(historyobject === undefined)
        return;

    for(var prop in historyobject){

        console.log('prop('+prop+')');

        var accounthistory = historyobject[prop];

        setupAccountHistory(accounthistory);

        var accountbalanceobject = accounthistory.balance;
        var accountbalance = calculateBalance(accountbalanceobject);
        console.log('accountbalance('+accountbalance+')');

        balanceval = balanceval + accountbalance;
    }
}

function openTransactionDetails(){


}

function onTransactionTypeSelected(){

    busyIndicator.running = true;

    var selection = selectTransationTypeDialog.getSelectionOfTransactionTypeDetails();

    var transactiontype = selection.transactiontype;
    if(transactiontype === 1){ // expense or income

        startAddExpenseIncomeDialog();

    }else if( transactiontype === 2){ // deposit

        startAddDepositDialog();

    }else{ // transfer

        startAddTransferDialog();
    }

    busyIndicator.running = false;
}

function startEditTransaction(){

    var ttype = parseInt(currenthistorylistitem.ttype);

    console.log('Start Edit Process ttype('+ttype+')');

    if(ttype === 1 || ttype === 2){ // expense or income

        startUpdateExpenseIncomeDialog();

    }else if(ttype === 3){ // deposits

        startUpdateDepositDialog();

    }else if(ttype === 4){ // transfer

        startUpdateTransferDialog();
    }
}

function onAddExpenseIncomeDialogAccepted(){

    busyIndicator.running = true;

    var expenseincomedata = expenseIncomeDialog.getExpenseIncomeDetails();
    var profile_id = ProfileUtility.currentProfile;
    expenseincomedata.profile_id = profile_id;
    var eitype = expenseIncomeDialog.eitype;
    var expenseincomedatanew = null;
    var typeString_ = typeExpenses;
    if(eitype === 1){ // expense

        expenseincomedatanew = ExpenseUtility.addExpense(expenseincomedata);

    }else{ // income

        expenseincomedatanew = IncomeUtility.addIncome(expenseincomedata);
    }

    if(expenseincomedatanew !== null){

        showInformationMessage(qsTr("Transaction added successfully"));

        addExpenseIncomeRecordToModel(expenseincomedatanew,typeString_,eitype);
    }

    busyIndicator.running = false;
}

function onUpdateExpenseIncomeDialogAccepted(){

    busyIndicator.running = true;

    var expenseincomedata = expenseIncomeDialog.getExpenseIncomeDetails();
    var _id = currenthistorylistitem._id;
    var profile_id = ProfileUtility.currentProfile;
    expenseincomedata.profile_id = profile_id;
    var eitype = expenseIncomeDialog.eitype;
    var expenseincomedatanew = null;
    var typeString_ = typeExpenses;
    var previous_ttype = parseInt(currenthistorylistitem.ttype);
    var newrecord = false;

    if(eitype === 1){ // expense

        if(eitype === previous_ttype)
            expenseincomedatanew = ExpenseUtility.updateExpense(_id,expenseincomedata);
        else{

            // delete the income record
            var res = IncomeUtility.deleteIncome(_id);
            if(res){

                // remove the record from the model
                var idx = findHistoryIndex(_id);
                if(idx !== -1)
                    historyModel.remove(idx);

                // add new expense record
                expenseincomedatanew = ExpenseUtility.addExpense(expenseincomedata);

                newrecord = true;
            }
        }

    }else{ // income

        if(eitype === previous_ttype){

            expenseincomedatanew = IncomeUtility.updateIncome(_id,expenseincomedata);

        }else{

            var res2 = ExpenseUtility.deleteExpense(_id);
            if(res2){

                console.log('Expense deleted successfully');

                // remove the record from the model
                var idx2 = findHistoryIndex(_id);
                if(idx2 !== -1)
                    historyModel.remove(idx2);

                // add new expense record
                expenseincomedatanew = IncomeUtility.addIncome(expenseincomedata);

                newrecord = true;
            }
        }
    }

    if(expenseincomedatanew !== null){

        showInformationMessage(qsTr("Transaction updated successfully"));

        if(!newrecord)
            updateExpenseIncomeRecordToModel(expenseincomedatanew,typeString_,eitype);
        else
            addExpenseIncomeRecordToModel(expenseincomedatanew,typeString_,eitype);
    }

    busyIndicator.running = false;
}

function startAddExpenseIncomeDialog(){

    busyIndicator.running = true;

    expenseIncomeDialog.contentheight = stackView.height-100;
    expenseIncomeDialog.contentwidth = stackView.width-100;
    expenseIncomeDialog.expenseincomedata = null;
    expenseIncomeDialog.resetDefaults();
    expenseIncomeDialog.title = qsTr("Add Expense/Income Dialog");
    expenseIncomeDialog.accepted.disconnect(onAddExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.disconnect(onUpdateExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.connect(onAddExpenseIncomeDialogAccepted);
    expenseIncomeDialog.open();

    busyIndicator.running = false;
}

function startUpdateExpenseIncomeDialog(){

    busyIndicator.running = true;

    expenseIncomeDialog.contentheight = stackView.height-100;
    expenseIncomeDialog.contentwidth = stackView.width-100;
    expenseIncomeDialog.expenseincomedata = currenthistorylistitem;
    expenseIncomeDialog.resetDefaults();
    expenseIncomeDialog.title = qsTr("Update Expense/Income Dialog");
    expenseIncomeDialog.accepted.disconnect(onAddExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.disconnect(onUpdateExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.connect(onUpdateExpenseIncomeDialogAccepted);
    expenseIncomeDialog.open();

    busyIndicator.running = false;
}

function onAddTransferDialogAccepted(){

    busyIndicator.running = true;

    var transferdata = transferDialog.getTransferDetails();
    var profile_id = ProfileUtility.currentProfile;
    transferdata.profile_id = profile_id;
    var transferdatanew = TransferUtility.addTransfer(transferdata);

    if(transferdatanew !== null && transferdatanew !== undefined){

        showInformationMessage(qsTr("Transfer added successfully"));

        addTransferRecordToModel(transferdatanew);
    }

    busyIndicator.running = false;
}

function onUpdateTransferDialogAccepted(){

    busyIndicator.running = true;

    var transferdata = transferDialog.getTransferDetails();
    var _id = currenthistorylistitem._id;
    var profile_id = ProfileUtility.currentProfile;
    transferdata.profile_id = profile_id;
    var transferdatanew = TransferUtility.updateTransfer(_id,transferdata);

    if(transferdatanew !== null && transferdatanew !== undefined){

        showInformationMessage(qsTr("Transfer updated successfully"));

        updateTransferRecordToModel(transferdatanew);
    }

    busyIndicator.running = false;
}

function startAddTransferDialog(){

    busyIndicator.running = true;

    transferDialog.contentheight = stackView.height-100;
    transferDialog.contentwidth = stackView.width-100;
    transferDialog.transferdata = null;
    transferDialog.resetDefaults();
    transferDialog.title = qsTr("Transfer Dialog");
    transferDialog.accepted.disconnect(onAddTransferDialogAccepted);
    transferDialog.accepted.disconnect(onUpdateTransferDialogAccepted);
    transferDialog.accepted.connect(onAddTransferDialogAccepted);
    transferDialog.open();

    busyIndicator.running = false;
}

function startUpdateTransferDialog(){

    busyIndicator.running = true;

    transferDialog.contentheight = stackView.height-100;
    transferDialog.contentwidth = stackView.width-100;
    transferDialog.transferdata = currenthistorylistitem;
    transferDialog.resetDefaults();
    transferDialog.title = qsTr("Transfer Dialog");
    transferDialog.accepted.disconnect(onAddTransferDialogAccepted);
    transferDialog.accepted.disconnect(onUpdateTransferDialogAccepted);
    transferDialog.accepted.connect(onUpdateTransferDialogAccepted);
    transferDialog.open();

    busyIndicator.running = false;
}


function onAddDepositDialogAccepted(){

    busyIndicator.running = true;

    var depositdata = depositDialog.getDepositDetails();
    var profile_id = ProfileUtility.currentProfile;
    depositdata.profile_id = profile_id;
    var depositdatanew = DepositUtility.addDeposit(depositdata);

    if(depositdatanew !== null && depositdatanew !== undefined){

        showInformationMessage(qsTr("Deposit added successfully"));

        addDepositRecordToModel(depositdatanew);
    }

    busyIndicator.running = false;
}

function onUpdateDepositDialogAccepted(){

    busyIndicator.running = true;

    var depositdata = depositDialog.getDepositDetails();
    var _id = currenthistorylistitem._id;
    var profile_id = ProfileUtility.currentProfile;
    depositdata.profile_id = profile_id;
    var depositdatanew = DepositUtility.updateDeposit(_id,depositdata);

    if(depositdatanew !== null && depositdatanew !== undefined){

        showInformationMessage(qsTr("Deposit updated successfully"));

        updateDepositRecordToModel(depositdatanew);
    }

    busyIndicator.running = false;
}

function startAddDepositDialog(){

    busyIndicator.running = true;

    depositDialog.contentheight = stackView.height-100;
    depositDialog.contentwidth = stackView.width-100;
    depositDialog.depositdata = null;
    depositDialog.resetDefaults();
    depositDialog.title = qsTr("Transfer Dialog");
    depositDialog.accepted.disconnect(onAddDepositDialogAccepted);
    depositDialog.accepted.disconnect(onUpdateDepositDialogAccepted);
    depositDialog.accepted.connect(onAddDepositDialogAccepted);
    depositDialog.open();

    busyIndicator.running = false;
}

function startUpdateDepositDialog(){

    busyIndicator.running = true;

    depositDialog.contentheight = stackView.height-100;
    depositDialog.contentwidth = stackView.width-100;
    depositDialog.depositdata = proxyModel.get(currenthistoryindex);
    depositDialog.resetDefaults();
    depositDialog.title = qsTr("Deposit Dialog");
    depositDialog.accepted.disconnect(onAddDepositDialogAccepted);
    depositDialog.accepted.disconnect(onUpdateDepositDialogAccepted);
    depositDialog.accepted.connect(onUpdateDepositDialogAccepted);
    depositDialog.open();

    busyIndicator.running = false;
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = currenthistorylistitem;

    var _id = recorddata._id;

    var ttype = 1;
    ttype = recorddata.ttype;

    var res = false;

    if(ttype == 1){ // expense

        res = ExpenseUtility.deleteExpense(_id);

    }else if(ttype == 2){ // income

        res = IncomeUtility.deleteIncome(_id);

    }else if(ttype == 3){ // deposit

        res = DepositUtility.deleteDeposit(_id);

    }else if(ttype == 4){ // transfer

        res = TransferUtility.deleteTransfer(_id);
    }

    if(res){

        historyModel.remove(currenthistoryindex);

        showInformationMessage(qsTr("Transaction deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeleteTransaction(){

    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete the transaction. Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.");
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}


function startAddTransaction(){

    selectTransationTypeDialog.contentheight = stackView.height-100;
    selectTransationTypeDialog.contentwidth = stackView.width-100;
    selectTransationTypeDialog.resetDefaults();
    selectTransationTypeDialog.open();
}

function findHistoryIndex(check_id){

    for(var i = 0; i < historyModel.count; i++){

        var record_id = historyModel.get(i)._id;

        if(record_id === check_id){

            return i;
        }
    }

    return -1;
}

function openDetailsPage(){

    var idx = findHistoryIndex(currenthistorylistitem._id);
    var currenthistorydata = historyModel.get(idx);

    var ttype = parseInt(currenthistorylistitem.ttype);
    console.log('item.ttype('+ttype+') currentIndex('+currenthistoryindex+') idx('+idx+') currenthistorylistitem._id('+currenthistorylistitem._id+")");

    if(ttype === 1 || ttype === 2){ // expense or income

        stackView.push({item:expenseIncomeComponent,properties:{expenseincomedata:currenthistorydata}});

    }else if(ttype === 3){ // deposits

        stackView.push({item:depositComponent,properties:{depositdata:currenthistorydata}});

    }else if(ttype === 4){ // transfer

        stackView.push({item:transferComponent,properties:{transferdata:currenthistorydata}});
    }
}
