import QtQuick 2.5
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import "LoginScript.js" as LoginEngine

Item{
    id: root
    visible: true
//    modality: Qt.ApplicationModal
//    title: qsTr("Login")
//    flags: Qt.Window

    signal accepted()

    function getConnectionDetails(){

        var connectiondetails_ = new LoginEngine.ConnectionDetails();
        return connectiondetails_;
    }


    LoginDialogContent{
        id: loginDialogContent
        implicitWidth: stackView.width
        implicitHeight: stackView.height
//        anchors.fill: parent
        buttonOK.onClicked: accepted()
    }


//    standardButtons: StandardButton.Ok | StandardButton.Cancel

    onAccepted: {

        LocalUserUtility.username = loginDialogContent.username;
        LocalUserUtility.password = loginDialogContent.password;
        LocalUserUtility.organizationId = loginDialogContent.organizationId;
//        root.visible = false;
    }

//    onVisibleChanged: {

//        if(visible){

//            var urname = LocalUserUtility.username;
//            var pass = LocalUserUtility.password;
//            var organizationId = LocalUserUtility.organizationId;

//            console.log('username('+urname+'),password('+pass+'),organizationId('+organizationId+')');

//            loginDialogContent.username = urname;
//            loginDialogContent.password = pass;
//            loginDialogContent.organizationId = organizationId;
//        }
//    }

    Component.onCompleted: {

        var urname = LocalUserUtility.username;
        var pass = LocalUserUtility.password;
        var organizationId = LocalUserUtility.organizationId;

        console.log('username('+urname+'),password('+pass+'),organizationId('+organizationId+')');

        loginDialogContent.username = urname;
        loginDialogContent.password = pass;
        loginDialogContent.organizationId = organizationId;
    }
}

