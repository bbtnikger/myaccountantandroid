import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "dialogs_forms"

Item {
    id: root

    property variant expenseincomedata

    function setup(){

        var tdate = expenseincomedata.date;
        var datestring = LocalUserUtility.localizedDateFromString(expenseincomedata.date);
        dateLabel.text = datestring;

        var ttype = parseInt(expenseincomedata.ttype);
        console.log('ttype('+ttype+')');
        var pname = "";
        if(expenseincomedata.ttype === 1 ){

            labelPayeeTitle.text = qsTr("Payee");

            if(expenseincomedata.payee !== undefined){

                pname = expenseincomedata.payee.name;
            }

        } else{

            labelPayeeTitle.text = qsTr("Payer");

            if(expenseincomedata.payer !== undefined){

                pname = expenseincomedata.payer.name;
            }
        }
        payeeLabel.text = pname;

        var category = expenseincomedata.category.name;
        categoryLabel.text = category;

        var acname = expenseincomedata.account.name;
        accountLabel.text = acname;

        var tmname = expenseincomedata.payment_method.name;
        paymentMethodLabel.text = tmname;

        var transactionType = ttype === 1? qsTr("Expenses") : qsTr("Income");
        transactionTypeLabel.text = transactionType;

        var cleareddescription = ReconcileUtility.clearedName(expenseincomedata.cleared);
        transactionStatusLabel.text = cleareddescription;

        var amountstring = LocalUserUtility.localizedNumber(expenseincomedata.amount) + " " + expenseincomedata.account.currency;
        amountLabel.text = amountstring;

        if(expenseincomedata.tag !== undefined){

//            var tagid = expenseincomedata.tag._id;
            var tagname = expenseincomedata.tag.name;
            tagLabel.text = tagname;
        }

//        page.transactionUpdated.connect(resetPage);
    }

    function onUpdateExpenseIncomeDialogAccepted(){

        busyIndicator.running = true;

        var expenseincomedata_ = expenseIncomeDialog.getExpenseIncomeDetails();
        var _id = expenseincomedata._id;
        var profile_id = ProfileUtility.currentProfile;
        expenseincomedata_.profile_id = profile_id;
        var eitype = expenseIncomeDialog.eitype;
        var expenseincomedatanew = null;
        var typeString_ = typeExpenses;
        var previous_ttype = expenseincomedata.ttype;
        console.log('previous_ttype('+previous_ttype+') eitype('+eitype+')');

        var newrecord = false;

        if(eitype === 1){ // expense

            if(eitype === previous_ttype)
                expenseincomedatanew = ExpenseUtility.updateExpense(_id,expenseincomedata_);
            else{

                // delete the income record
                var res = IncomeUtility.deleteIncome(_id);
                if(res){

                    // remove the record from the model
                    var idx = findHistoryIndex(_id);
                    if(idx !== -1)
                        historyModel.remove(idx);

                    // add new expense record
                    expenseincomedatanew = ExpenseUtility.addExpense(expenseincomedata_);

                    newrecord = true;
                }
            }

        }else{ // income

            if(eitype === previous_ttype){

                expenseincomedatanew = IncomeUtility.updateIncome(_id,expenseincomedata_);

            }else{

                var res2 = ExpenseUtility.deleteExpense(_id);
                if(res2){

                    console.log('Expense deleted successfully');

                    // remove the record from the model
                    var idx2 = findHistoryIndex(_id);
                    if(idx2 !== -1)
                        historyModel.remove(idx2);

                    // add new expense record
                    expenseincomedatanew = IncomeUtility.addIncome(expenseincomedata_);

                    newrecord = true;
                }
            }

            typeString_ = typeIncome;
        }

        if(expenseincomedatanew !== null){

            showInformationMessage(qsTr("Transaction updated successfully"));
            console.log(qsTr("Transaction updated successfully"));

            console.log('expenseincomedatanew.account._id('+expenseincomedatanew.account._id+') newrecord('+newrecord+')');

            expenseincomedata = expenseincomedatanew;

            setup();

            expenseIncomeDialog.close();
        }

        busyIndicator.running = false;
    }

    function openEditDialog(){

        busyIndicator.running = true;

        expenseIncomeDialog.contentheight = root.height-100;
        expenseIncomeDialog.contentwidth = root.width-100;
        expenseIncomeDialog.expenseincomedata = expenseincomedata;
        expenseIncomeDialog.resetDefaults();
        expenseIncomeDialog.open();

        busyIndicator.running = false;
    }

    ExpenseIncomeDialog{
        id: expenseIncomeDialog
        title: qsTr("Update Expense/Income Dialog");

        onAccepted: onUpdateExpenseIncomeDialogAccepted()
    }

    ColumnLayout{
        anchors.top: parent.top
        anchors.bottom: toolbar.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 9
        spacing: 9

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Transaction type")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: transactionTypeLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Date")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: dateLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Account")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: accountLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                id: categoryTitleLabel
                font.pointSize: 11
                font.italic: true
                text: qsTr("Category")
            }

            Label {
                id: categoryLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Amount")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: amountLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                id: labelPayeeTitle
                font.pointSize: 11
                font.italic: true
            }

            Label {
                id: payeeLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Tag")
                font.pointSize: 11
                font.italic: true
            }

            Label {
                id: tagLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                font.pointSize: 11
                text: qsTr("Payment method")
                font.italic: true
            }

            Label {
                id: paymentMethodLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                font.pointSize: 11
                text: qsTr("Transaction status")
                font.italic: true
            }

            Label {
                id: transactionStatusLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                font.pointSize: 11
                text: qsTr("Description")
                font.italic: true
            }

            Label {
                id: descriptionLabel
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 10
        }
    }

    ToolBar {
        id: toolbar
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        RowLayout {
            spacing: 6
            anchors.fill: parent

            ToolButton{
                tooltip: qsTr("Back")
                iconSource: "qrc:/images/ic_previous_64x64.png"
                onClicked: stackView.pop()
            }

            Item{
                Layout.fillWidth: true
                Layout.preferredHeight: 10
            }

            ToolButton{
                tooltip: qsTr("Edit")
                iconSource: "qrc:/images/ic_edit_64x64.png"
                onClicked: openEditDialog()
            }

            Item{
                Layout.fillWidth: true
                Layout.preferredHeight: 10
            }
        }
    }

    Component.onCompleted: setup()
}

