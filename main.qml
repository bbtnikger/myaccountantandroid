import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import "Main.js" as MainScript
import "dialogs_forms"

ApplicationWindow{
    id: root
    title: qsTr("MyAccountant Cloud")
    width: 800
    height: 640
    visible: true
    minimumWidth: 640
    minimumHeight: 480

    ToastMessage{
        id: toastMessage
    }

    MessageDialog{
        id: messageDialog
    }

    function showInformationMessage(message){

        var tmsg = toastMessage.createObject(0,{"body_":message});

    }

    menuBar: MenuBar {

        Menu {
            title: qsTr("&Login")
            MenuItem {
                text: qsTr("&Login")
                enabled: stackView.depth>1 || busyIndicator.running? false: true
                onTriggered: stackView.push({item:loginComponent})
            }
            MenuItem{
                text: qsTr("Logout")
                enabled: stackView.depth>1 || busyIndicator.running? false: true
                onTriggered: LoginUtility.sendRequestToLogout()
            }
            MenuItem{
                text: qsTr("Setup")
                enabled: stackView.depth>1 || busyIndicator.running? false: true
            }
            MenuItem {
                text: qsTr("E&xit")
                enabled: stackView.depth>1 || busyIndicator.running? false: true
                onTriggered: Qt.quit();
            }
        }
    }

    Item{
        anchors.fill: parent

        StackView{
            id: stackView
            anchors.fill: parent
            initialItem: tabviewComponent

            Component.onCompleted: stackView.push({item:loginComponent})
        }

        Component{
            id: loginComponent

            LoginScreen{
                id: loginScreen

                onAccepted: {

                    busyIndicator.running = true;

                    var connectiondetails_ = getConnectionDetails();

                    var username = connectiondetails_.username;
                    var password = connectiondetails_.password;
                    var organizationId = connectiondetails_.organizationId;

                    LocalUserUtility.username = username;
                    LocalUserUtility.password = password;
                    LocalUserUtility.organizationId = organizationId;

                    var res = LoginUtility.sendRequestToLogin(username,password,organizationId);

                    busyIndicator.running = false;
                }
            }
        }

        Component{
            id: tabviewComponent

            Item{
                opacity: busyIndicator.running? 0.8 : 1.0
                enabled: busyIndicator.running? false : true

                TabView{
                    id: tabView
                    anchors.fill: parent

                    onCurrentIndexChanged: {

                        busyIndicator.running = true;
                        getTab(currentIndex).item.resetPage();
                        busyIndicator.running = false;
                    }

                    Tab {
                        title: qsTr("Accounts")
                        AccountsView{ }
                    }
                    Tab {
                        title: qsTr("Categories")
                        CategoriesView{ }
                    }
                    Tab {
                        title: qsTr("History")
                        HistoryView{ }
                    }
                    Tab{
                        title: qsTr("Manage")
                        ManageTypesView{ }
                    }

        //            Tab{
        //                title: qsTr("Budgets")
        //                Rectangle{ color: "orange" }
        //            }
//                    Tab{
//                        title: qsTr("Tools")
//                        ToolsView{ }
//                    }
                }

                function onLoginFailed(){

                    busyIndicator.running = false;
                }

                function onLoginSuccess(){

                    var list = ProfileUtility.getProfiles();
                    if(list === undefined)
                        return;

                    var len = list.length;
                    if(len>0){

                        var profile = list[0];
                        ProfileUtility.currentProfile = profile._id;
                    }

                    tabView.getTab(0).item.resetPage();

                    stackView.pop();
                }

                function showInformationMessage(message){

//                    var tmsg = toastMessage.createObject(0,{"body_":message});
                    messageDialog.title = qsTr("Information");
                    messageDialog.text = message;
                    messageDialog.open();

                }

                ToastMessage{
                    id: toastMessage
                }

                MessageDialog{
                    id: messageDialog
                }

                Component.onCompleted: {

                    LoginUtility.loginSuccess.connect(onLoginSuccess);
                    LoginUtility.loginFailed.connect(onLoginFailed);
                }
            }
        }

        BusyIndicator{
            id: busyIndicator
            anchors.centerIn: parent
            running: false
            width: 128
            height: 128
        }
    }
}
