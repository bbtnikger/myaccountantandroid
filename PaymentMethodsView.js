function func() {

}

function onAddPaymentMethodDialogAccepted(){

    busyIndicator.running = true;

    var paymentmethoddata = paymentMethodDialog.getPaymentMethodDetails();
    var profile_id = ProfileUtility.currentProfile;
    paymentmethoddata.profile_id = profile_id;
    var payementmethoddatanew = PaymentMethodUtility.addPaymentMethod(paymentmethoddata);

    if(payementmethoddatanew !== null){

        paymentMethodsViewList.model.append(payementmethoddatanew);

        showInformationMessage(qsTr("Payment Method added successfully"));
    }

    busyIndicator.running = false;
}

function onUpdatePaymentMethodDialogAccepted(){

    busyIndicator.running = true;

    var paymentmethoddetails = paymentMethodDialog.getPaymentMethodDetails();
    var _id = paymentMethodsViewList.currentpaymentmethodlistitem._id;
    var paymentmethoddatanew = PaymentMethodUtility.updatePaymentMethod(_id,paymentmethoddetails);

    if(paymentmethoddatanew !== null){

        paymentMethodsViewList.model.set(paymentMethodsViewList.currenctpaymentmethodindex,paymentmethoddatanew);

        showInformationMessage(qsTr("Payment Method updated successfully"));
    }

    busyIndicator.running = false;
}

function startAddPaymentMethodDialog(){

    paymentMethodDialog.contentheight = stackView.height-100;
    paymentMethodDialog.contentwidth = stackView.width-100;
    paymentMethodDialog.paymentmethoddata = undefined;
    paymentMethodDialog.resetDefaults();
    paymentMethodDialog.title = qsTr("New PaymentMethod Dialog");
    paymentMethodDialog.accepted.disconnect(onAddPaymentMethodDialogAccepted);
    paymentMethodDialog.accepted.disconnect(onUpdatePaymentMethodDialogAccepted);
    paymentMethodDialog.accepted.connect(onAddPaymentMethodDialogAccepted);
    paymentMethodDialog.open();
}

//var PaymentMethodData = function(paymentMethodlistitem){

//    this.
//}

function startUpdatePaymentMethodDialog(){

//    var paymentmethoddata = new PaymentMethodData(paymentMethodsViewList.currentpaymentMethodlistitem);
//    paymentMethodDialog.paymentmethoddata

    paymentMethodDialog.paymentmethoddata = paymentMethodsViewList.currentpaymentmethodlistitem;
    paymentMethodDialog.resetDefaults();
    paymentMethodDialog.contentheight = stackView.height;
    paymentMethodDialog.contentwidth = stackView.width;
    paymentMethodDialog.title = qsTr("Update PaymentMethod Dialog");
    paymentMethodDialog.accepted.disconnect(onAddPaymentMethodDialogAccepted);
    paymentMethodDialog.accepted.disconnect(onUpdatePaymentMethodDialogAccepted);
    paymentMethodDialog.accepted.connect(onUpdatePaymentMethodDialogAccepted);
    paymentMethodDialog.open();
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = paymentMethodsViewList.currentpaymentmethodlistitem;
    var _id = recorddata._id;
    var res = PaymentMethodUtility.deletePaymentMethod(_id);

    if(res){

        paymentMethodsViewList.model.remove(paymentMethodsViewList.currenctpaymentmethodindex);

        showInformationMessage(qsTr("Payment Method deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeletePaymentMethod(){

    var title_ = paymentMethodsViewList.currentpaymentmethodlistitem.name;
    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete ("+title_+'). Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.');
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}
