import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "ProfileView.js" as ProfileViewScript
import "dialogs_forms"

Item {
    id: root

    property alias profilelistitem: profileViewTabList.profilelistitem
    property alias profile_id: profileViewTabList.profile_id

    onProfilelistitemChanged: {

        console.log('[ProfileView]-profilelistitem('+profilelistitem+') profile_id('+profile_id+')')

        profileViewTabList.resetTabs();
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    RowLayout{
        id: rowLayout
        anchors.fill: parent
        spacing: 0

        ProfileViewTabList{
            id: profileViewTabList

            Layout.minimumWidth: 200
            Layout.maximumWidth: 300
            Layout.preferredWidth: 200
            Layout.fillHeight: true
            profilelistitem: root.profilelistitem
            profile_id: root.profile_id
            onCurrenttabindexChanged: ProfileViewScript.onCurrentTabIndexChanged(currenttabindex)
        }

        Loader{
            id: loader
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.topMargin: 1
            anchors.bottomMargin: 1
            Layout.fillWidth: true
        }
    }
}

