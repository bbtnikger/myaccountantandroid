import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import "PayeesViewList.js" as PayeesViewListScript

Item {
    id: root

    property variant currentpayeelistitem
    property alias currentpayeeindex: listView.currentIndex
    property alias model: listModel

    signal addPayee()
    signal editPayee()
    signal deletePayee()

    ListModel{
        id: listModel
    }

    Menu{
        id: menu
        MenuItem{
            text: qsTr("Edit")
            onTriggered: listView.startEditProcess()
        }
        MenuItem{
            text: qsTr("Delete")
            onTriggered: listView.startDeleteProcess()
        }
    }

    Component{
        id: itemDelegate
        Item{
            width: listView.width
            height: 110

            MouseArea {
                anchors.fill: parent
                onClicked: listView.currentIndex = index
                onPressAndHold: { listView.currentIndex = index; menu.popup(); }
            }

            Rectangle{
                anchors.fill: parent
                color: "#000000FF"
                border.color: "grey"
            }

            RowLayout{
                anchors.fill: parent
                anchors.margins: 6

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: name
//                    font.pointSize: 11
                    Layout.fillWidth: true
                }

//                Image {
//                    id: img
//                    fillMode: Image.PreserveAspectFit
//                    anchors.verticalCenter: parent.verticalCenter
//                    source: "qrc:/images/ic_edit - 24x24.png"
//                    opacity: marea.pressed? 0.6 : 1.0


//                    MouseArea{
//                        id: marea
//                        anchors.fill: parent
//                        onClicked: { listView.currentIndex = index; listView.startEditProcess(); }
//                    }
//                }

//                Image {
//                    id: imgdel
//                    fillMode: Image.PreserveAspectFit
//                    anchors.verticalCenter: parent.verticalCenter
//                    source: "qrc:/images/ic_delete - 24x24.png"
//                    opacity: mareadel.pressed? 0.6 : 1.0


//                    MouseArea{
//                        id: mareadel
//                        anchors.fill: parent
//                        onClicked: { listView.currentIndex = index; listView.startDeleteProcess(); }
//                    }
//                }
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: listView.width-2; height: 110
            y: listView.currentItem.y
            x: 1
            color: "lightsteelblue";
        }
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    Component {
        id: headerComponent

        Item {
            width: listView.width
            height: 40
            y: ListView.view.y-height

            Label{
                text: qsTr("Payees")
                font.pointSize: 13
                font.bold: true
                font.italic: true
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 12

            }
        }
    }

    ColumnLayout{
        id: columnLayout
        anchors.fill: parent
        anchors.margins: 1

        ListView{
            id: listView
            model: listModel
            delegate: itemDelegate
            highlight: highlight
            Layout.fillHeight: true
            anchors.left: parent.left
            anchors.right: parent.right
            highlightFollowsCurrentItem: false
            header: headerComponent
            focus: true

            onCurrentIndexChanged: {

                currentpayeelistitem = listModel.get(currentIndex);
            }

            function startEditProcess(){

                editPayee();
            }

            function startDeleteProcess(){

                deletePayee();
            }
        }

        ToolBar {
            id: toolbar
            anchors.left: parent.left
            anchors.right: parent.right
            RowLayout {
                spacing: 6
                anchors.fill: parent

                ToolButton{
                    tooltip: qsTr("Back")
                    iconSource: "qrc:/images/ic_previous_64x64.png"
                    onClicked: stackView.pop()
                }

                ToolButton {
                    tooltip: qsTr("Add Payee")
                    iconSource: "qrc:/images/ic_add_64x64.png"
                    onClicked: addPayee()
                }
            }
        }
    }

    Component.onCompleted: PayeesViewListScript.resetPayees()
}

