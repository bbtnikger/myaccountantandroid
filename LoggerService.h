/*
 * LoggerService.h
 *
 *  Created on: Jan 19, 2015
 *      Author: Nick
 */

#ifndef LOGGERSERVICE_H_
#define LOGGERSERVICE_H_

#include <QObject>
#include <QVariantMap>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlRecord>

#define LOGGERSERVICE LoggingService::LoggerService::instance()

namespace LoggingService {

class LoggerService: public QObject {
	Q_OBJECT

private:

	LoggerService(QObject *parent = 0);
	virtual ~LoggerService();

public:

	enum LOGGTYPE{
		LOGGTYPE_SHUTDOWN  = 0,   /* Shut down the system NOW. eg: for OEM use */
		LOGGTYPE_CRITICAL  = 1,   /* Unexpected unrecoverable error. eg: hard disk error */
		LOGGTYPE_ERROR     = 2,   /* Unexpected recoverable error. eg: needed to reset a hw controller */
		LOGGTYPE_WARNING   = 3,   /* Expected error. eg: parity error on a serial port */
		LOGGTYPE_NOTICE    = 4,   /* Warnings. eg: Out of paper */
		LOGGTYPE_INFO      = 5,   /* Information. eg: Printing page 3 */
		LOGGTYPE_DEBUG1    = 6,   /* Debug messages eg: Normal detail */
		LOGGTYPE_DEBUG2    = 7    /* Debug messages eg: Fine detail */
	};

	static LoggerService* instance();

	static void deleteInstance();

	Q_INVOKABLE void logMessage(const QString &msg, LOGGTYPE lt = LOGGTYPE_INFO, unsigned int code = 0);

	Q_INVOKABLE QString lastError() const { return m_lastError; }

	Q_INVOKABLE QString logMessagesText() const;

	QString mapToString(const QVariantMap &map) const;

public slots:

    void clearInstance() { deleteInstance(); }

private:

	QString lineFromSqlRecord(const QSqlRecord &record) const ;

	void initialize();

	bool clearOldMessages();

	static LoggerService *m_pInstance;

	QSqlDatabase m_db;

	QString mDbNameWithPath;

	QString m_lastError;
};

} /* namespace LoggingService */

#endif /* LOGGERSERVICE_H_ */
