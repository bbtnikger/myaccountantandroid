import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import "AccountTypesViewList.js" as AccountTypesViewListScript

Item {
    id: root

    property variant currentaccounttypelistitem
    property alias currentaccounttypeindex: listView.currentIndex
    property alias model: listModel

    signal addAccountType()
    signal editAccountType()
    signal deleteAccountType()

    ListModel{
        id: listModel
    }

    Menu{
        id: menu
        MenuItem{
            text: qsTr("Edit")
            onTriggered: listView.startEditProcess()
        }
        MenuItem{
            text: qsTr("Delete")
            onTriggered: listView.startDeleteProcess()
        }
    }

    Component {
        id: headerComponent

        Item {
            width: listView.width
            height: 40
            y: ListView.view.y-height

            Label{
                text: qsTr("Account Types")
                font.pointSize: 13
                font.bold: true
                font.italic: true
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 12

            }
        }
    }

    Component{
        id: itemDelegate
        Item{
            width: listView.width
            height: 110

            Rectangle{
                anchors.fill: parent
                color: "#000000FF"
                border.color: "grey"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: listView.currentIndex = index
                onPressAndHold: { listView.currentIndex = index; menu.popup(); }
            }

            RowLayout{
                anchors.fill: parent
                anchors.margins: 6

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: name
                    Layout.fillWidth: true
                }
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: listView.width-2; height: 110
            y: listView.currentItem.y
            x: 1
            color: "lightsteelblue";
        }
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000FF"
        border.color: "grey"
    }

    ColumnLayout{
        id: columnLayout
        anchors.fill: parent
        anchors.margins: 1

        ListView{
            id: listView
            model: listModel
            delegate: itemDelegate
            highlight: highlight
            Layout.fillHeight: true
            anchors.left: parent.left
            anchors.right: parent.right
            highlightFollowsCurrentItem: false
            header: headerComponent
            focus: true

            onCurrentIndexChanged: {

                currentaccounttypelistitem = listModel.get(currentIndex);
            }

            function startEditProcess(){

                editAccountType();
            }

            function startDeleteProcess(){

                deleteAccountType();
            }
        }

        ToolBar {
            id: toolbar
            anchors.left: parent.left
            anchors.right: parent.right
            RowLayout {
                spacing: 6
                anchors.fill: parent

                ToolButton{
                    tooltip: qsTr("Back")
                    iconSource: "qrc:/images/ic_previous_64x64.png"
                    onClicked: stackView.pop()
                }

                ToolButton {
                    tooltip: qsTr("Add Account Type")
                    iconSource: "qrc:/images/ic_add_64x64.png"
                    onClicked: addAccountType()
                }
            }
        }
    }

    Component.onCompleted: AccountTypesViewListScript.resetAccountTypes()
}

