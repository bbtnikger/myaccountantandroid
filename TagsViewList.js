function func() {

}

function resetTags(){

    columnLayout.visible = false;

    clearTags();
    setupTags();

    columnLayout.visible = true;
}

function clearTags(){

    listModel.clear();
}

function setupTags(){

    var list = TagUtility.getTags(ProfileUtility.currentProfile);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        listModel.append(record_);
    }
}
