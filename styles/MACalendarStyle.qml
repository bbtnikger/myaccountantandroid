import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1

CalendarStyle {
    id: root

    gridVisible: false
    navigationBar: Rectangle{
        height: 60
        anchors.top: parent.top
        RowLayout{
            anchors.fill: parent

            ToolButton{
                iconSource: "qrc:/images/ic_previous_64x64.png"
                onClicked: {

                    var dt = control.selectedDate;
                    var month = dt.getMonth() - 1;
                    if(month<0)
                        dt.setFullYear(dt.getFullYear()-1);
                    month = month % 12;
                    dt.setMonth(month);
                    control.selectedDate = dt;
                }
            }

            Item{
                Layout.fillWidth: true
            }

            ComboBox{
                id: monthComboBox
                model:[1,2,3,4,5,6,7,8,9,10,11,12]
                currentIndex: control.selectedDate.getMonth()
                width: 110
                Layout.maximumWidth: 110
                Layout.minimumWidth: 110
                onCurrentIndexChanged: {

                    var dt = control.selectedDate;
                    var month = currentIndex;
                    dt.setMonth(month);
                    control.selectedDate = dt;
                }
            }

            TextField{
                id: textFieldYear
                text: control.selectedDate.getFullYear()
                width: 130
                Layout.maximumWidth: 130
                Layout.minimumWidth: 130
                validator: IntValidator{ bottom: 1900; top: 2100 }
                onActiveFocusChanged: {

                    console.log('active focus:'+activeFocus);
                    if(activeFocus == false){

                        var dt = control.selectedDate;
                        var year = 0;
                        year = text;
                        dt.setFullYear(year);
                        control.selectedDate = dt;
                    }
                }

                onAccepted: {

                    var dt = control.selectedDate;
                    var year = 0;
                    year = text;
                    dt.setFullYear(year);
                    control.selectedDate = dt;
                }
            }

            Item{
                Layout.fillWidth: true
            }

            ToolButton{
                iconSource: "qrc:/images/ic_next_64x64.png"
                onClicked: {

                    var dt = control.selectedDate;
                    var month = dt.getMonth() + 1;
                    if(month>11)
                        dt.setFullYear(dt.getFullYear()+1);
                    month = month % 12;
                    dt.setMonth(month);
                    control.selectedDate = dt;
                }
            }
        }
    }
}
