#include "loginutility.h"
#include "networkutility.h"
#include "localuserutility.h"
#include "authorizationutility.h"
#include "LoggerService.h"

#include <QNetworkRequest>
#include <QEventLoop>
#include <QUrlQuery>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

using namespace MyAccountantCloud::HelperClasses;

LoginUtility* LoginUtility::m_pInstance = NULL;

LoginUtility* LoginUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new LoginUtility();
    }

    return m_pInstance;
}

void LoginUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

LoginUtility::LoginUtility(QObject *parent) : QObject(parent)
{

}

const QString SP_API_KEY_ID         = QStringLiteral("59GTABQ56RSYIHYSEO2ARQ4D4");
const QString SP_API_KEY_SECRET     = QStringLiteral("lcl0+pERBoOol3HlR2dAiUdK/pQ9SLyISfTRbANM1eA");

bool LoginUtility::sendRequestToLogin(const QString &username, const QString &password, const QString &organizationId){

    QNetworkRequest request;

    QByteArray authorization;
    authorization.append(SP_API_KEY_ID);
    authorization.append(":");
    authorization.append(SP_API_KEY_SECRET);
    authorization = authorization.toBase64();
    authorization.prepend("Basic ");

//    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("oauth?grant_type=client_credentials"));
    QString url = QString::fromLatin1("https://api.stormpath.com/v1/applications/5lJ9s5IUg3H4jbBWT4prfo/loginAttempts");

    request.setUrl(QUrl(url));

    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("Authorization",authorization);
    (QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QByteArray ba;
    QByteArray userba;
    userba.append(username);
    userba.append(":");
    userba.append(password);
    userba = userba.toBase64();
    QVariantMap map;
    map[QLatin1String("type")] = QLatin1String("basic");
    map[QLatin1String("value")] = userba;
    QJsonObject jobj = QJsonObject::fromVariantMap(map);
    QJsonDocument doc(jobj);
    ba.append(doc.toJson());

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->post(request,ba);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onLoginReplyFinished(reply, username, password, organizationId);
}

bool LoginUtility::sendRequestToLogout(){

    QNetworkRequest request;


    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("logout"));
    request.setUrl(QUrl(url));

    request.setHeader( QNetworkRequest::ContentTypeHeader,QVariant( QString("text/xml;charset=utf-8")));
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));
    request.setSslConfiguration(QSslConfiguration::defaultConfiguration());

    QByteArray ba;
    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
//    connect(reply,SIGNAL(sslErrors(QList<QSslError>)),NETWORKUTILITY,SLOT(onSslErrors(QList<QSslError>)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onLogoutReplyFinished(reply);
}


bool LoginUtility::redirectAfterLogout(const QString &url){

    Q_UNUSED(url);

    QNetworkRequest request;

    request.setHeader( QNetworkRequest::ContentTypeHeader,QVariant( QString("text/xml;charset=utf-8")));
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));
    request.setSslConfiguration(QSslConfiguration::defaultConfiguration());

    QByteArray ba;
    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
//    connect(reply,SIGNAL(sslErrors(QList<QSslError>)),NETWORKUTILITY,SLOT(onSslErrors(QList<QSslError>)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onLogoutRedirectFinished(reply);
}

bool LoginUtility::onLoginReplyFinished(QNetworkReply *reply, const QString &username, const QString &password, const QString &organizationId){

    Q_UNUSED(organizationId);
    Q_UNUSED(username);
    Q_UNUSED(password);

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    qDebug() << content_;
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << "reply url:" << reply->url();
    qDebug() << "network request url:" << reply->request().url();

    QString accounthref;
    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("account")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            accounthref = val.toObject().value(QLatin1String("href")).toString();

            break;
        }

        qDebug() << "accounthref(" << accounthref << ")";

        if(accounthref.isEmpty()){

            // check for user to the ma-api-service


            emit loginFailed();
            return false;
        }

        return sendGetApiKeyRequest(accounthref);

    }else{

        qDebug() << parserError.errorString();
    }

    return false;
}

bool LoginUtility::onLogoutReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    qDebug() << "content:\n" << content_;
    qDebug() << "reply url:" << reply->url();
    qDebug() << "network request url:" << reply->request().url();

    QUrl newUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
    qDebug() << newUrl;
    QString redirectUrl = newUrl.toString();
    redirectAfterLogout(redirectUrl);

    return false;
}

bool LoginUtility::onLogoutRedirectFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    qDebug() << "content:\n" << content_;
    qDebug() << "reply url:" << reply->url();
    qDebug() << "network request url:" << reply->request().url();

    return false;
}

bool LoginUtility::sendGetApiKeyRequest(const QString &href){

    QNetworkRequest request;

    QByteArray authorization;
    authorization.append(SP_API_KEY_ID);
    authorization.append(":");
    authorization.append(SP_API_KEY_SECRET);
    authorization = authorization.toBase64();
    authorization.prepend("Basic ");

    QString url = href;
    url.append(QLatin1String("/apiKeys"));

    request.setUrl(QUrl(url));
    request.setRawHeader("Authorization",authorization);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));
//    request.setSslConfiguration(QSslConfiguration::defaultConfiguration());

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onSendGetApiKeyRequestFinished(reply, href);
}

bool LoginUtility::onSendGetApiKeyRequestFinished(QNetworkReply *reply, const QString &accounthref){

    qDebug() << __FUNCTION__ << accounthref;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
    qDebug() << "reply url:" << reply->url();
    qDebug() << "network request url:" << reply->request().url();

    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("items")); i != jsonobject.end(); i++){

            QJsonValue val = *i;

            QJsonArray arr = val.toArray();
            if(!arr.isEmpty()){

                QJsonValue firstItem = arr.first();

                QString apiKeyId = firstItem.toObject().value(QLatin1String("id")).toString();
                QString apiKeySecret = firstItem.toObject().value(QLatin1String("secret")).toString();

                qDebug() << apiKeyId << apiKeySecret;

                AUTHORIZATIONUTILITY->setApiKeyId(apiKeyId);
                AUTHORIZATIONUTILITY->setApiKeySecret(apiKeySecret);

//                emit loginSuccess();

                break;
            }
        }

//        emit loginSuccess();
//        return sendGetTokenRequest(LOCALUSERUTILITY->generalURL());
        return sendGetTokenRequest(LOCALUSERUTILITY->generalURL());

    }else{

        qDebug() << parserError.errorString();
    }

    return false;
}

bool LoginUtility::sendGetTokenRequest(const QString &url){

    QNetworkRequest request;

    QByteArray authorization = LOCALUSERUTILITY->constructAuthorizationBasicToken();
    qDebug() << authorization;

    QString urlstring = url;
    urlstring.append(QLatin1String("/oauth?grant_type=client_credentials"));

    request.setUrl(QUrl(urlstring));

    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("Authorization",authorization);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

//    if(url.startsWith(QLatin1String("https"),Qt::CaseInsensitive)){

//        LOGGERSERVICE->logMessage(QLatin1String("Log type: server start with https, try to set SSL configuration to the network request."));

//        QSslConfiguration conf = request.sslConfiguration();

//        // this is used in order to catch the SSL key error
//        bool hasSSLKeyError_;
//        QString passPhrase = QLatin1String("NikGer1@");
//        QString certPrivateKeyPath = QLatin1String(":/client.pem");
//        conf = NETWORKUTILITY->getSslConfiguration(conf,certPrivateKeyPath,passPhrase,&hasSSLKeyError_);

//        if(hasSSLKeyError_){

////            emit hasSSLKeyError();
//            return false;
//        }

//        request.setSslConfiguration(conf);

////        QCoreApplication::processEvents();
////        NETWORKUTILITY->resetNetworkInstances();
//    }

//    request.setSslConfiguration();

    QByteArray ba;
    QByteArray userba;
    userba.append(LOCALUSERUTILITY->username());
    userba.append(":");
    userba.append(LOCALUSERUTILITY->password());
    userba = userba.toBase64();
    QVariantMap map;
    map[QLatin1String("type")] = QLatin1String("basic");
    map[QLatin1String("value")] = userba;
    QJsonObject jobj = QJsonObject::fromVariantMap(map);
    QJsonDocument doc(jobj);
    ba.append(doc.toJson());

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->post(request,ba);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply,SIGNAL(sslErrors(QList<QSslError>)),NETWORKUTILITY,SLOT(onSslErrors(QList<QSslError>)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onSendGetTokenReuestFinished(reply);
}

bool LoginUtility::onSendGetTokenReuestFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
    qDebug() << "reply url:" << reply->url();
    qDebug() << "network request url:" << reply->request().url();

    QString access_token;
    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("access_token")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            access_token = val.toString();
            LOCALUSERUTILITY->setAuthToken(access_token);

            break;
        }

        qDebug() << "access_token(" << access_token << ")";

        if(!access_token.isEmpty()){

            // check for user to the ma-api-service


            emit loginSuccess();
        }

        return true;

    }else{

        qDebug() << parserError.errorString();
    }

    return false;
}

LoginUtility::~LoginUtility(){

    qDebug() << __FUNCTION__;
}
