#ifndef TAGUTILITY_H
#define TAGUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define TAGUTILITY MyAccountantCloud::HelperClasses::TagUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class TagUtility : public QObject
{
    Q_OBJECT
private:
    explicit TagUtility(QObject *parent = 0);

    ~TagUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static TagUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addTag
     * \param tag
     */
    Q_INVOKABLE QVariant addTag(const QVariantMap &tag);


    /*!
     * \brief getTags
     * \return
     */
    Q_INVOKABLE QVariantList getTags(const QString &profile_id);


    /*!
     * \brief deleteTag
     * \param tag
     * \return
     */
    Q_INVOKABLE bool deleteTag(const QString &_id);

    /*!
     * \brief updateTag
     * \param tag
     * \return
     */
    Q_INVOKABLE QVariant updateTag(const QString &_id, const QVariantMap &tag);

signals:

public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddTagReplyFinished
     * \param reply
     */
    QVariant onAddTagReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetTagsReplyFinished
     * \param reply
     */
    QVariantList onGetTagsReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteTagReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteTagReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateTagReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateTagReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static TagUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // TAGUTILITY_H
