#include "accountutility.h"
#include "networkutility.h"
#include "localuserutility.h"
#include "LoggerService.h"

#include <QNetworkRequest>
#include <QEventLoop>
#include <QUrlQuery>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

using namespace MyAccountantCloud::HelperClasses;


AccountUtility* AccountUtility::m_pInstance = NULL;

AccountUtility* AccountUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new AccountUtility();
    }

    return m_pInstance;
}

void AccountUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

AccountUtility::AccountUtility(QObject *parent) : QObject(parent){

}


QVariant AccountUtility::addAccount(const QVariantMap &account){

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounts"));
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(account);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("account"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "account(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->post(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onAddAccountReplyFinished(reply);
}

QVariantList AccountUtility::getAccounts(const QString &profile_id){

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2?").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounts"));

    QUrlQuery urlquery(url);
    urlquery.addQueryItem(QLatin1String("profile_id"),profile_id);
    request.setUrl(QUrl(urlquery.toString()));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetAccountsReplyFinished(reply);
}


QVariant AccountUtility::onAddAccountReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        map = _map.value(QLatin1String("account"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}

QVariantList AccountUtility::onGetAccountsReplyFinished(QNetworkReply *reply){

    QVariantList list;

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return list;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("accounts")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            QJsonArray arr = val.toArray();
            foreach (QJsonValue catval, arr) {

                list.append(catval.toVariant());
            }

            break;
        }

        return list;

    }else{

        qDebug() << parserError.errorString();
        LOGGERSERVICE->logMessage(QString::fromLatin1("GET api accounts failed,error(%1)").arg(parserError.errorString()));
    }

    return list;
}

bool AccountUtility::onDeleteAccountReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << "content:" << content_;
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


//    QString access_token;
//    QJsonParseError parserError;
//    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
//    if(parserError.error == QJsonParseError::NoError){

//        qDebug() << json.toJson();

//        return true;

//    }else{

//        qDebug() << parserError.errorString();
//    }

    return true;
}

bool AccountUtility::deleteAccount(const QString &_id){

    qDebug() << Q_FUNC_INFO << _id;

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2/%3").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounts")).arg(_id);
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->deleteResource(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onDeleteAccountReplyFinished(reply);
}


QVariant AccountUtility::updateAccount(const QString &_id, const QVariantMap &account){

    QNetworkRequest request;

    qDebug() << Q_FUNC_INFO << _id << account;

    QString url = QString::fromLatin1("%1/%2/%3").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounts")).arg(_id);
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(account);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("account"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "account(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->put(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onUpdateAccountReplyFinished(reply);
}

QVariant AccountUtility::onUpdateAccountReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        return _map.value(QLatin1String("account"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}

QVariantMap AccountUtility::getAccountBalances(const QString &_id, const QString &profile_id){

    QNetworkRequest request;

    qDebug() << Q_FUNC_INFO << profile_id << _id;
    LOGGERSERVICE->logMessage(QString::fromLatin1("%1 - %2,%3").arg(QString::fromUtf8(Q_FUNC_INFO)).arg(profile_id).arg(_id));

    QString url = QString::fromLatin1("%1/%2?").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounts/balance"));
    QUrlQuery urlquery(url);
    urlquery.addQueryItem(QLatin1String("profile_id"),profile_id);
    urlquery.addQueryItem(QLatin1String("account_id"),_id);
    request.setUrl(QUrl(urlquery.toString()));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetAccountBalancesReplyFinished(reply);
}

QVariantMap AccountUtility::onGetAccountBalancesReplyFinished(QNetworkReply *reply){

    QVariantMap map;

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("balance")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            QJsonObject jsonobj = val.toObject();

            map = jsonobj.toVariantMap();

            break;
        }

    }else{

        qDebug() << parserError.errorString();
        LOGGERSERVICE->logMessage(QString::fromLatin1("GET api history failed,error(%1)").arg(parserError.errorString()));
    }

    qDebug() << map;

    return map;
}
