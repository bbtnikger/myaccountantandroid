#include "profileutility.h"
#include "networkutility.h"
#include "localuserutility.h"
#include "LoggerService.h"

#include <QNetworkRequest>
#include <QEventLoop>
#include <QUrlQuery>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

using namespace MyAccountantCloud::HelperClasses;


ProfileUtility* ProfileUtility::m_pInstance = NULL;

ProfileUtility* ProfileUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new ProfileUtility();
    }

    return m_pInstance;
}

void ProfileUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

ProfileUtility::ProfileUtility(QObject *parent) : QObject(parent){

}


QVariant ProfileUtility::addProfile(const QVariantMap &profile){

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/profiles"));
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(profile);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("profile"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "profile(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->post(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onAddProfileReplyFinished(reply);
}

QVariantList ProfileUtility::getProfiles(){

    qDebug() << Q_FUNC_INFO;
    LOGGERSERVICE->logMessage(QString::fromUtf8(Q_FUNC_INFO));

    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/profiles"));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    qDebug() << Q_FUNC_INFO << url << ba;

    QNetworkRequest request;

    request.setUrl(QUrl(url));
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type","application/json");
    request.setRawHeader("Accept","application/json");
    request.setSslConfiguration(QSslConfiguration::defaultConfiguration());
//    request.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/json"));
//    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
//    connect(reply,SIGNAL(finished()),this,SLOT(onGetProfilesReplyFinished()));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetProfilesReplyFinished(reply);

//    return QVariantList();
}


QVariant ProfileUtility::onAddProfileReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        map = _map.value(QLatin1String("profile"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}

//QVariantList ProfileUtility::onGetProfilesReplyFinished(){

//    QVariantList list;

//    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());

//    qDebug() << __FUNCTION__;

//    if(!reply){

//        qDebug() << QLatin1String("reply is null");
//        return list;
//    }

//    QString content_ = QString::fromUtf8(reply->readAll());
//    if(content_.startsWith(QLatin1String("\"")))
//        content_.remove(0,1);
//    if(content_.endsWith(QLatin1String("\"")))
//        content_.chop(1);

//    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


//    QJsonParseError parserError;
//    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
//    if(parserError.error == QJsonParseError::NoError){

//        QJsonObject jsonobject = json.object();

//        QJsonObject::const_iterator i;

//        for(i = jsonobject.find(QLatin1String("profiles")); i != jsonobject.end(); i++){

//            QJsonValue val = *i;
//            QJsonArray arr = val.toArray();
//            foreach (QJsonValue catval, arr) {

//                list.append(catval.toVariant());
//            }

//            break;
//        }

//        return list;

//    }else{

//        qDebug() << parserError.errorString();
//    }

//    return list;
//}

QVariantList ProfileUtility::onGetProfilesReplyFinished(QNetworkReply *reply){

    QVariantList list;

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return list;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
    qDebug() << reply->request().rawHeader("Authorization");
    qDebug() << "reply url:" << reply->url();
    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("profiles")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            QJsonArray arr = val.toArray();
            foreach (QJsonValue catval, arr) {

                list.append(catval.toVariant());
            }

            break;
        }

        return list;

    }else{

        qDebug() << parserError.errorString();
    }

    return list;
}

bool ProfileUtility::onDeleteProfileReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << "content:" << content_;
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


//    QString access_token;
//    QJsonParseError parserError;
//    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
//    if(parserError.error == QJsonParseError::NoError){

//        qDebug() << json.toJson();

//        return true;

//    }else{

//        qDebug() << parserError.errorString();
//    }

    return true;
}

bool ProfileUtility::deleteProfile(const QString &_id){

    qDebug() << Q_FUNC_INFO << _id;

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2/%3").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/profiles")).arg(_id);
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->deleteResource(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onDeleteProfileReplyFinished(reply);
}


QVariant ProfileUtility::updateProfile(const QString &_id, const QVariantMap &profile){

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2/%3").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/profiles")).arg(_id);
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(profile);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("profile"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "profile(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->put(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onUpdateProfileReplyFinished(reply);
}

QVariant ProfileUtility::onUpdateProfileReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        return _map.value(QLatin1String("profile"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}
