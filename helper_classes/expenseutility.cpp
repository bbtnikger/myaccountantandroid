#include "expenseutility.h"
#include "networkutility.h"
#include "localuserutility.h"
#include "LoggerService.h"

#include <QNetworkRequest>
#include <QEventLoop>
#include <QUrlQuery>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

using namespace MyAccountantCloud::HelperClasses;


ExpenseUtility* ExpenseUtility::m_pInstance = NULL;

ExpenseUtility* ExpenseUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new ExpenseUtility();
    }

    return m_pInstance;
}

void ExpenseUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

ExpenseUtility::ExpenseUtility(QObject *parent) : QObject(parent){

}


QVariant ExpenseUtility::addExpense(const QVariantMap &expense){

//    qDebug() << QString::fromLatin1("Add expense, parameters(%1,%2)").arg(payee.value(QLatin1String("name")).toString()).arg(payee.value(QLatin1String("profile_id")).toString());
//    LOGGERSERVICE->logMessage(QString::fromLatin1("Add payee, parameters(%1,%2)").arg(payee.value(QLatin1String("name")).toString()).arg(payee.value(QLatin1String("profile_id")).toString()));

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/expenses"));
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(expense);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("expense"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "expense(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->post(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onAddExpenseReplyFinished(reply);
}

QVariantList ExpenseUtility::getExpenses(const QString &profile_id){

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/expenses"));

    QUrlQuery urlquery(url);
    urlquery.addQueryItem(QLatin1String("profile_id"),profile_id);
    request.setUrl(QUrl(urlquery.toString()));

    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetExpensesReplyFinished(reply);
}


QVariant ExpenseUtility::onAddExpenseReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        map = _map.value(QLatin1String("expense"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}

QVariantList ExpenseUtility::onGetExpensesReplyFinished(QNetworkReply *reply){

    QVariantList list;

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return list;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("expenses")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            QJsonArray arr = val.toArray();
            foreach (QJsonValue catval, arr) {

                list.append(catval.toVariant());
            }

            break;
        }

        return list;

    }else{

        qDebug() << parserError.errorString();
    }

    return list;
}

bool ExpenseUtility::onDeleteExpenseReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << "content:" << content_;
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


//    QString access_token;
//    QJsonParseError parserError;
//    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
//    if(parserError.error == QJsonParseError::NoError){

//        qDebug() << json.toJson();

//        return true;

//    }else{

//        qDebug() << parserError.errorString();
//    }

    return true;
}

bool ExpenseUtility::deleteExpense(const QString &_id){

    qDebug() << Q_FUNC_INFO << _id;

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2/%3").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/expenses")).arg(_id);
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->deleteResource(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onDeleteExpenseReplyFinished(reply);
}


QVariant ExpenseUtility::updateExpense(const QString &_id, const QVariantMap &expense){

    QNetworkRequest request;

    qDebug() << Q_FUNC_INFO << _id << expense;

    QString url = QString::fromLatin1("%1/%2/%3").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/expenses")).arg(_id);
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(expense);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("expense"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "expense(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->put(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onUpdateExpenseReplyFinished(reply);
}

QVariant ExpenseUtility::onUpdateExpenseReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        return _map.value(QLatin1String("expense"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}

QVariantList ExpenseUtility::getExpenses(const QString &profile_id, const QVariantMap &queryParameters){

    QNetworkRequest request;

    qDebug() << Q_FUNC_INFO << profile_id << queryParameters.count();
    LOGGERSERVICE->logMessage(QString::fromLatin1("%1 - %2, Number of queryParams(%3)").arg(QString::fromUtf8(Q_FUNC_INFO)).arg(profile_id).arg(queryParameters.count()));

    QString url = QString::fromLatin1("%1/%2?").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/expenses"));
    QUrlQuery urlquery(url);
    urlquery.addQueryItem(QLatin1String("profile_id"),profile_id);

    QVariantMap::const_iterator i;
    for(i = queryParameters.constBegin(); i != queryParameters.constEnd(); i++){

        QString paramName = i.key();
        QString paramValue = i.value().toString();

//        qDebug() << "paramName" << paramName << "paramValue" << paramValue;

        urlquery.addQueryItem(paramName,paramValue);
    }

    request.setUrl(QUrl(urlquery.toString()));

    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetExpensesReplyFinished(reply);
}


QVariantMap ExpenseUtility::getExpenseProperties(const QString &profile_id){

    QNetworkRequest request;

    qDebug() << Q_FUNC_INFO << profile_id;
    LOGGERSERVICE->logMessage(QString::fromLatin1("%1 - %2").arg(QString::fromUtf8(Q_FUNC_INFO)).arg(profile_id));

    QString url = QString::fromLatin1("%1/%2?").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/expense/properties"));
    QUrlQuery urlquery(url);
    urlquery.addQueryItem(QLatin1String("profile_id"),profile_id);
    request.setUrl(QUrl(urlquery.toString()));

    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetExpensePropertiesReplyFinished(reply);
}

QVariantMap ExpenseUtility::onGetExpensePropertiesReplyFinished(QNetworkReply *reply){

    QVariantMap map;

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

//    qDebug() << content_;

    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("expenseproperties")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            QJsonObject jsonobj = val.toObject();

            map = jsonobj.toVariantMap();

            break;
        }

    }else{

        qDebug() << parserError.errorString();
        LOGGERSERVICE->logMessage(QString::fromLatin1("GET api history failed,error(%1)").arg(parserError.errorString()));
    }

//    qDebug() << map;

    return map;
}
