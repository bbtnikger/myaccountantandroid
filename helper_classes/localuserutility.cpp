/*
 * LocalUserUtility.cpp
 *
 *  Created on: May 28, 2015
 *      Author: Nick
 */

#include "LocalUserUtility.h"
#include "NetworkUtility.h"
#include "authorizationutility.h"
#include "settingsmanager.h"
//#include "Constants.h"
//#include "FaultResponseUtility.h"
//#include "LoggerService.h"

#include <QDebug>

using namespace MyAccountantCloud::HelperClasses;

LocalUserUtility* LocalUserUtility::m_pInstance = NULL;


LocalUserUtility* LocalUserUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new LocalUserUtility();
    }

    return m_pInstance;
}

void LocalUserUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

LocalUserUtility::LocalUserUtility(QObject *parent)
    : /*m_pSettings(new QSettings(QLatin1String("IDEAL APPS"), QLatin1String("MyAccountant"), this)), */QObject(parent) {

    // TODO Auto-generated constructor stub

    initialize();
}


void LocalUserUtility::initialize(){

    m_currentLoggingLevel 					= All;

    //a bool value representing whether we are refreshing the token.
//    m_AuthTokenIsRefreshing 				= false;

    //the default values for the "next authentication and relogin"
//    m_nextAuthStatementInterval 			= QTime(0, 0, 2940);
//    m_nextReloginInterval 					= QTime(0, 0, 3540);

    //these are 2 bools which only disabling the popups if one is allready displayed to disable popup spam to the user
//    m_Cancel_All_The_Requests 				= false;
//    m_NoConnectionToServerPopupFlagShown 	= false;

    //this is the bool settings value for use only over Wifi, if the user can refresh with 3g or not.
//    m_UseOnlyOverWifi 						= true;
    //whether the auto refresh is on.
//    m_AutoRefreshIsActive 					= true;

    //this is a static value for the clientInfo since we need more than in one page information from it.
//	internal static MobileClientInfoCollectionFix ClientInfo = null;
//	public static string DefaultLocale = null;

    /// <summary>
    /// the authentication token's next renew time
    /// </summary>

    //username used from the user during login screen.
    //alexandros.xatzidimitriou@system
    // http://195.97.14.73:8081/axis/services/
    // Asd123!.
//	m_username 								= m_pSettings->value(QLatin1String("username"),QLatin1String("user@system")).toString();
//	m_password								= m_pSettings->value(QLatin1String("password"),QLatin1String("password")).toString();
//    m_domain                                = m_pSettings->value(QLatin1String("domain"),QLatin1String("myaccountant.com")).toString();
//    m_domain                                = QLatin1String("localhost");
    m_domain                                = QLatin1String("myaccountantapp.com");
    m_username                              = SETTINGSMANAGER->settingsValueFor(QLatin1String("username"),"");
    m_password                              = SETTINGSMANAGER->settingsValueFor(QLatin1String("password"),"");
    m_OrganizationId                        = SETTINGSMANAGER->settingsValueFor(QLatin1String("organizationId"),"");
//    m_domain							= m_pSettings->value(QLatin1String("domain"),"").toString();
    m_AuthToken                             = SETTINGSMANAGER->settingsValueFor(QLatin1String("authToken"),"");
//    m_SymphoniaUserId						= m_pSettings->value(QLatin1String("symphoniaUserId"),"").toString();

    qDebug() << m_username << m_password << m_OrganizationId;

    m_isLoggedIn							= false;
}


void LocalUserUtility::setupByLoginInfo(const QVariant &var){

    QVariantMap map 	= var.toMap();

    m_username 			= map.value(QLatin1String("username")).toString();
    m_password 			= map.value(QLatin1String("password")).toString();

    if(!m_username.isEmpty())
        setUsername(m_username);

    if(!m_password.isEmpty())
        setPassword(m_password);
}

QByteArray LocalUserUtility::constructAuthorizationBearerToken() const{

    QByteArray ba;
    ba.append(QLatin1String("Bearer "));
    ba.append(LOCALUSERUTILITY->authToken().toUtf8());
//    QByteArray ba;
//    ba.append(AUTHORIZATIONUTILITY->apiKeyId());
//    ba.append(QLatin1String(":"));
//    ba.append(AUTHORIZATIONUTILITY->apiKeySecret());
//    ba = ba.toBase64();
//    ba.prepend("Bearer ");
    return ba;
}

QByteArray LocalUserUtility::constructAuthorizationBasicToken() const{

    QByteArray ba;
    ba.append(AUTHORIZATIONUTILITY->apiKeyId());
    ba.append(QLatin1String(":"));
    ba.append(AUTHORIZATIONUTILITY->apiKeySecret());
    ba = ba.toBase64();
    ba.prepend("Basic ");
    return ba;
}

QString LocalUserUtility::generalURL(bool isHTTPS, bool containSubdomain) const{

    QString gurl;
    if(!isHTTPS){

        if(containSubdomain)
            gurl = QString::fromLatin1("http://%1.%2:3000").arg(m_OrganizationId).arg(m_domain);
        else
            gurl = QString::fromLatin1("http://%1:3000").arg(m_domain);

    }else{

        if(containSubdomain)
            gurl = QString::fromLatin1("https://%1.%2:443").arg(m_OrganizationId).arg(m_domain);
        else
            gurl = QString::fromLatin1("https://%1:443").arg(m_domain);
    }

    return gurl;
}

QString LocalUserUtility::localizedDate(const QDateTime &dt, int format){

    Qt::DateFormat df = (Qt::DateFormat)format;
    return dt.date().toString(df);
}

QString LocalUserUtility::localizedNumber(double num, int digits){

    return m_locale.toString(num,'f',digits);
}

void LocalUserUtility::setDomain(const QString &gu){

    m_domain = gu;

    SETTINGSMANAGER->setSettingsValueFor(QLatin1String("domain"),m_domain);
}

void LocalUserUtility::setOrganizationId(const QString &s){

    m_OrganizationId = s;

    SETTINGSMANAGER->setSettingsValueFor(QLatin1String("organizationId"),m_OrganizationId);
}

void LocalUserUtility::setEMailAddress(const QString &e){

    m_EMailAddress = e;

    SETTINGSMANAGER->setSettingsValueFor(QLatin1String("eMailAddress"),m_EMailAddress);
}

void LocalUserUtility::setAuthToken(const QString &a){

    m_AuthToken = a;

    SETTINGSMANAGER->setSettingsValueFor(QLatin1String("authToken"),m_AuthToken);
}

void LocalUserUtility::setUsername(const QString &u){

    m_username = u;

    SETTINGSMANAGER->setSettingsValueFor(QLatin1String("username"),m_username);

    emit usernameChanged();
}

void LocalUserUtility::setClientId(const QString &u){

    m_clientId = u;

    SETTINGSMANAGER->setSettingsValueFor(QLatin1String("clientId"),m_clientId);
}

void LocalUserUtility::setClientSecret(const QString &u){

    m_clientSecret = u;

    SETTINGSMANAGER->setSettingsValueFor(QLatin1String("m_clientSecret"),m_clientSecret);
}

void LocalUserUtility::setPassword(const QString &p){

    m_password = p;

    SETTINGSMANAGER->setSettingsValueFor(QLatin1String("password"),m_password);

    emit passwordChanged();
}

QString LocalUserUtility::localizedDateFromString(const QString &datestring, int format){

    Qt::DateFormat df = (Qt::DateFormat)format;
    QDateTime dt = QDateTime::fromString(datestring,Qt::ISODate);
    return dt.date().toString(df);
}

QDateTime LocalUserUtility::dateFromString(const QString &datestring, int format){

	Qt::DateFormat df = (Qt::DateFormat)format;
	return QDateTime::fromString(datestring,df);
}

LocalUserUtility::~LocalUserUtility() {
    // TODO Auto-generated destructor stub
}
