#ifndef INCOMEUTILITY_H
#define INCOMEUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define INCOMEUTILITY MyAccountantCloud::HelperClasses::IncomeUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class IncomeUtility : public QObject
{
    Q_OBJECT
private:
    explicit IncomeUtility(QObject *parent = 0);

    ~IncomeUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static IncomeUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addIncome
     * \param income
     */
    Q_INVOKABLE QVariant addIncome(const QVariantMap &income);


    /*!
     * \brief getIncomes
     * \return
     */
    Q_INVOKABLE QVariantList getIncomes(const QString &profile_id);

    /*!
     * \brief getIncomes
     * \param profile_id
     * \param queryParameters
     * \return
     */
    Q_INVOKABLE QVariantList getIncomes(const QString &profile_id, const QVariantMap &queryParameters);


    /*!
     * \brief deleteIncome
     * \param income
     * \return
     */
    Q_INVOKABLE bool deleteIncome(const QString &_id);

    /*!
     * \brief updateIncome
     * \param income
     * \return
     */
    Q_INVOKABLE QVariant updateIncome(const QString &_id, const QVariantMap &income);

signals:


public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddIncomeReplyFinished
     * \param reply
     */
    QVariant onAddIncomeReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetIncomesReplyFinished
     * \param reply
     */
    QVariantList onGetIncomesReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteIncomeReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteIncomeReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateIncomeReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateIncomeReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static IncomeUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // INCOMEUTILITY_H
