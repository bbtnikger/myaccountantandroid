#ifndef TRANSACTIONUTILITY_H
#define TRANSACTIONUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define TRANSACTIONUTILITY MyTransactionantCloud::HelperClasses::TransactionUtility::instance()

namespace MyTransactionantCloud{

namespace HelperClasses{

class TransactionUtility : public QObject
{
    Q_OBJECT
private:
    explicit TransactionUtility(QObject *parent = 0);

    ~TransactionUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static TransactionUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addTransaction
     * \param transaction
     */
    Q_INVOKABLE QVariant addTransaction(const QVariantMap &transaction);


    /*!
     * \brief getTransactions
     * \return
     */
    Q_INVOKABLE QVariantList getTransactions();


    /*!
     * \brief deleteTransaction
     * \param transaction
     * \return
     */
    Q_INVOKABLE bool deleteTransaction(const QVariantMap &transaction);

    /*!
     * \brief updateTransaction
     * \param transaction
     * \return
     */
    Q_INVOKABLE QVariant updateTransaction(const QVariantMap &transaction);

signals:

private slots:

    /*!
     * \brief onAddTransactionReplyFinished
     * \param reply
     */
    QVariant onAddTransactionReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetTransactionsReplyFinished
     * \param reply
     */
    QVariantList onGetTransactionsReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteTransactionReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteTransactionReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateTransactionReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateTransactionReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static TransactionUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyTransactionantCloud

#endif // ACCOUNTUTILITY_H
