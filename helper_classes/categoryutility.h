#ifndef CATEGORYUTILITY_H
#define CATEGORYUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define CATEGORYUTILITY MyAccountantCloud::HelperClasses::CategoryUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class CategoryUtility : public QObject
{
    Q_OBJECT
private:
    explicit CategoryUtility(QObject *parent = 0);

    ~CategoryUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static CategoryUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addCategory
     * \param category
     */
    Q_INVOKABLE QVariant addCategory(const QVariantMap &category);


    /*!
     * \brief getCategories
     * \return
     */
    Q_INVOKABLE QVariantList getCategories(const QString &profile_id);


    /*!
     * \brief deleteCategory
     * \param category
     * \return
     */
    Q_INVOKABLE bool deleteCategory(const QString &_id);

    /*!
     * \brief updateCategory
     * \param category
     * \return
     */
    Q_INVOKABLE QVariant updateCategory(const QString &_id, const QVariantMap &category);

signals:


public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddCategoryReplyFinished
     * \param reply
     */
    QVariant onAddCategoryReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetCategoriesReplyFinished
     * \param reply
     */
    QVariantList onGetCategoriesReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteCategoryReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteCategoryReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateCategoryReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateCategoryReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static CategoryUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // CATEGORYUTILITY_H
