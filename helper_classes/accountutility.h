#ifndef ACCOUNTUTILITY_H
#define ACCOUNTUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define ACCOUNTUTILITY MyAccountantCloud::HelperClasses::AccountUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class AccountUtility : public QObject
{
    Q_OBJECT
private:

    explicit AccountUtility(QObject *parent = 0);

    ~AccountUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static AccountUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addAccount
     * \param account
     */
    Q_INVOKABLE QVariant addAccount(const QVariantMap &account);


    /*!
     * \brief getAccounts
     * \return
     */
    Q_INVOKABLE QVariantList getAccounts(const QString &profile_id);


    /*!
     * \brief deleteAccount
     * \param account
     * \return
     */
    Q_INVOKABLE bool deleteAccount(const QString &_id);

    /*!
     * \brief updateAccount
     * \param account
     * \return
     */
    Q_INVOKABLE QVariant updateAccount(const QString &_id, const QVariantMap &account);

    /*!
     * \brief getAccountBalances
     * \param _id
     * \param account
     * \return
     */
    Q_INVOKABLE QVariantMap getAccountBalances(const QString &_id, const QString &account);

signals:


public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddAccountReplyFinished
     * \param reply
     */
    QVariant onAddAccountReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetAccountsReplyFinished
     * \param reply
     */
    QVariantList onGetAccountsReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteAccountReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteAccountReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateAccountReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateAccountReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetAccountBalancesReplyFinished
     * \param reply
     * \return
     */
    QVariantMap onGetAccountBalancesReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static AccountUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // ACCOUNTUTILITY_H
