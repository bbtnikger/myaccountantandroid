/*
 * SettingsManager.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: Nick
 */

#include "SettingsManager.h"
#include "LoggerService.h"

#include <QFile>
#include <QDebug>
#include <QStandardPaths>

using namespace MyAccountantCloud::HelperClasses;

const QString ORG_NAME = QStringLiteral("IDEAL APPS");
const QString APP_NAME = QStringLiteral("MyAccountant");

SettingsManager* SettingsManager::m_pInstance = NULL;

SettingsManager* SettingsManager::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new SettingsManager();
    }

    return m_pInstance;
}

void SettingsManager::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

QVariantMap SettingsGroup::toMap() const{

	QVariantMap map;
	map[QLatin1String("sgid")]			= m_sgid;
	map[QLatin1String("name")]			= m_name;
	map[QLatin1String("description")]	= m_description;
	map[QLatin1String("icon")]			= m_icon;
	return map;
}

SettingsManager::SettingsManager(QObject *parent)
    : m_darkThemeStyle(false), QObject(parent) {

    setupCurrencies();
}


SettingsManager::~SettingsManager() {
	// TODO Auto-generated destructor stub


}


void SettingsManager::initialize(){

	// Accounts
	SettingsGroup *pGroupAccounts = new SettingsGroup(this);
	pGroupAccounts->setSgid(0);
	pGroupAccounts->setName(tr("Accounts"));
	pGroupAccounts->setDescription(tr("Account settings"));
	pGroupAccounts->setIcon(m_darkThemeStyle? QLatin1String("gla_account.png"):QLatin1String("gla_account_inverted.png"));

	// Currencies
	SettingsGroup *pGroupCurrencies = new SettingsGroup(this);
	pGroupCurrencies->setSgid(1);
	pGroupCurrencies->setName(tr("Currencies"));
	pGroupCurrencies->setDescription(tr("Currency settings"));
	pGroupCurrencies->setIcon(m_darkThemeStyle?  QLatin1String("gla_currencyconvert.png") : QLatin1String("gla_currencyconvert_inverted.png"));

	// Theme
	SettingsGroup *pGroupTheme = new SettingsGroup(this);
	pGroupTheme->setSgid(2);
	pGroupTheme->setName(tr("Theme"));
	pGroupTheme->setDescription(tr("Theme settings ( themes, fonts etc. )"));
	pGroupTheme->setIcon(m_darkThemeStyle?  QLatin1String("gla_settingscustomization.png") : QLatin1String("gla_settingscustomization_inverted.png"));

	// General
	SettingsGroup *pGroupGeneral = new SettingsGroup(this);
	pGroupGeneral->setSgid(4);
	pGroupGeneral->setName(tr("General"));
	pGroupGeneral->setDescription(tr("Default tab, language, keyboard"));
	pGroupGeneral->setIcon(m_darkThemeStyle?  QLatin1String("gla_settingsgeneral.png") : QLatin1String("gla_settingsgeneral_inverted.png"));

	// Defaults
	SettingsGroup *pGroupDefaults = new SettingsGroup(this);
	pGroupDefaults->setSgid(5);
	pGroupDefaults->setName(tr("Defaults"));
	pGroupDefaults->setDescription(tr("Default payee, account, payment method"));
	pGroupDefaults->setIcon(m_darkThemeStyle?  QLatin1String("gla_expensesincome.png") : QLatin1String("gla_expensesincome_inverted.png"));

	// Active Frame
	SettingsGroup *pGroupActiveFrame = new SettingsGroup(this);
	pGroupActiveFrame->setSgid(6);
	pGroupActiveFrame->setName(tr("Active Frame"));
	pGroupActiveFrame->setDescription(tr("Select the accounts to be visible on the active frame"));
	pGroupActiveFrame->setIcon(m_darkThemeStyle?  QLatin1String("gla_settingsactiveframe.png") : QLatin1String("gla_settingsactiveframe_inverted.png"));

	// Security
	SettingsGroup *pGroupSecurity = new SettingsGroup(this);
	pGroupSecurity->setSgid(7);
	pGroupSecurity->setName(tr("Security"));
	pGroupSecurity->setDescription(tr("Application password, active frame lock, show content on active frame"));
	pGroupSecurity->setIcon(m_darkThemeStyle?  QLatin1String("gla_settingssecurity.png") : QLatin1String("gla_settingssecurity_inverted.png"));

	// Logging
	SettingsGroup *pGroupLogging = new SettingsGroup(this);
	pGroupLogging->setSgid(8);
	pGroupLogging->setName(tr("Logging"));
	pGroupLogging->setDescription(tr("Event logging, send log file to the support team"));
	pGroupLogging->setIcon(m_darkThemeStyle?  QLatin1String("gla_settingslogging.png") : QLatin1String("gla_settingslogging_inverted.png"));

	// Backup and Restore
	SettingsGroup *pGroupBackupandRestore = new SettingsGroup(this);
	pGroupBackupandRestore->setSgid(9);
	pGroupBackupandRestore->setName(tr("Backup and Restore"));
	pGroupBackupandRestore->setDescription(tr("Backup and restore utilities"));
	pGroupBackupandRestore->setIcon(m_darkThemeStyle?  QLatin1String("gla_toolsmaintenance.png") : QLatin1String("gla_toolsmaintenance_inverted.png"));

	SettingsGroup *pGroupReceipts = new SettingsGroup(this);
	pGroupReceipts->setSgid(10);
	pGroupReceipts->setName(tr("Receipts"));
	pGroupReceipts->setDescription(tr("Re parent receipt directory paths"));
	pGroupReceipts->setIcon(m_darkThemeStyle?  QLatin1String("gla_toolsreceipt.png") : QLatin1String("gla_toolsreceipt_inverted.png"));

	SettingsGroup *pGroupImportUtility = new SettingsGroup(this);
	pGroupImportUtility->setSgid(11);
	pGroupImportUtility->setName(tr("Import"));
	pGroupImportUtility->setDescription(tr("Import records from external data sources"));
	pGroupImportUtility->setIcon(m_darkThemeStyle?  QLatin1String("gla_toolsimport.png") : QLatin1String("gla_toolsimport_inverted.png"));

	m_settingGroupList << pGroupLogging << pGroupBackupandRestore << pGroupReceipts << pGroupImportUtility << pGroupSecurity << pGroupActiveFrame << pGroupAccounts << pGroupDefaults << pGroupTheme << pGroupGeneral << pGroupCurrencies;
}

QVariantList SettingsManager::settingGroupVariantList() {

	QVariantList list;
	QList<SettingsGroup*>::const_iterator i;
	for(i = m_settingGroupList.constBegin(); i != m_settingGroupList.constEnd(); i++){

		SettingsGroup *pGroup = *i;
		list << pGroup->toMap();
	}

	return list;
}

void SettingsManager::setSettingsValueFor(const QString &name, const QString &value) {

    QString path ;
    QString filename;

    path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) ;
    filename = "config.ini" ;
    QSettings settings(path + "/" + filename,QSettings::IniFormat) ;

//    QSettings settings(ORG_NAME,APP_NAME);

	qDebug() << "name : " << name << " , value : " << value;

    settings.setValue(name, value);
}

QString SettingsManager::settingsValueFor(const QString &keyval, const QString &defaultVal) {

//    QSettings settings(ORG_NAME,APP_NAME);
    QString path ;
    QString filename;

    path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) ;
    filename = "config.ini" ;
    QSettings settings(path + "/" + filename,QSettings::IniFormat) ;

	// If no value has been saved return the value.
    if (settings.value(keyval).isNull()) {
		return defaultVal;
	}

    return settings.value(keyval).toString();
}

void SettingsManager::setSettingsGroupValueFor(const QString &group, const QString &name, const QString &value) {

	qDebug() << "name : " << name << " , value : " << value << "group: " << group;

//    QSettings settings(ORG_NAME,APP_NAME);

    QString path ;
    QString filename;

    path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) ;
    filename = "config.ini" ;
    QSettings settings(path + "/" + filename,QSettings::IniFormat) ;

    settings.beginGroup(group);
    settings.setValue(name,value);
    settings.endGroup();
}

QString SettingsManager::settingsGroupValueFor(const QString &group, const QString &keyval, const QString &defaultVal) {

//    QSettings settings(ORG_NAME,APP_NAME);
    QString path ;
    QString filename;

    path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) ;
    filename = "config.ini" ;
    QSettings settings(path + "/" + filename,QSettings::IniFormat) ;


    settings.beginGroup(group);
    QString val = settings.value(keyval, defaultVal).toString();
    settings.endGroup();

	return val;
}

void SettingsManager::setupCurrencies(){

    QString path = QLatin1String(":/misc/currencycodes2.csv");
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "could not open the file to read currencies...";

        return;
    }

    QTextStream in(&file);
    QString text = in.readAll();

    QMap<QString, QString> helperMap;

    QString codeString;
    QString countryString;
    QStringList lines = text.split("\n", QString::SkipEmptyParts);
    foreach(QString line, lines)
    {

//		QStringList helperList = line.split("-");
        QStringList helperList = line.split("\t");
        codeString = helperList.first();
        codeString = codeString.trimmed();
        if (helperList.size() > 1) {
            countryString = helperList.at(helperList.size() - 2);
            countryString = countryString.trimmed();
            if (countryString.contains(QLatin1String("funds"))
                    || countryString.contains(QLatin1String("bond")))
                continue;
            helperMap[codeString] = countryString;
        }
    }

    m_globalCurrencyCode = settingsValueFor(QString("currency"), QString("USD"));

    m_currencyList.clear();

    QMap<QString, QString>::const_iterator i;
    for (i = helperMap.constBegin(); i != helperMap.constEnd(); i++) {

        QVariantMap map;
        map["code"] = i.key();
        map["country"] = i.value();

        m_currencyList << map;
    }
}
