#ifndef RECONCILEUTILITY_H
#define RECONCILEUTILITY_H

#include <QObject>
#include <QHash>

//! public access
#define RECONCILEUTILITY MyAccountantCloud::HelperClasses::ReconcileUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{


class ReconcileUtility : public QObject
{
    Q_OBJECT
private:

    explicit ReconcileUtility(QObject *parent = 0);

    ~ReconcileUtility() { }

public:

    /*!
     * \brief instance
     * \return
     */
    static ReconcileUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief transactionStatusName
     * \param transactionStatus
     * \return
     */
//    Q_INVOKABLE QString transactionStatusName(int transactionStatus) const;


    Q_INVOKABLE QString clearedName(bool cleared) const;

signals:

public slots:

    void clearInstance() { deleteInstance(); }


private:

//    QHash<int,QString> m_transactionStatusHash;

    const QString CLEAREDTITLE;
    const QString UNCLEAREDTITLE;


    static ReconcileUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // RECONCILEUTILITY_H
