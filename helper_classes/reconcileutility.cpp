#include "reconcileutility.h"

using namespace MyAccountantCloud::HelperClasses;

ReconcileUtility* ReconcileUtility::m_pInstance = NULL;

ReconcileUtility* ReconcileUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new ReconcileUtility();
    }

    return m_pInstance;
}

void ReconcileUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

ReconcileUtility::ReconcileUtility(QObject *parent) : CLEAREDTITLE(tr("Cleared")), UNCLEAREDTITLE(tr("Uncleared")), QObject(parent){

//    m_transactionStatusHash[0] = tr("Uncleared");
//    m_transactionStatusHash[1] = tr("Cleared");
//    m_transactionStatusHash[2] = tr("Reconciled");
//    m_transactionStatusHash[3] = tr("Cleared and Reconciled");
}

QString ReconcileUtility::clearedName(bool cleared) const{

    return cleared? CLEAREDTITLE : UNCLEAREDTITLE;
}


//QString ReconcileUtility::transactionStatusName(int transactionStatus) const {

//    return m_transactionStatusHash.value(transactionStatus);
//}
