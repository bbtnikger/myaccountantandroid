#ifndef LOGINUTILITY_H
#define LOGINUTILITY_H

#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>

//! public access
#define LOGINUTILITY MyAccountantCloud::HelperClasses::LoginUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

/*!
 * \brief The LoginUtility class
 */
class LoginUtility : public QObject
{
    Q_OBJECT

private:
    /*!
     * \brief LoginUtility
     * \param parent
     */
    explicit LoginUtility(QObject *parent = 0);

    /*!
     * \brief ~LoginUtility
     */
    ~LoginUtility();

public:

    /*!
     * \brief sendRequestToLogin
     * \param apiKeyId
     * \param apiKeySecret
     * \param username
     */
//    Q_INVOKABLE bool sendRequestToLogin(const QString &apiKeyId, const QString &apiKeySecret, const QString &username);
    Q_INVOKABLE bool sendRequestToLogin(const QString &username, const QString &password, const QString &organizationId);

    /*!
     * \brief sendRequestToLogout
     * \return
     */
    Q_INVOKABLE bool sendRequestToLogout();

    /*!
     * \brief instance
     * \return
     */
    static LoginUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

signals:

    void approveNeeded(const QString &html);

    void loginSuccess();
    void loginFailed();

public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onLoginReplyFinished
     * \param reply
     * \param apiKeyId
     * \param apiKeySecret
     * \param username
     * \return
     */
//    bool onLoginReplyFinished(QNetworkReply *reply, const QString &apiKeyId, const QString &apiKeySecret, const QString &username);
    bool onLoginReplyFinished(QNetworkReply *reply, const QString &username, const QString &password, const QString &organizationId);


    /*!
     * \brief onLogoutReplyFinished
     * \param reply
     * \return
     */
    bool onLogoutReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onLogoutRedirectFinished
     * \param reply
     * \return
     */
    bool onLogoutRedirectFinished(QNetworkReply *reply);


    bool onSendGetApiKeyRequestFinished(QNetworkReply *reply, const QString &accounthref);


    bool onSendGetTokenReuestFinished(QNetworkReply *reply);

private:


    bool sendGetTokenRequest(const QString &url);


    bool sendGetApiKeyRequest(const QString &href);


    bool redirectAfterLogout(const QString &url);

    /*!
     * \brief m_pInstance
     */
    static LoginUtility *m_pInstance;


    QString m_transaction_id;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // LOGINUTILITY_H
