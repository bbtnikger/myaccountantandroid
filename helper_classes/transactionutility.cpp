#include "transactionutility.h"
#include "networkutility.h"
#include "localuserutility.h"

#include <QNetworkRequest>
#include <QEventLoop>
#include <QUrlQuery>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

using namespace MyTransactionantCloud::HelperClasses;


TransactionUtility* TransactionUtility::m_pInstance = NULL;

TransactionUtility* TransactionUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new TransactionUtility();
    }

    return m_pInstance;
}

void TransactionUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

TransactionUtility::TransactionUtility(QObject *parent) : QObject(parent){

}


QVariant TransactionUtility::addTransaction(const QVariantMap &transaction){

    QNetworkRequest request;

    request.setUrl(QUrl(QLatin1String("http://localhost:3000/api/transactions")));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(transaction);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("transaction"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "transaction(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->post(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onAddTransactionReplyFinished(reply);
}

QVariantList TransactionUtility::getTransactions(){

    QNetworkRequest request;

    request.setUrl(QUrl(QLatin1String("http://localhost:3000/api/transactions")));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetTransactionsReplyFinished(reply);
}


QVariant TransactionUtility::onAddTransactionReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        map = _map.value(QLatin1String("transaction"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}

QVariantList TransactionUtility::onGetTransactionsReplyFinished(QNetworkReply *reply){

    QVariantList list;

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return list;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("transactions")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            QJsonArray arr = val.toArray();
            foreach (QJsonValue catval, arr) {

                list.append(catval.toVariant());
            }

            break;
        }

        return list;

    }else{

        qDebug() << parserError.errorString();
    }

    return list;
}

bool TransactionUtility::onDeleteTransactionReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << "content:" << content_;
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


//    QString access_token;
//    QJsonParseError parserError;
//    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
//    if(parserError.error == QJsonParseError::NoError){

//        qDebug() << json.toJson();

//        return true;

//    }else{

//        qDebug() << parserError.errorString();
//    }

    return true;
}

bool TransactionUtility::deleteTransaction(const QVariantMap &transaction){

    qDebug() << Q_FUNC_INFO << transaction;

    QNetworkRequest request;

    QString _id = transaction.value(QLatin1String("_id")).toString();
    qDebug() << "_id" << _id;
    request.setUrl(QUrl(QString::fromLatin1("http://localhost:3000/api/transactions/%1").arg(_id)));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->deleteResource(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onDeleteTransactionReplyFinished(reply);
}


QVariant TransactionUtility::updateTransaction(const QVariantMap &transaction){

    QNetworkRequest request;

    QString _id = transaction.value(QLatin1String("_id")).toString();
    qDebug() << "_id" << _id;
    request.setUrl(QUrl(QString::fromLatin1("http://localhost:3000/api/transactions/%1").arg(_id)));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(transaction);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("transaction"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "transaction(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->put(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onUpdateTransactionReplyFinished(reply);
}

QVariant TransactionUtility::onUpdateTransactionReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        return _map.value(QLatin1String("transaction"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}
