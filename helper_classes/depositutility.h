#ifndef DEPOSITUTILITY_H
#define DEPOSITUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define DEPOSITUTILITY MyAccountantCloud::HelperClasses::DepositUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class DepositUtility : public QObject
{
    Q_OBJECT
private:
    explicit DepositUtility(QObject *parent = 0);

    ~DepositUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static DepositUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addDeposit
     * \param deposit
     */
    Q_INVOKABLE QVariant addDeposit(const QVariantMap &deposit);


    /*!
     * \brief getDeposits
     * \return
     */
    Q_INVOKABLE QVariantList getDeposits(const QString &profile_id);

    /*!
     * \brief getDeposits
     * \param profile_id
     * \param queryParameters
     * \return
     */
    Q_INVOKABLE QVariantList getDeposits(const QString &profile_id, const QVariantMap &queryParameters);

    /*!
     * \brief deleteDeposit
     * \param deposit
     * \return
     */
    Q_INVOKABLE bool deleteDeposit(const QString &_id);

    /*!
     * \brief updateDeposit
     * \param deposit
     * \return
     */
    Q_INVOKABLE QVariant updateDeposit(const QString &_id, const QVariantMap &deposit);

signals:


public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddDepositReplyFinished
     * \param reply
     */
    QVariant onAddDepositReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetDepositsReplyFinished
     * \param reply
     */
    QVariantList onGetDepositsReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteDepositReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteDepositReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateDepositReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateDepositReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static DepositUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // DEPOSITUTILITY_H
