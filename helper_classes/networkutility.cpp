#include "networkutility.h"
#include "localuserutility.h"
#include "LoggerService.h"

#include <QDebug>
#include <QAuthenticator>
#include <QNetworkProxy>
#include <QFile>
#include <QSslCipher>

using namespace MyAccountantCloud::HelperClasses;
using namespace LoggingService;

NetworkUtility* NetworkUtility::m_pInstance = NULL;

NetworkUtility* NetworkUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new NetworkUtility();
    }

    return m_pInstance;
}

void NetworkUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

NetworkUtility::NetworkUtility(QObject *parent)
    : m_pNetworkManager(new QNetworkAccessManager(this)),
      QObject(parent){

    bool res = connect ( m_pNetworkManager, SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)), this, SLOT(onAuthenticationRequestSlot(QNetworkReply*,QAuthenticator*)) );
    Q_ASSERT(res);

    res = connect(m_pNetworkManager,SIGNAL(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)),this,SLOT(onProxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)));
    Q_ASSERT(res);

//    loadSSLCertificate(QLatin1String(":/client.pem"),QLatin1String("NikGer1@"));
}

void NetworkUtility::onAuthenticationRequestSlot(QNetworkReply *aReply, QAuthenticator *aAuthenticator){

    qDebug() << __FUNCTION__;

    QString code_ = LOCALUSERUTILITY->code();
    if(code_.isEmpty()){

        QString username = LOCALUSERUTILITY->username();
        QString password = LOCALUSERUTILITY->password();

        qDebug() << "username:" << username << ",password:" << password;

        aAuthenticator->setUser(username);
        aAuthenticator->setPassword(password);

    }else{

        QString clientId = LOCALUSERUTILITY->clientId();
        QString clientSecret = LOCALUSERUTILITY->clientSecret();

        qDebug() << "clientId:" << clientId << ",clientSecret:" << clientSecret;

        aAuthenticator->setUser(clientId);
        aAuthenticator->setPassword(clientSecret);
    }
}

void NetworkUtility::onProxyAuthenticationRequired(const QNetworkProxy & proxy, QAuthenticator *authenticator){

    Q_UNUSED(proxy);
    qDebug() << __FUNCTION__;

    QString code_ = LOCALUSERUTILITY->code();
    if(code_.isEmpty()){

        QString username = LOCALUSERUTILITY->username();
        QString password = LOCALUSERUTILITY->password();

        qDebug() << "username:" << username << ",password:" << password;

        authenticator->setUser(username);
        authenticator->setPassword(password);

    }else{

        QString clientId = LOCALUSERUTILITY->clientId();
        QString clientSecret = LOCALUSERUTILITY->clientSecret();

        qDebug() << "clientId:" << clientId << ",clientSecret:" << clientSecret;

        authenticator->setUser(clientId);
        authenticator->setPassword(clientSecret);
    }
}

void NetworkUtility::onNetworkError(QNetworkReply::NetworkError error){

//    Q_ASSERT(m_pNetworkConfigurationManager);

//    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, onNetworkError, error code: %1").arg(error),LoggerService::LOGGTYPE_ERROR);

    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    if(!reply)
        return;

    QString urlpath = reply->url().toString();
    qDebug() << "Reply URL" << urlpath;

    QString reply_content = reply->readAll();
    qDebug() << QString::fromLatin1("Log type: Error, read reply content:%1").arg(reply_content);
//    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, read reply content: %1").arg(reply_content),LoggerService::LOGGTYPE_ERROR);

    int httpstatus = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute).toInt();
    qDebug() << QString::fromLatin1("Log type: Error, HTTP status code: %1").arg(httpstatus);
//    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, HTTP status code: %1").arg(httpstatus),LoggerService::LOGGTYPE_ERROR);

//    if(!m_pNetworkConfigurationManager){

//        LOGGERSERVICE->logMessage(QLatin1String("Log type: Error, The Network configuration manager is null"),LoggerService::LOGGTYPE_ERROR);
//        return;
//    }

    m_networkErrorString = reply->errorString();

    setNetworkError(error);

    if(error == QNetworkReply::NoError){

//        LOGGERSERVICE->logMessage(QString::fromLatin1("Network Error: No Error"),LoggerService::LOGGTYPE_ERROR);
        return;
    }

    qDebug() << QString::fromLatin1("Log type: Error, Network Error shown: (%1), error string from network reply object: (%2), error code:(%3)").arg(m_networkErrorString).arg(reply->errorString()).arg(error);
//    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, Network Error shown: %1, error string from network reply object : %2, error code:%3").arg(m_networkErrorString).arg(reply->errorString()).arg(error),LoggerService::LOGGTYPE_ERROR);
}

void NetworkUtility::loadSSLCertificate(const QString &certPrivateKeyPath, const QString &passPhrase){

//    Q_ASSERT(m_pNetworkConfigurationManager);

    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, getSslConfiguration, trying to load certificate from file: %1").arg(certPrivateKeyPath));

    QString path = certPrivateKeyPath;
    if(path.startsWith(QLatin1String("file://")))
        path = path.remove(QLatin1String("file://"));

    QFile file(path);
    if(!file.exists()){

        qDebug() << "file does not exist";
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, setupPrivateSslKey, File does not exist, file path : %1").arg(path),LoggerService::LOGGTYPE_ERROR);
        return ;

    }else{

        qDebug() << QLatin1String("Log type: Info, File exists");
        LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, File exists"));
    }

    if(!file.open(QIODevice::ReadOnly)){

        qDebug() << "Could not read the file.";
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, setupPrivateSslKey, Could not open the file for reading, file path : %1 ").arg(path),LoggerService::LOGGTYPE_ERROR);

        return;

    }else{

        qDebug() << QLatin1String("Log type: Info, file open");
        LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, file open"));
    }

    QByteArray pemdata = file.readAll();
    file.close();


    QSslConfiguration defaultsslconf = QSslConfiguration::defaultConfiguration();

    //
    // PRIVATE KEY
    //
    LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, Trying to load private key from data"));

    QSslKey sslkey = QSslKey(pemdata,QSsl::Rsa,QSsl::Pem,QSsl::PrivateKey,passPhrase.toUtf8());

    //
    // SSL KEY
    //
    bool isnull = sslkey.isNull();
    QString isnullstring = isnull? "IS NULL" : "IS NOT NULL";
    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, SSL Key info: %1, type: %2, algorithm: %3").arg(isnullstring).arg(sslkey.type()).arg(sslkey.algorithm()));
    qDebug() << QString::fromLatin1("Log type: Info, SSL Key info: %1, type: %2, algorithm: %3").arg(isnullstring).arg(sslkey.type()).arg(sslkey.algorithm());

    if(!isnull){

        qDebug() << QLatin1String("Set SSL Private Key to the SSL Configuration");
        LOGGERSERVICE->logMessage(QLatin1String("Set SSL Private Key to the SSL Configuration"));
    }else{

        qDebug() << QLatin1String("ERROR: Could not set SSL Private Key to the SSL Configuration");
        LOGGERSERVICE->logMessage(QLatin1String("Set SSL Private Key to the SSL Configuration"),LoggerService::LOGGTYPE_ERROR);
    }

    m_sslConfiguration.setPrivateKey(sslkey);


    QSslCertificate localCertificate;


    QList<QSslCertificate> caCertificates;

    //
    // PROTOCOL - CIPHERS
    //
//    m_sslConfiguration.setProtocol(defaultsslconf.protocol());
//    m_sslConfiguration.setCiphers(defaultsslconf.ciphers());

    QList<QSslCertificate> certsFromFile = QSslCertificate::fromData(pemdata, QSsl::Pem);
    if(certsFromFile.isEmpty()){

        LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, Certificate list from data is empty"));
        qDebug() << QLatin1String("Log type: Info, Certificate list from data is empty");
    }

    //
    // CERTIFICATES
    //
    qDebug() << QLatin1String("Log type: Info, Trying to load certificates from data");
    LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, Trying to load certificates from data"));

    foreach(QSslCertificate cert,caCertificates){

        QString info = QString::fromLatin1("Organization:%1,CommonName:%2,LocalityName:%3,OrganizationalUnitName:%4,CountryName:%5,StateOrProvinceName:%6").arg(
                cert.issuerInfo(QSslCertificate::Organization).join(QLatin1String("/"))).arg(cert.issuerInfo(QSslCertificate::CommonName).join(QLatin1String("/"))).arg(cert.issuerInfo(QSslCertificate::LocalityName).join(QLatin1String("/"))).arg(
                cert.issuerInfo(QSslCertificate::OrganizationalUnitName).join(QLatin1String("/"))).arg(cert.issuerInfo(QSslCertificate::CountryName).join(QLatin1String("/"))).arg(cert.issuerInfo(QSslCertificate::StateOrProvinceName).join(QLatin1String("/")));
        QString certString = QString::fromLatin1("Version:%1, Serial number: %2, Issuer:%3, Effective date:%4, Expire date: %5").arg(QString(cert.version())).arg(QString(cert.serialNumber())).arg(info).arg(cert.effectiveDate().toString(Qt::ISODate)).arg(cert.expiryDate().toString(Qt::ISODate));
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Certificate from data info : %1").arg(certString));
        qDebug() << QString::fromLatin1("Log type: Info, Certificate from data info : %1").arg(certString);
    }

    if(!certsFromFile.isEmpty()){

        // TODO
        // Which is the local certificate from the list of certificates?
        //
        // check each certificate in the list each one step by step.
        //
        localCertificate = certsFromFile.first();
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Set the first certificate found as the Local Certificate "));
    }

    QList<QSslCertificate> certs;

    //
    // get current certificates from default config
    //
    // print the current ca Certificates
    // it may help in picking up the right local certificate
    //
    certs.append(defaultsslconf.caCertificates());

    if(certsFromFile.length()>1){
//
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Add both certificate found to the CA Certificate directory"));
        certs.append(certsFromFile);
    }
    certs.append(caCertificates);

    m_sslConfiguration.setCaCertificates(caCertificates);

    if(!localCertificate.isNull()){

        m_sslConfiguration.setLocalCertificate(localCertificate);

        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Local Certificate set to the SSL Configuration."));
        qDebug() << QString::fromLatin1("Log type: Info, Local Certificate set to the SSL Configuration.");

    }else{

        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Local Certificate NOT set to the SSL Configuration as it is not valid."),LoggerService::LOGGTYPE_ERROR);
        qDebug() << QString::fromLatin1("Log type: Info, Local Certificate NOT set to the SSL Configuration as it is not valid.");
    }


    qDebug() << "m_sslConfiguration.isNull:" << m_sslConfiguration.isNull();

    QSslConfiguration::setDefaultConfiguration(m_sslConfiguration);

    m_networkConfigurationManager.updateConfigurations();
}

void NetworkUtility::onSslErrors(const QList<QSslError> &list){

    m_sslErrorStringList.clear();
    m_sslErrorList.clear();

    foreach (const QSslError &error, list){

        m_sslErrorStringList << QString::fromLatin1("ssl error code:%1,%2").arg(error.error()).arg(error.errorString());
    }

    m_sslErrorList = list;

    QString error_ = m_sslErrorStringList.join(QLatin1String("\n"));

    setSslErrorList(list);

    if(list.isEmpty()){

        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, SSL Error List is empty"),LoggerService::LOGGTYPE_ERROR);
        qDebug() << QString::fromLatin1("Log type: Error, SSL Error List is empty");
        return;
    }

    qDebug() << QString::fromLatin1("Log type: Error, SSL error list:\n%1").arg(error_);
    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, SSL error list:\n%1").arg(error_),LoggerService::LOGGTYPE_ERROR);
}

QSslConfiguration NetworkUtility::getSslConfiguration(QSslConfiguration &sslConfiguration, const QString &certPrivateKeyPath, const QString &passPhrase, bool *hasSSKKeyError){

    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, getSslConfiguration, trying to load certificate from file: %1").arg(certPrivateKeyPath));

    QString path = certPrivateKeyPath;
    if(path.startsWith(QLatin1String("file://")))
        path = path.remove(QLatin1String("file://"));

    QFile file(path);
    if(!file.exists()){

        qDebug() << "file does not exist";
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, setupPrivateSslKey, File does not exist, file path : %1").arg(path),LoggerService::LOGGTYPE_ERROR);
        return sslConfiguration;

    }else{

        LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, File exists"));
        qDebug() << QLatin1String("Log type: Info, File exists");
    }

    if(!file.open(QIODevice::ReadOnly)){

        qDebug() << "Could not read the file.";
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Error, setupPrivateSslKey, Could not open the file for reading, file path : %1 ").arg(path),LoggerService::LOGGTYPE_ERROR);

        return sslConfiguration;

    }else{


        LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, file open"));
        qDebug() << QLatin1String("Log type: Info, file open");
    }

    QByteArray pemdata = file.readAll();

    file.close();

    //
    // PRIVATE KEY
    //
    LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, Trying to load private key from data"));
    qDebug() << QLatin1String("Log type: Info, Trying to load private key from data");

    QSslKey sslKey = QSslKey(pemdata,QSsl::Rsa,QSsl::Pem,QSsl::PrivateKey,passPhrase.toUtf8());

    bool isnull = sslKey.isNull();
    QString isnullstring = isnull? "IS NULL" : "IS NOT NULL";

    //
    LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, SSL Key info: %1, type: %2, algorithm: %3").arg(isnullstring).arg(sslKey.type()).arg(sslKey.algorithm()));
    qDebug() << QString::fromLatin1("Log type: Info, SSL Key info: %1, type: %2, algorithm: %3").arg(isnullstring).arg(sslKey.type()).arg(sslKey.algorithm());

    if(hasSSKKeyError){

        if(isnull){

            *hasSSKKeyError = true;

            LOGGERSERVICE->logMessage(QLatin1String("Log type: Error, Exiting function, cause: SSL Key is NULL probably because of a wrong password"),LoggerService::LOGGTYPE_ERROR);
            qDebug() << QLatin1String("Log type: Error, Exiting function, cause: SSL Key is NULL probably because of a wrong password");

            return sslConfiguration;

        }else{

            *hasSSKKeyError = false;
        }
    }

    //
    // CERTIFICATES
    //
    LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, Trying to load certificates from data"));
    qDebug() << QLatin1String("Log type: Info, Trying to load certificates from data");

    QList<QSslCertificate> certsFromFile = QSslCertificate::fromData(pemdata, QSsl::Pem);
    if(certsFromFile.isEmpty()){

        LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, Certificate list from data is empty"));
        qDebug() << QLatin1String("Log type: Info, Certificate list from data is empty");
    }

    QSslCertificate localCertificate;
    foreach(QSslCertificate cert,certsFromFile){

        QString info = QString::fromLatin1("Organization:%1,CommonName:%2,LocalityName:%3,OrganizationalUnitName:%4,CountryName:%5,StateOrProvinceName:%6").arg(
                cert.issuerInfo(QSslCertificate::Organization).join(QLatin1String("/"))).arg(cert.issuerInfo(QSslCertificate::CommonName).join(QLatin1String("/"))).arg(cert.issuerInfo(QSslCertificate::LocalityName).join(QLatin1String("/"))).arg(
                cert.issuerInfo(QSslCertificate::OrganizationalUnitName).join(QLatin1String("/"))).arg(cert.issuerInfo(QSslCertificate::CountryName).join(QLatin1String("/"))).arg(cert.issuerInfo(QSslCertificate::StateOrProvinceName).join(QLatin1String("/")));
        QString certString = QString::fromLatin1("Version:%1, Serial number: %2, Issuer:%3, Effective date:%4, Expire date: %5, is null: %6").arg(QString(cert.version())).arg(QString(cert.serialNumber())).arg(info).arg(cert.effectiveDate().toString(Qt::ISODate)).arg(cert.expiryDate().toString(Qt::ISODate)).arg(cert.isNull());
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Certificate from data info : %1").arg(certString));
        qDebug() << QString::fromLatin1("Log type: Info, Certificate from data info : %1").arg(certString);
    }


    if(!certsFromFile.isEmpty()){

        // TODO
        // Which is the local certificate from the list of certificates?
        //
        // check each certificate in the list each one step by step.
        //
        localCertificate = certsFromFile.first();
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Set the first certificate found as the Local Certificate "));
        qDebug() << QString::fromLatin1("Log type: Info, Set the first certificate found as the Local Certificate ");
    }

    //
    // SETUP
    //

    QList<QSslCertificate> certs;

    //
    // get current certificates from default config
    //
    // print the current ca Certificates
    // it may help in picking up the right local certificate
    //
    certs.append(sslConfiguration.caCertificates());

    //
    // get custom certificates loaded from file previously
    //
    if(certsFromFile.length()>0){
//
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Add both certificate found to the CA Certificate directory"));
        qDebug() << QString::fromLatin1("Log type: Info, Add both certificate found to the CA Certificate directory");
        certs.append(certsFromFile);
    }

    //
    // set it up in new config
    //
    sslConfiguration.setCaCertificates(certs);
    if(!localCertificate.isNull()){

        sslConfiguration.setLocalCertificate(localCertificate);
        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Local Certificate set to the SSL Configuration."));
        qDebug() << QString::fromLatin1("Log type: Info, Local Certificate set to the SSL Configuration.");

    }else{

        LOGGERSERVICE->logMessage(QString::fromLatin1("Log type: Info, Local Certificate NOT set to the SSL Configuration as it is not valid."),LoggerService::LOGGTYPE_ERROR);
        qDebug() << QString::fromLatin1("Log type: Info, Local Certificate NOT set to the SSL Configuration as it is not valid.");
    }


    // check that the key is not null
//	if(!isnull)
    sslConfiguration.setPrivateKey(sslKey);

    //
    LOGGERSERVICE->logMessage(QLatin1String("Log type: Info, set it up in new config "));
    qDebug() << QLatin1String("Log type: Info, set it up in new config ");

    QSslConfiguration::setDefaultConfiguration(sslConfiguration);

    //
//    if(m_networkConfigurationManager)
        m_networkConfigurationManager.updateConfigurations();

    return sslConfiguration;
}

NetworkUtility::~NetworkUtility(){

    qDebug() << __FUNCTION__;
}
