/*
 * SettingsManager.h
 *
 *  Created on: Nov 15, 2013
 *      Author: Nick
 */

#ifndef SETTINGSMANAGER_H_
#define SETTINGSMANAGER_H_

#include <qobject.h>
#include <QSettings>
#include <QVariantList>

//! public access
#define SETTINGSMANAGER MyAccountantCloud::HelperClasses::SettingsManager::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class SettingsGroup : public QObject{
	Q_OBJECT
	Q_PROPERTY( int sgid 				READ sgid 			WRITE setSgid			NOTIFY sgidChanged )
	Q_PROPERTY( QString name 			READ name 			WRITE setName			NOTIFY nameChanged)
	Q_PROPERTY( QString description 	READ description 	WRITE setDescription	NOTIFY descriptionChanged)
	Q_PROPERTY( QString icon 			READ icon 			WRITE setIcon			NOTIFY iconChanged)

public:
	SettingsGroup(QObject *parent = 0) : m_sgid(0), QObject(parent) { }

	int sgid() const { return m_sgid; }
	void setSgid(int sg) { m_sgid = sg; }

	QString name() const { return m_name; }
	void setName(const QString &n) { m_name = n; }

	QString description() const { return m_description; }
	void setDescription(const QString &s) { m_description = s; }

	QString icon() const { return m_icon; }
	void setIcon(const QString &i) { m_icon = i; }

	QVariantMap toMap() const ;

signals:
	void sgidChanged();
	void nameChanged();
	void descriptionChanged();
	void iconChanged();

private:
	int 	m_sgid;
	QString m_name;
	QString m_description;
	QString m_icon;
};

class SettingsManager: public QObject {

	Q_OBJECT
    Q_PROPERTY(QVariantList currencyList            READ currencyList           WRITE setCurrencyList           NOTIFY currencyListChanged)
    Q_PROPERTY(QString      globalCurrencyCode      READ globalCurrencyCode     WRITE setGlobalCurrencyCode     NOTIFY globalCurrencyCodeChanged)

protected:

	/*!
	 *
	 */
    SettingsManager(QObject *parent = 0);

    /*!
     *
     */
    virtual ~SettingsManager();


public:

    /*!
     * \brief instance
     * \return
     */
    static SettingsManager* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

	/*!
	 *
	 */
	Q_INVOKABLE QVariantList settingGroupVariantList() ;

	/*!
	 *
	 */
	Q_INVOKABLE void clear() { qDeleteAll(m_settingGroupList); m_settingGroupList.clear(); }

	/*!
	 *
	 */
	Q_INVOKABLE void initialize();

	/*!
	 *
	 */
	Q_INVOKABLE void setDarkThemeStyle(bool dts) { m_darkThemeStyle = dts; }


	Q_INVOKABLE
	void setSettingsValueFor(const QString&, const QString&);

	Q_INVOKABLE
	QString settingsValueFor(const QString&, const QString&);

	Q_INVOKABLE
	void setSettingsGroupValueFor(const QString &group, const QString&, const QString&);

	Q_INVOKABLE
	QString settingsGroupValueFor(const QString &group, const QString&, const QString&);


    QVariantList currencyList() const { return m_currencyList; }
    void setCurrencyList(const QVariantList &list) { m_currencyList = list; }

    QString globalCurrencyCode() const { return m_globalCurrencyCode; }
    void setGlobalCurrencyCode(const QString &c) { m_globalCurrencyCode = c; }

public slots:

    void clearInstance() { deleteInstance(); }

signals:

    void currencyListChanged();
    void globalCurrencyCodeChanged();

private:

    void setupCurrencies();

    QString m_globalCurrencyCode;

    QList<SettingsGroup*> m_settingGroupList;

    bool m_darkThemeStyle;

    QVariantList m_currencyList;

    /*!
     * \brief m_pInstance
     */
    static SettingsManager *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif /* SETTINGSMANAGER_H_ */
