#ifndef PAYMENTMETHODUTILITY_H
#define PAYMENTMETHODUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define PAYMENTMETHODUTILITY MyAccountantCloud::HelperClasses::PaymentMethodUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class PaymentMethodUtility : public QObject
{
    Q_OBJECT
private:
    explicit PaymentMethodUtility(QObject *parent = 0);

    ~PaymentMethodUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static PaymentMethodUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addPaymentMethod
     * \param paymentmethod
     */
    Q_INVOKABLE QVariant addPaymentMethod(const QVariantMap &paymentmethod);


    /*!
     * \brief getPaymentMethods
     * \return
     */
    Q_INVOKABLE QVariantList getPaymentMethods(const QString &profile_id);


    /*!
     * \brief deletePaymentMethod
     * \param paymentmethod
     * \return
     */
    Q_INVOKABLE bool deletePaymentMethod(const QString &_id);

    /*!
     * \brief updatePaymentMethod
     * \param paymentmethod
     * \return
     */
    Q_INVOKABLE QVariant updatePaymentMethod(const QString &_id, const QVariantMap &paymentmethod);

signals:


public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddPaymentMethodReplyFinished
     * \param reply
     */
    QVariant onAddPaymentMethodReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetPaymentMethodsReplyFinished
     * \param reply
     */
    QVariantList onGetPaymentMethodsReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeletePaymentMethodReplyFinished
     * \param reply
     * \return
     */
    bool onDeletePaymentMethodReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdatePaymentMethodReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdatePaymentMethodReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static PaymentMethodUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // PAYMENTMETHODUTILITY_H
