#ifndef HISTORYUTILITY_H
#define HISTORYUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define HISTORYUTILITY MyAccountantCloud::HelperClasses::HistoryUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class HistoryUtility : public QObject{

    Q_OBJECT

private:

    /*!
     * \brief HistoryUtility
     * \param parent
     */
    explicit HistoryUtility(QObject *parent = 0);


public:

    /*!
     * \brief instance
     * \return
     */
    static HistoryUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief getHistory
     * \return
     */
    Q_INVOKABLE QVariantMap getHistory(const QString &profile_id);

    /*!
     * \brief getHistory
     * \param queryParameters
     * \return
     */
    Q_INVOKABLE QVariantMap getHistory(const QVariantMap &queryParameters);


    /*!
     * \brief getAccountHistory
     * \param profile_id
     * \param account_id
     * \return
     */
    Q_INVOKABLE QVariantMap getAccountHistory(const QString &profile_id, const QString &account_id);

signals:


public slots:

    void clearInstance() { deleteInstance(); }


private:


//    QVariantList createTransactionList(const QVariantList &list, int ttype) const;


//    QVariantMap createTransactionMap(const QVariantMap &record, int ttype) const;


//    QVariantMap sortTransactionList(const QVariantList &list) const;


    void sortAccountHistoryMap(QVariantMap &map);

    /*!
     * \brief onGetPayeesReplyFinished
     * \param reply
     */
    QVariantMap onGetHistoryReplyFinished(QNetworkReply *reply);


    /*!
     * \brief m_pInstance
     */
    static HistoryUtility *m_pInstance;

};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // HISTORYUTILITY_H
