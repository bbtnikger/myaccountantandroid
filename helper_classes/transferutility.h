#ifndef TRANSFERUTILITY_H
#define TRANSFERUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define TRANSFERUTILITY MyAccountantCloud::HelperClasses::TransferUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class TransferUtility : public QObject
{
    Q_OBJECT
private:
    explicit TransferUtility(QObject *parent = 0);

    ~TransferUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static TransferUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addTransfer
     * \param transfer
     */
    Q_INVOKABLE QVariant addTransfer(const QVariantMap &transfer);


    /*!
     * \brief getTransfers
     * \return
     */
    Q_INVOKABLE QVariantList getTransfers(const QString &profile_id);

    /*!
     * \brief getIncomes
     * \param profile_id
     * \param queryParameters
     * \return
     */
    Q_INVOKABLE QVariantList getTransfers(const QString &profile_id, const QVariantMap &queryParameters);

    /*!
     * \brief deleteTransfer
     * \param transfer
     * \return
     */
    Q_INVOKABLE bool deleteTransfer(const QString &_id);

    /*!
     * \brief updateTransfer
     * \param transfer
     * \return
     */
    Q_INVOKABLE QVariant updateTransfer(const QString &_id, const QVariantMap &transfer);

signals:


public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddTransferReplyFinished
     * \param reply
     */
    QVariant onAddTransferReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetTransfersReplyFinished
     * \param reply
     */
    QVariantList onGetTransfersReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteTransferReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteTransferReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateTransferReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateTransferReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static TransferUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // TRANSFERUTILITY_H
