#include "historyutility.h"
#include "networkutility.h"
#include "localuserutility.h"
#include "LoggerService.h"

#include <QNetworkRequest>
#include <QEventLoop>
#include <QUrlQuery>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

using namespace MyAccountantCloud::HelperClasses;

HistoryUtility* HistoryUtility::m_pInstance = NULL;

HistoryUtility* HistoryUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new HistoryUtility();
    }

    return m_pInstance;
}

void HistoryUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

HistoryUtility::HistoryUtility(QObject *parent) : QObject(parent){

}

QVariantMap HistoryUtility::getHistory(const QString &profile_id){

    QNetworkRequest request;

    qDebug() << Q_FUNC_INFO << profile_id;
    LOGGERSERVICE->logMessage(QString::fromLatin1("%1 - %2").arg(QString::fromUtf8(Q_FUNC_INFO)).arg(profile_id));

    QString url = QString::fromLatin1("%1/%2?").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/history"));
    QUrlQuery urlquery(url);
    urlquery.addQueryItem(QLatin1String("profile_id"),profile_id);
    request.setUrl(QUrl(urlquery.toString()));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetHistoryReplyFinished(reply);
}

QVariantMap HistoryUtility::getAccountHistory(const QString &profile_id, const QString &account_id){

    QNetworkRequest request;

    qDebug() << Q_FUNC_INFO << profile_id << account_id;
    LOGGERSERVICE->logMessage(QString::fromLatin1("%1 - %2,%3").arg(QString::fromUtf8(Q_FUNC_INFO)).arg(profile_id).arg(account_id));

    QString url = QString::fromLatin1("%1/%2?").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/history/account"));
    QUrlQuery urlquery(url);
    urlquery.addQueryItem(QLatin1String("profile_id"),profile_id);
    urlquery.addQueryItem(QLatin1String("account_id"),account_id);
    request.setUrl(QUrl(urlquery.toString()));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetHistoryReplyFinished(reply);
}

QVariantMap HistoryUtility::getHistory(const QVariantMap &queryParameters){

    QNetworkRequest request;

    qDebug() << Q_FUNC_INFO << queryParameters.count();
    LOGGERSERVICE->logMessage(QString::fromLatin1("%1 - %2").arg(QString::fromUtf8(Q_FUNC_INFO)).arg(queryParameters.count()));

    QString url = QString::fromLatin1("%1/%2?").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/history"));
    QUrlQuery urlquery(url);
    QVariantMap::const_iterator i;
    for(i = queryParameters.constBegin(); i != queryParameters.constEnd(); i++){

        QString queryparamkey   = i.key();
        QString queryparamvalue = i.value().toString();

        urlquery.addQueryItem(queryparamkey,queryparamvalue);
    }

    request.setUrl(QUrl(urlquery.toString()));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetHistoryReplyFinished(reply);
}

QVariantMap HistoryUtility::onGetHistoryReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariantMap map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("history")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            QJsonObject jsonobj = val.toObject();

            map = jsonobj.toVariantMap();

            break;
        }

    }else{

        qDebug() << parserError.errorString();
        LOGGERSERVICE->logMessage(QString::fromLatin1("GET api history failed,error(%1)").arg(parserError.errorString()));
    }

//    qDebug() << map;

//    sortAccountHistoryMap(map);

    return map;
}

//QVariantMap HistoryUtility::sortTransactionList(const QVariantList &list) const{

//    QVariantMap smap;

//    QVariantList::const_iterator i;
//    for(i = list.constBegin(); i != list.constEnd(); i++){

//        QVariant var = *i;
//        QVariantMap map = var.toMap();

//        QString dt = map[QLatin1String("date")].toString();

//        smap.insert(dt,map);
//    }

//    return smap;
//}

//QVariantMap HistoryUtility::createTransactionMap(const QVariantMap &record, int ttype) const{

//    QVariantMap map = record;

//    QDate dt = QDate::fromString(record.value(QLatin1String("date")).toString(),Qt::ISODate);

//    map.insert(QLatin1String("datename"),dt.toString(Qt::SystemLocaleShortDate));
//    map.insert(QLatin1String("accountname"),)

//    record_.datename = LocalUserUtility.localizedDate(dt);
//    record_.accountname = record_.origin.name;
//    record_.type = ttypestring;
//    record_.transactionname = record_.destination.name;
//    record_.ttype = ttype;
//    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
//    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);
//}

//QVariantList HistoryUtility::createTransactionList(const QVariantList &list, int ttype) const{

//    QVariantList newlist;

//    QVariantList::const_iterator i;
//    for(i = list.constBegin(); i != list.constEnd(); i++){

//        QVariant var = *i;
//        QVariantMap map = var.toMap();

//        QVariantMap newmap = createTransactionMap(map,ttype);
//    }


//    return newlist;
//}

//void HistoryUtility::sortAccountHistoryMap(QVariantMap &map){

//    QVariantMap transactionsMap = map[QLatin1String("transactions")].toMap();
//    QVariantMap balanceMap = map[QLatin1String("transactions")].toMap();

//    QVariantList fulllist;

//    QVariantList expensesList = transactionsMap[QLatin1String("expenses")].toList();
//    QVariantList expenseTList = createTransactionList(expensesList);

//    QVariantList incomeList = transactionsMap[QLatin1String("incomes")].toList();
//    QVariantList incomeTList = createTransactionList(incomeList);

//    QVariantList depositList = transactionsMap[QLatin1String("deposits")].toList();


//    QVariantMap transferMap = transactionsMap[QLatin1String("transfers")].toMap();
//    QVariantList originTransferList = transferMap[QLatin1String("origin")].toList();
//    QVariantList destinationTransferList = transferMap[QLatin1String("destination")].toList();

//    fulllist.append(expensesList);
//    fulllist.append(incomeList);
//    fulllist.append(depositList);
//    fulllist.append(originTransferList);
//    fulllist.append(destinationTransferList);

//    QVariantMap sortedTransactionsMap = sortTransactionList(fulllist);



//}
