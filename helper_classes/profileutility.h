#ifndef PROFILEUTILITY_H
#define PROFILEUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define PROFILEUTILITY MyAccountantCloud::HelperClasses::ProfileUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class ProfileUtility : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currentProfile READ currentProfile WRITE setCurrentProfile NOTIFY currentProfileChanged)

private:
    explicit ProfileUtility(QObject *parent = 0);

    ~ProfileUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static ProfileUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addProfile
     * \param profile
     */
    Q_INVOKABLE QVariant addProfile(const QVariantMap &profile);


    /*!
     * \brief getProfiles
     * \return
     */
    Q_INVOKABLE QVariantList getProfiles();


    /*!
     * \brief deleteProfile
     * \param profile
     * \return
     */
    Q_INVOKABLE bool deleteProfile(const QString &_id);

    /*!
     * \brief updateProfile
     * \param profile
     * \return
     */
    Q_INVOKABLE QVariant updateProfile(const QString &_id, const QVariantMap &profile);


    QString currentProfile() const { return m_currentProfile; }
    void setCurrentProfile(const QString &map) { m_currentProfile = map; }

signals:
    void currentProfileChanged();

public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddProfileReplyFinished
     * \param reply
     */
    QVariant onAddProfileReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetProfilesReplyFinished
     * \param reply
     */
    QVariantList onGetProfilesReplyFinished(QNetworkReply *reply);
//    QVariantList onGetProfilesReplyFinished();

    /*!
     * \brief onDeleteProfileReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteProfileReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateProfileReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateProfileReplyFinished(QNetworkReply *reply);


signals:

    void profilesFetchedSuccessfully();
    void profilesFetchedFailed();

private:

    /*!
     * \brief m_pInstance
     */
    static ProfileUtility *m_pInstance;


    QString m_currentProfile;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // PROFILEUTILITY_H
