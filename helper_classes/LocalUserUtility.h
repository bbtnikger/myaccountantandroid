/*
 * LocalUserUtility.h
 *
 *  Created on: May 28, 2015
 *      Author: Nick
 */

#ifndef LOCALUSERUTILITY_H_
#define LOCALUSERUTILITY_H_

#include <QObject>
#include <QDateTime>
#include <QSettings>
#include <QLocale>

#define LOCALUSERUTILITY MyAccountantCloud::HelperClasses::LocalUserUtility::instance()

/*!
 *
 */
namespace MyAccountantCloud {

/*!
 *
 */
namespace HelperClasses{

/*!
 * \brief The LocalUserUtility class
 */
class LocalUserUtility: public QObject {
    Q_OBJECT
    Q_PROPERTY( QString 	domain                      READ domain                     WRITE setDomain                     NOTIFY domainChanged)
//    Q_PROPERTY( QString 	facadeVersion 				READ facadeVersion				WRITE setFacadeVersion  			NOTIFY facadeVersionChanged)
    Q_PROPERTY( QString 	organizationId              READ organizationId             WRITE setOrganizationId 			NOTIFY organizationIdChanged)
    Q_PROPERTY( QString 	eMailAddress 				READ eMailAddress				WRITE setEMailAddress  				NOTIFY eMailAddressChanged)
    Q_PROPERTY( QString 	authToken 					READ authToken					WRITE setAuthToken  				NOTIFY authTokenChanged)
    Q_PROPERTY( QString 	username 					READ username					WRITE setUsername  					NOTIFY usernameChanged)
    Q_PROPERTY( QString     clientId                    READ clientId                   WRITE setClientId                   NOTIFY clientIdChanged)
    Q_PROPERTY( QString     clientSecret                READ clientSecret               WRITE setClientSecret               NOTIFY clientSecretChanged)
    Q_PROPERTY( QString 	password 					READ password					WRITE setPassword  					NOTIFY passwordChanged)
    Q_PROPERTY( QString     code                        READ code                       WRITE setCode                       NOTIFY codeChanged)
//    Q_PROPERTY( bool 		autoRefreshIsActive 		READ autoRefreshIsActive		WRITE setAutoRefreshIsActive  		NOTIFY autoRefreshIsActiveChanged)
//    Q_PROPERTY( bool 		useOnlyOverWifi 			READ useOnlyOverWifi			WRITE setUseOnlyOverWifi  			NOTIFY useOnlyOverWifiChanged)
//    Q_PROPERTY( bool 		authTokenIsRefreshing 		READ authTokenIsRefreshing		WRITE setAuthTokenIsRefreshing  	NOTIFY authTokenIsRefreshingChanged)
//    Q_PROPERTY( QDateTime 	dateTimeAquiredTheToken 	READ dateTimeAquiredTheToken  	WRITE setDateTimeAquiredTheToken	NOTIFY dateTimeAquiredTheTokenChanged)
//    Q_PROPERTY( QDateTime 	nextDateTimeTokenUpdate 	READ nextDateTimeTokenUpdate  	WRITE setNextDateTimeTokenUpdate	NOTIFY nextDateTimeTokenUpdateChanged)
//    Q_PROPERTY( QDateTime 	nextRelogin 				READ nextRelogin  				WRITE setNextRelogin				NOTIFY nextReloginChanged)
//    Q_PROPERTY( QTime 		nextAuthStatementInterval 	READ nextAuthStatementInterval  WRITE setNextAuthStatementInterval	NOTIFY nextAuthStatementIntervalChanged)
//    Q_PROPERTY( QTime 		nextReloginInterval 		READ nextReloginInterval  		WRITE setNextReloginInterval		NOTIFY nextReloginIntervalChanged)

    Q_PROPERTY( bool 		isLoggedIn 					READ isLoggedIn  				WRITE setIsLoggedIn					NOTIFY isLoggedInChanged)

public:

    enum LevelOfLogging
    {
        All,
        ErrorOnly,
        NoLogging,
    };

private:

    LocalUserUtility(QObject *parent = 0);

    virtual ~LocalUserUtility();

public:

    static LocalUserUtility* instance();

    static void deleteInstance();

    LevelOfLogging levelOfLogging() { return m_currentLoggingLevel; }
    void setLevelOfLogging(LevelOfLogging lol) { m_currentLoggingLevel = lol; }

    QString domain()  const { return m_domain; }
    void setDomain(const QString &gu);

//    QString facadeVersion() const { return m_FacadeVersion; }
//    void setFacadeVersion(const QString &s) { m_FacadeVersion = s; m_pSettings->setValue(QLatin1String("facadeVersion"),m_FacadeVersion); }

//    QDateTime dateTimeAquiredTheToken() const { return m_dateTimeAquiredTheToken; }
//    void setDateTimeAquiredTheToken(const QDateTime &t) { m_dateTimeAquiredTheToken = t; }

//    QDateTime nextDateTimeTokenUpdate() const { return m_nextDateTimeTokenUpdate; }
//    void setNextDateTimeTokenUpdate(const QDateTime &t) { m_nextDateTimeTokenUpdate = t; }

//    QDateTime nextRelogin() const { return m_nextRelogin; }
//    void setNextRelogin(const QDateTime &t) { m_nextRelogin = t; }

//    bool cancel_All_The_Requests() const { return m_Cancel_All_The_Requests; }
//    void setCancel_All_The_Requests(bool c) { m_Cancel_All_The_Requests = c; }

//    QTime nextAuthStatementInterval() const { return m_nextAuthStatementInterval; }
//    void setNextAuthStatementInterval(const QTime &t) { m_nextAuthStatementInterval = t; }

//    QTime nextReloginInterval() const { return m_nextReloginInterval; }
//    void setNextReloginInterval(const QTime &t) { m_nextReloginInterval = t; }

//    bool authTokenIsRefreshing() const { return m_AuthTokenIsRefreshing; }
//    void setAuthTokenIsRefreshing(bool a) { m_AuthTokenIsRefreshing = a; }

//    bool useOnlyOverWifi() const { return m_UseOnlyOverWifi; }
//    void setUseOnlyOverWifi(bool u) { m_UseOnlyOverWifi = u; }

//    bool autoRefreshIsActive() const { return m_AutoRefreshIsActive; }
//    void setAutoRefreshIsActive(bool a) { m_AutoRefreshIsActive = a; m_pSettings->setValue(QLatin1String("autoRefreshIsActive"),m_AutoRefreshIsActive); }

    QString organizationId() const { return m_OrganizationId; }
    void setOrganizationId(const QString &s);

    QString eMailAddress() const { return m_EMailAddress; }
    void setEMailAddress(const QString &e);

    QString authToken() const { return m_AuthToken; }
    void setAuthToken(const QString &a);

    QString username() const { return m_username; }
    void setUsername(const QString &u);

    QString clientId() const { return m_clientId; }
    void setClientId(const QString &u);

    QString clientSecret() const { return m_clientSecret; }
    void setClientSecret(const QString &u);

    QString code() const { return m_code; }
    void setCode(const QString &code) { m_code = code; }

    QString password() const { return m_password; }
    void setPassword(const QString &p);

    bool isLoggedIn() const { return m_isLoggedIn; }
    void setIsLoggedIn(bool il) { m_isLoggedIn = il; emit isLoggedInChanged(); }

    void setupByLoginInfo(const QVariant& );


    QByteArray constructAuthorizationBearerToken() const;


    QByteArray constructAuthorizationBasicToken() const;


    QString generalURL(bool isHTTPS = false, bool containSubdomain = true) const;


    Q_INVOKABLE QString localizedDate(const QDateTime &dt, int format = Qt::SystemLocaleShortDate);


    Q_INVOKABLE QString localizedNumber(double num, int digits = 2);


    Q_INVOKABLE QString localizedDateFromString(const QString &datestring, int format = Qt::SystemLocaleShortDate);


    Q_INVOKABLE QDateTime dateFromString(const QString &datestring, int format = Qt::ISODate);

signals:

    void domainChanged();
//    void facadeVersionChanged();
    void organizationIdChanged();
    void eMailAddressChanged();
    void authTokenChanged();
    void usernameChanged();
    void passwordChanged();
//    void autoRefreshIsActiveChanged();
//    void useOnlyOverWifiChanged();
//    void authTokenIsRefreshingChanged();
//    void dateTimeAquiredTheTokenChanged();
//    void nextDateTimeTokenUpdateChanged();
//    void nextReloginChanged();
//    void nextAuthStatementIntervalChanged();
//    void nextReloginIntervalChanged();
    void isLoggedInChanged();
    void clientIdChanged();
    void clientSecretChanged();
    void codeChanged();


public slots:

    /*!
     * \brief clearInstance
     */
    void clearInstance() { deleteInstance(); }

private:

    void initialize();

    LevelOfLogging m_currentLoggingLevel;

    static LocalUserUtility* m_pInstance;

    //this is the static string across the app for the template of our url. we update it when the user types on login screen.
    /// <summary>
    /// base URL of the facade webservice
    /// </summary>
    QString m_domain;

//    //we need this string for several displays and logs across the app. we get from the refreshGeneralInfos
//    QString m_FacadeVersion;

//    //the 3 timers we need for reauthenticate and relogin. also when we got the token.
//    /// <summary>
//    /// the authentication token's creation/receival time
//    /// </summary>
//    QDateTime m_dateTimeAquiredTheToken;
//    QDateTime m_nextDateTimeTokenUpdate;
//    QDateTime m_nextRelogin;


//    //a bool value representing whether we are refreshing the token.
//    bool m_AuthTokenIsRefreshing;

//    //the default values for the "next authentication and relogin"
//    QTime m_nextAuthStatementInterval;
//    QTime m_nextReloginInterval;

//    //these are 2 bools which only disabling the popups if one is allready displayed to disable popup spam to the user
//    bool m_Cancel_All_The_Requests;
//    bool m_NoConnectionToServerPopupFlagShown;

//    //this is the bool settings value for use only over Wifi, if the user can refresh with 3g or not.
//    bool m_UseOnlyOverWifi;
//    //whether the auto refresh is on.
//    bool m_AutoRefreshIsActive;

    /// <summary>
    /// the userId for symphonia services
    /// </summary>
    QString m_OrganizationId;
    QString m_EMailAddress;

    //this is a static value for the clientInfo since we need more than in one page information from it.
    //   internal static MobileClientInfoCollectionFix ClientInfo = null;
    //   public static string DefaultLocale = null;

    /// <summary>
    /// the current OpenScape webservice authentication token
    /// </summary>
    QString m_AuthToken;

    /// <summary>
    /// the authentication token's next renew time
    /// </summary>

    //username used from the user during login screen.
    QString m_username;

    /// <summary>
    /// password used from the user during login screen
    /// </summary>
    QString m_password;

    /*!
     * \brief m_clientId
     */
    QString m_clientId;

    /*!
     * \brief m_clientSecret
     */
    QString m_clientSecret;

    /*!
     * \brief m_code
     */
    QString m_code;

    /*!
     *
     */
//    QSettings *m_pSettings;


    bool m_isLoggedIn;

    QLocale m_locale;
};

} /*! namespace HelperClasses */

} /*! namespace OpenScapeUCBB */

#endif /* LOCALUSERUTILITY_H_ */
