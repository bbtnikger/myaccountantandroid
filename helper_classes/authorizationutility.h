#ifndef AUTHORIZATIONUTILITY_H
#define AUTHORIZATIONUTILITY_H

#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>

//! public access
#define AUTHORIZATIONUTILITY MyAccountantCloud::HelperClasses::AuthorizationUtility::instance()

#define STORMPATH_API_KEY_ID AUTHORIZATIONUTILITY->apiKeyId()
#define STORMPATH_API_KEY_SECRET AUTHORIZATIONUTILITY->apiKeySecret()

//#define STORMPATH_API_KEY_ID "59GTABQ56RSYIHYSEO2ARQ4D4"
//#define STORMPATH_API_KEY_SECRET "lcl0+pERBoOol3HlR2dAiUdK/pQ9SLyISfTRbANM1eA"

namespace MyAccountantCloud{

namespace HelperClasses{

/*!
 * \brief The AuthorizationUtility class
 */
class AuthorizationUtility : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString apiKeyId READ apiKeyId WRITE setApiKeyId NOTIFY apiKeyIdChanged)
    Q_PROPERTY(QString apiKeySecret READ apiKeySecret WRITE setApiKeySecret NOTIFY apiKeySecretChanged)

private:
    /*!
     * \brief AuthorizationUtility
     * \param parent
     */
    explicit AuthorizationUtility(QObject *parent = 0);

    /*!
     * \brief ~AuthorizationUtility
     */
    ~AuthorizationUtility();


signals:

    void apiKeyIdChanged();
    void apiKeySecretChanged();


public slots:

    void clearInstance() { deleteInstance(); }

public:

    /*!
     * \brief instance
     * \return
     */
    static AuthorizationUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    QString apiKeyId() const { return m_apiKeyId; }
    void setApiKeyId(const QString &s) { m_apiKeyId = s; }

    QString apiKeySecret() const { return m_apiKeySecret; }
    void setApiKeySecret(const QString &s) { m_apiKeySecret = s; }


private:

    /*!
     * \brief m_pInstance
     */
    static AuthorizationUtility *m_pInstance;


    QString m_apiKeyId;
    QString m_apiKeySecret;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // AUTHORIZATIONUTILITY_H
