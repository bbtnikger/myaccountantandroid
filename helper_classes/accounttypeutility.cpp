#include "accounttypeutility.h"
#include "networkutility.h"
#include "localuserutility.h"
#include "LoggerService.h"

#include <QNetworkRequest>
#include <QEventLoop>
#include <QUrlQuery>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

using namespace MyAccountantCloud::HelperClasses;


AccountTypeUtility* AccountTypeUtility::m_pInstance = NULL;

AccountTypeUtility* AccountTypeUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new AccountTypeUtility();
    }

    return m_pInstance;
}

void AccountTypeUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

AccountTypeUtility::AccountTypeUtility(QObject *parent) : QObject(parent){

}


QVariant AccountTypeUtility::addAccountType(const QVariantMap &accounttype){

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounttypes"));
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(accounttype);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("accounttype"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "accounttype(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->post(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onAddAccountTypeReplyFinished(reply);
}

QVariantList AccountTypeUtility::getAccountTypes(const QString &profile_id){

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2?").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounttypes"));
    QUrlQuery urlquery(url);
    urlquery.addQueryItem(QLatin1String("profile_id"),profile_id);
    request.setUrl(QUrl(urlquery.toString()));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->get(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onGetAccountTypesReplyFinished(reply);
}


QVariant AccountTypeUtility::onAddAccountTypeReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
    qDebug() << reply->request().rawHeader("Authorization");
    qDebug() << "reply url:" << reply->url();
    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        map = _map.value(QLatin1String("accounttype"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}

QVariantList AccountTypeUtility::onGetAccountTypesReplyFinished(QNetworkReply *reply){

    QVariantList list;

//    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        LOGGERSERVICE->logMessage(QString::fromLatin1("GET api accounts failed,error(%1)").arg(QLatin1String("Reply is null")));
        return list;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

//    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        QJsonObject jsonobject = json.object();

        QJsonObject::const_iterator i;

        for(i = jsonobject.find(QLatin1String("accounttypes")); i != jsonobject.end(); i++){

            QJsonValue val = *i;
            QJsonArray arr = val.toArray();
            foreach (QJsonValue catval, arr) {

                list.append(catval.toVariant());
            }

            break;
        }

        return list;

    }else{

        qDebug() << parserError.errorString();
        LOGGERSERVICE->logMessage(QString::fromLatin1("GET api account types failed,error(%1)").arg(parserError.errorString()));
    }

    return list;
}

bool AccountTypeUtility::onDeleteAccountTypeReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return false;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << "content:" << content_;
//    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


//    QString access_token;
//    QJsonParseError parserError;
//    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
//    if(parserError.error == QJsonParseError::NoError){

//        qDebug() << json.toJson();

//        return true;

//    }else{

//        qDebug() << parserError.errorString();
//    }

    return true;
}

bool AccountTypeUtility::deleteAccountType(const QString &_id){

    qDebug() << Q_FUNC_INFO << _id;

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounttypes/%1")).arg(_id);
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QNetworkReply *reply = NETWORKMANAGER->deleteResource(request);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onDeleteAccountTypeReplyFinished(reply);
}


QVariant AccountTypeUtility::updateAccountType(const QString &_id, const QVariantMap &accounttype){

    QNetworkRequest request;

    QString url = QString::fromLatin1("%1/%2/%3").arg(LOCALUSERUTILITY->generalURL()).arg(QLatin1String("api/accounttypes")).arg(_id);
    request.setUrl(QUrl(url));
    QByteArray ba = LOCALUSERUTILITY->constructAuthorizationBearerToken();
    request.setRawHeader("Authorization",ba);
    request.setRawHeader("Content-Type", "application/json");
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    QEventLoop loop;
    QByteArray bad;
    QJsonDocument json = QJsonDocument::fromVariant(accounttype);
    QJsonObject jsonObject;
    jsonObject.insert(QLatin1String("accounttype"),json.object());
    QJsonDocument json_(jsonObject);
    bad.append(json_.toJson());
    qDebug() << "accounttype(" << bad << ")";
    QNetworkReply *reply = NETWORKMANAGER->put(request,bad);
    reply->waitForReadyRead(10000);
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),NETWORKUTILITY,SLOT(onNetworkError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),&loop,SLOT(quit()));
    loop.exec();

    return onUpdateAccountTypeReplyFinished(reply);
}

QVariant AccountTypeUtility::onUpdateAccountTypeReplyFinished(QNetworkReply *reply){

    qDebug() << __FUNCTION__;

    QVariant map;

    if(!reply){

        qDebug() << QLatin1String("reply is null");
        return map;
    }

    QString content_ = QString::fromUtf8(reply->readAll());
    if(content_.startsWith(QLatin1String("\"")))
        content_.remove(0,1);
    if(content_.endsWith(QLatin1String("\"")))
        content_.chop(1);

    qDebug() << content_;
//    qDebug() << reply->request().rawHeader("Authorization");
    qDebug() << "reply url:" << reply->url();
//    qDebug() << "network request url:" << reply->request().url();


    QJsonParseError parserError;
    QJsonDocument json = QJsonDocument::fromJson(content_.toUtf8(),&parserError);
    if(parserError.error == QJsonParseError::NoError){

        qDebug() << "QJsonDocument created:" << json.toJson();
        QVariantMap _map = json.toVariant().toMap();
        qDebug() << _map;

        return _map.value(QLatin1String("accounttype"));

    }else{

        qDebug() << parserError.errorString();
    }

    return map;
}
