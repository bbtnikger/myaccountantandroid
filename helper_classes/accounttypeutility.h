#ifndef ACCOUNTTYPEUTILITY_H
#define ACCOUNTTYPEUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define ACCOUNTTYPEUTILITY MyAccountantCloud::HelperClasses::AccountTypeUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class AccountTypeUtility : public QObject
{
    Q_OBJECT
private:
    explicit AccountTypeUtility(QObject *parent = 0);

    ~AccountTypeUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static AccountTypeUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addAccountType
     * \param accounttype
     */
    Q_INVOKABLE QVariant addAccountType(const QVariantMap &accounttype);


    /*!
     * \brief getAccountTypes
     * \return
     */
    Q_INVOKABLE QVariantList getAccountTypes(const QString &profile_id);


    /*!
     * \brief deleteAccountType
     * \param _id
     * \return
     */
    Q_INVOKABLE bool deleteAccountType(const QString &_id);

    /*!
     * \brief updateAccountType
     * \param accounttype
     * \return
     */
    Q_INVOKABLE QVariant updateAccountType(const QString &_id, const QVariantMap &accounttype);

signals:

public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddAccountTypeReplyFinished
     * \param reply
     */
    QVariant onAddAccountTypeReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetAccountTypesReplyFinished
     * \param reply
     */
    QVariantList onGetAccountTypesReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteAccountTypeReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteAccountTypeReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateAccountTypeReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateAccountTypeReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static AccountTypeUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // ACCOUNTTYPEUTILITY_H
