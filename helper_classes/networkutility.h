#ifndef NETWORKUTILITY_H
#define NETWORKUTILITY_H

#include <QObject>

#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QtNetwork/QSslError>
#include <QtNetwork/QSslKey>
#include <QtNetwork/QSslConfiguration>
#include <QNetworkConfigurationManager>

//! public access
#define NETWORKUTILITY MyAccountantCloud::HelperClasses::NetworkUtility::instance()
#define NETWORKMANAGER MyAccountantCloud::HelperClasses::NetworkUtility::instance()->networkManager()

namespace MyAccountantCloud{

namespace HelperClasses{

class NetworkUtility : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString 						networkErrorString			READ networkErrorString		WRITE setNetworkErrorString		NOTIFY networkErrorStringChanged)
    Q_PROPERTY(QNetworkReply::NetworkError 	networkError				READ networkError			WRITE setNetworkError			NOTIFY networkErrorChanged)
    Q_PROPERTY(QList<QSslError>				sslErrorList				READ sslErrorList			WRITE setSslErrorList			NOTIFY sslErrorListChanged)
    Q_PROPERTY(QStringList					sslErrorStringList			READ sslErrorStringList		WRITE setSslErrorStringList		NOTIFY sslErrorStringListChanged)
private:

    /*!
     * \brief NetworkUtility
     * \param parent
     */
    explicit NetworkUtility(QObject *parent = 0);

    /*!
     * \brief NetworkUtility
     */
    ~NetworkUtility();

public:

    /*!
     * \brief instance
     * \return
     */
    static NetworkUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief networkErrorString
     * \return
     */
    QString networkErrorString() const { return m_networkErrorString; }

    /*!
     * \brief setNetworkErrorString
     * \param errorString
     */
    void setNetworkErrorString(const QString &errorString) { m_networkErrorString = errorString; }

    /*!
     * \brief networkError
     * \return
     */
    QNetworkReply::NetworkError networkError() const { return m_networkError; }

    /*!
     * \brief setNetworkError
     * \param error
     */
    void setNetworkError(QNetworkReply::NetworkError error) { m_networkError = error; emit networkErrorChanged(); }

    /*!
     * \brief networkManager
     * \return
     */
    QNetworkAccessManager* networkManager() { return m_pNetworkManager; }

    QList<QSslError> sslErrorList() const { return m_sslErrorList; }
    void setSslErrorList(const QList<QSslError> list) { m_sslErrorList = list; emit sslErrorListChanged(); }

    QStringList sslErrorStringList() const { return m_sslErrorStringList; }
    void setSslErrorStringList(const QStringList &error) { m_sslErrorStringList = error; }


    QSslConfiguration getSslConfiguration(QSslConfiguration &sslConfiguration, const QString &certPrivateKeyPath, const QString &passPhrase, bool *hasSSKKeyError = 0);

public slots:

    /*!
     * \brief clearInstance
     */
    void clearInstance() { deleteInstance(); }

    /*!
     * \brief onNetworkError
     * \param error
     */
    void onNetworkError(QNetworkReply::NetworkError error);


    void onSslErrors(const QList<QSslError> &list);



signals:

    /*!
     * \brief networkErrorStringChanged
     */
    void networkErrorStringChanged();

    /*!
     * \brief networkErrorChanged
     */
    void networkErrorChanged();


    void sslErrorListChanged();


    void sslErrorStringListChanged();

public slots:

    /*!
     * \brief onAuthenticationRequestSlot
     * \param aReply
     * \param aAuthenticator
     */
    void onAuthenticationRequestSlot(QNetworkReply *aReply, QAuthenticator *aAuthenticator);

    /*!
     * \brief onProxyAuthenticationRequired
     * \param proxy
     * \param authenticator
     */
    void onProxyAuthenticationRequired(const QNetworkProxy & proxy, QAuthenticator *authenticator);

private:

    void loadSSLCertificate(const QString &certPrivateKeyPath, const QString &passPhrase);

    QString m_networkErrorString;

    QNetworkReply::NetworkError m_networkError;

    QNetworkAccessManager* m_pNetworkManager;

    static NetworkUtility* m_pInstance;

    QStringList m_sslErrorStringList;

    QList<QSslError> m_sslErrorList;

    QNetworkConfigurationManager m_networkConfigurationManager;

    QSslConfiguration m_sslConfiguration;
};

} // namespace HelperClasses

} // namespace MyAccountantCloud

#endif // NETWORKUTILITY_H
