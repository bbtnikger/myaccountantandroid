#ifndef PAYEEUTILITY_H
#define PAYEEUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define PAYEEUTILITY MyAccountantCloud::HelperClasses::PayeeUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class PayeeUtility : public QObject
{
    Q_OBJECT
private:
    explicit PayeeUtility(QObject *parent = 0);

    ~PayeeUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static PayeeUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addPayee
     * \param payee
     */
    Q_INVOKABLE QVariant addPayee(const QVariantMap &payee);


    /*!
     * \brief getPayees
     * \return
     */
    Q_INVOKABLE QVariantList getPayees(const QString &profile_id);


    /*!
     * \brief deletePayee
     * \param payee
     * \return
     */
    Q_INVOKABLE bool deletePayee(const QString &_id);

    /*!
     * \brief updatePayee
     * \param payee
     * \return
     */
    Q_INVOKABLE QVariant updatePayee(const QString &_id, const QVariantMap &payee);

signals:


public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddPayeeReplyFinished
     * \param reply
     */
    QVariant onAddPayeeReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetPayeesReplyFinished
     * \param reply
     */
    QVariantList onGetPayeesReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeletePayeeReplyFinished
     * \param reply
     * \return
     */
    bool onDeletePayeeReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdatePayeeReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdatePayeeReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief m_pInstance
     */
    static PayeeUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // PAYEEUTILITY_H
