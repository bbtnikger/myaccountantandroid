#ifndef EXPENSEUTILITY_H
#define EXPENSEUTILITY_H

#include <QObject>
#include <QNetworkReply>

//! public access
#define EXPENSEUTILITY MyAccountantCloud::HelperClasses::ExpenseUtility::instance()

namespace MyAccountantCloud{

namespace HelperClasses{

class ExpenseUtility : public QObject
{
    Q_OBJECT
private:

    /*!
     * \brief ExpenseUtility
     * \param parent
     */
    explicit ExpenseUtility(QObject *parent = 0);


    ~ExpenseUtility(){}

public:

    /*!
     * \brief instance
     * \return
     */
    static ExpenseUtility* instance();

    /*!
     * \brief deleteInstance
     */
    static void deleteInstance();

    /*!
     * \brief addExpense
     * \param expense
     */
    Q_INVOKABLE QVariant addExpense(const QVariantMap &expense);


    /*!
     * \brief getExpenses
     * \return
     */
    Q_INVOKABLE QVariantList getExpenses(const QString &profile_id);

    /*!
     * \brief getExpenses
     * \param profile_id
     * \param queryParameters
     * \return
     */
    Q_INVOKABLE QVariantList getExpenses(const QString &profile_id, const QVariantMap &queryParameters);


    /*!
     * \brief deleteExpense
     * \param expense
     * \return
     */
    Q_INVOKABLE bool deleteExpense(const QString &_id);

    /*!
     * \brief updateExpense
     * \param expense
     * \return
     */
    Q_INVOKABLE QVariant updateExpense(const QString &_id, const QVariantMap &expense);

    /*!
     * \brief getExpenseProperties
     * \param profile_id
     * \return
     */
    Q_INVOKABLE QVariantMap getExpenseProperties(const QString &profile_id);

signals:


public slots:

    void clearInstance() { deleteInstance(); }

private slots:

    /*!
     * \brief onAddExpenseReplyFinished
     * \param reply
     */
    QVariant onAddExpenseReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onGetExpensesReplyFinished
     * \param reply
     */
    QVariantList onGetExpensesReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onDeleteExpenseReplyFinished
     * \param reply
     * \return
     */
    bool onDeleteExpenseReplyFinished(QNetworkReply *reply);

    /*!
     * \brief onUpdateExpenseReplyFinished
     * \param reply
     * \return
     */
    QVariant onUpdateExpenseReplyFinished(QNetworkReply *reply);

private:

    /*!
     * \brief onGetPayeesReplyFinished
     * \param reply
     */
    QVariantMap onGetExpensePropertiesReplyFinished(QNetworkReply *reply);

    /*!
     * \brief m_pInstance
     */
    static ExpenseUtility *m_pInstance;
};

} // namespace HelperClasses

} // MyAccountantCloud

#endif // EXPENSEUTILITY_H
