#include "authorizationutility.h"
#include "networkutility.h"
#include "localuserutility.h"

#include <QNetworkRequest>
#include <QEventLoop>
#include <QUrlQuery>

#include <QJsonObject>
#include <QJsonDocument>

using namespace MyAccountantCloud::HelperClasses;

AuthorizationUtility* AuthorizationUtility::m_pInstance = NULL;

AuthorizationUtility* AuthorizationUtility::instance(){

    if(m_pInstance == NULL){

        m_pInstance = new AuthorizationUtility();
    }

    return m_pInstance;
}

void AuthorizationUtility::deleteInstance(){

    if(m_pInstance != NULL){

        delete m_pInstance;
        m_pInstance = NULL;
    }
}

AuthorizationUtility::AuthorizationUtility(QObject *parent) : QObject(parent){


}

AuthorizationUtility::~AuthorizationUtility(){


}
