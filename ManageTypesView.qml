import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "ManageTypesView.js" as ManageTypesViewScript

Item {
    id: root

    function resetPage(){


    }

    ManageTypesViewList{
        anchors.fill: parent

        onClicked: {

            busyIndicator.running = true;

            if(currenttypeindex === 0){ // account types

              stackView.push({item:accountTypeComponent});

            } else if(currenttypeindex === 1) { // payees

                stackView.push({item:payeeComponent});

            }else if(currenttypeindex === 2){ // payment methods

                stackView.push({item:paymentMethodComponent});

            }else if(currenttypeindex === 3){ // tags

                stackView.push({item:tagComponent});
            }

            busyIndicator.running = false;
        }

        onCurrenttypeindexChanged: {

//            busyIndicator.running = true;

//            if(currenttypeindex === 0){ // account types

//              stackView.push({item:accountTypeComponent});

//            } else if(currenttypeindex === 1) { // payees

//                stackView.push({item:payeeComponent});

//            }else if(currenttypeindex === 2){ // payment methods

//                stackView.push({item:paymentMethodComponent});

//            }else if(currenttypeindex === 3){ // tags

//                stackView.push({item:tagComponent});
//            }

//            busyIndicator.running = false;
        }
    }

    Component{
        id: payeeComponent
        PayeesView{ }
    }

    Component{
        id: paymentMethodComponent
        PaymentMethodsView{ }
    }

    Component{
        id: accountTypeComponent
        AccountTypesView{ }
    }

    Component{
        id: tagComponent
        TagsView{ }
    }

//    RowLayout{
//        anchors.fill: parent
//        spacing: 1

//        ManageTypesViewList{
//            Layout.minimumWidth: 200
//            Layout.maximumWidth: 300
//            Layout.preferredWidth: 200
//            Layout.fillHeight: true

//            onCurrenttypeindexChanged: {

//                busyIndicator.running = true;

//                loader.sourceComponent = undefined;

//                if(currenttypeindex === 0){ // account types

//                  loader.source = "AccountTypesView.qml";

//                } else if(currenttypeindex === 1) { // payees

//                    loader.source = "PayeesView.qml";

//                }else if(currenttypeindex === 2){ // payment methods

//                    loader.source = "PaymentMethodsView.qml";

//                }else if(currenttypeindex === 3){ // tags

//                    loader.source = "TagsView.qml";
//                }

//                busyIndicator.running = false;
//            }
//        }

//        Loader{
//            id: loader
//            anchors.top: parent.top
//            anchors.bottom: parent.bottom
//            anchors.topMargin: 1
//            anchors.bottomMargin: 1
//            Layout.fillWidth: true
//        }
//    }
}

