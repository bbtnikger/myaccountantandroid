function func() {

}

function resetTypes(){

    clearTypes();
    setupTypes();
}

function clearTypes(){

    listModel.clear();
}

function setupTypes(){

    var list = [];
    list.push({name:qsTr("Account Types")});
    list.push({name:qsTr("Payees")});
    list.push({name:qsTr("Payment Methods")});
    list.push({name:qsTr("Tags")});

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        listModel.append(record_);
    }
}

function onCurrentIndexChanged(currentIndex){

    console.log('[ManageTypes ListView]-Current Index('+currentIndex+')');

    currenttypelistitem = listModel.get(currentIndex);
    currenttypeindex = currenttypelistitem._id;
}
