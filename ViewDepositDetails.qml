import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import "dialogs_forms"

Item {
    id: root

    property variant depositdata

    function setup(){

        var datestring = LocalUserUtility.localizedDateFromString(depositdata.date);
        var amount = LocalUserUtility.localizedNumber(depositdata.amount);
//        var description = depositdata.memo;
//        var reminder = deposititem.reminder;
//        var reminderdetails = CalendarManager.eventDetails(deposititem.reminder);
        var cleareddescription = ReconcileUtility.clearedName(depositdata.cleared);
        var account = depositdata.account.name;

//        console.log("seting up the deposit .. reminder : " + reminder +  " , reminderdetails : " + reminderdetails);

        dateLabel.text = datestring;
        amountLabel.text = amount + " " + depositdata.account.currency;
//        descriptionLabel.text = description;
        depositStatusLabel.text = cleareddescription;
        accountLabel.text = account;
    }

    function openEditDialog(){

        busyIndicator.running = true;

        depositDialog.depositdata = depositdata;
        depositDialog.resetDefaults();
        depositDialog.contentheight = root.height-100;
        depositDialog.contentwidth = root.width-100;
        depositDialog.open();

        busyIndicator.running = false;
    }

    function onUpdateDepositDialogAccepted(){

        busyIndicator.running = true;

        var depositdata_ = depositDialog.getDepositDetails();
        var _id = depositdata._id;
        var profile_id = ProfileUtility.currentProfile;
        depositdata_.profile_id = profile_id;
        var depositdatanew = DepositUtility.updateDeposit(_id,depositdata_);

        if(depositdatanew !== null && depositdatanew !== undefined){

            showInformationMessage(qsTr("Deposit updated successfully"));
            console.log(qsTr("Deposit updated successfully"));

            depositdata = depositdatanew;

            setup();

            depositDialog.close();
        }

        busyIndicator.running = false;
    }

    DepositDialog{
        id: depositDialog
        title: qsTr("Deposit Dialog");

        onAccepted: onUpdateDepositDialogAccepted()
    }

    ColumnLayout{
        anchors.top: parent.top
        anchors.bottom: toolbar.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 9
        spacing: 9

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Date")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: dateLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Account")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: accountLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Amount")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: amountLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Deposit status")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: depositStatusLabel
            }
        }

        ColumnLayout{
            spacing: 3

            Label {
                text: qsTr("Description")
                font.italic: true
                font.pointSize: 11
            }

            Label {
                id: descriptionLabel
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 10
        }
    }

    ToolBar {
        id: toolbar
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        RowLayout {
            spacing: 6
            anchors.fill: parent

            ToolButton{
                tooltip: qsTr("Back")
                iconSource: "qrc:/images/ic_previous_64x64.png"
                onClicked: stackView.pop()
            }

            Item{
                Layout.fillWidth: true
                Layout.preferredHeight: 10
            }

            ToolButton{
                tooltip: qsTr("Edit")
                iconSource: "qrc:/images/ic_edit_64x64.png"
                onClicked: openEditDialog()
            }

            Item{
                Layout.fillWidth: true
                Layout.preferredHeight: 10
            }
        }
    }

    Component.onCompleted: setup()
}

