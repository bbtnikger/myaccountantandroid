function func() {

}

function resetHistory(){

    columnLayout.visible = false;

    clearHistory();
//    accountsViewDashboardBalance.resetDefaults();

    //
    fetchHistory();

    if(history === undefined)
        return;

    setupTransactions();
//    resetBalance();

    columnLayout.visible = true;
}

function clearHistory(){

    historyModel.clear();
}

function addExpenseIncomeRecordToModel(record_,ttypestring,ttype){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    var acnane = record_.account.name;
    record_.accountname = acnane;
    record_.type = ttypestring;
    var catname = record_.category.name;
    record_.description = catname;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.ttype = ttype;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    historyModel.append(record_);
}

function updateExpenseIncomeRecordToModel(record_,ttypestring,ttype){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    var acnane = record_.account.name;
    record_.accountname = acnane;
    record_.type = ttypestring;
    var catname = record_.category.name;
    record_.description = catname;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.ttype = ttype;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    var idx = findHistoryIndex(record_._id);
    if(idx !== -1)
        historyModel.set(idx,record_);
}

function addTransferRecordToModel(record_){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    var acname = record_.origin.name;
    record_.accountname = acname;
    record_.type = typeTransfers;
    record_.ttype = 4;
    var destname = record_.destination.name;
    record_.description = destname;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    historyModel.append(record_);
}

function updateTransferRecordToModel(record_){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    var acname = record_.origin.name;
    record_.accountname = acname;
    record_.type = typeTransfers;
    record_.ttype = 4;
    var destname = record_.destination.name;
    record_.description = destname;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    var idx = findHistoryIndex(record_._id);
    if(idx !== -1)
        historyModel.set(idx,record_);
}

function addDepositRecordToModel(record_){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    record_.accountname = record_.account.name;
    record_.type = typeDeposit;
    record_.ttype = 3;
    record_.description = record_.memo;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    historyModel.append(record_);
}

function updateDepositRecordToModel(record_){

    var dt = new Date(record_.date);
    record_.datename = LocalUserUtility.localizedDate(dt);
    record_.accountname = record_.account.name;
    record_.type = typeDeposit;
    record_.description = record_.memo;
    record_.ttype = 3;
    record_.amounttext = LocalUserUtility.localizedNumber(record_.amount) + ' ' + accountcurrency;
    record_.paymentstatus = ReconcileUtility.clearedName(record_.cleared);

    var idx = findHistoryIndex(record_._id);
    if(idx !== -1)
        historyModel.set(idx,record_);
}

function processExpenses(expenses){

    var len = expenses.length;
    for(var i = 0; i < len; i++){

        var record_ = expenses[i];

        addExpenseIncomeRecordToModel(record_,typeExpenses,1);
    }
}

function processIncomes(incomes){

    var len = incomes.length;
    for(var i = 0; i < len; i++){

        var record_ = incomes[i];

        addExpenseIncomeRecordToModel(record_,typeIncome,2);
    }
}

function processTransfers(transfers){

    var len = transfers.length;
    for(var i = 0; i < len; i++){

        var record_ = transfers[i];

        addTransferRecordToModel(record_);
    }
}

function processDeposits(deposits){

    var len = deposits.length;
    for(var i = 0; i < len; i++){

        var record_ = deposits[i];

        addDepositRecordToModel(record_);
    }
}

var ExpenseHistoryQuery = function(){

    this.account = root.account_id;
}

var IncomeHistoryQuery = function(){

    this.account = root.account_id;
}

var DepositHistoryQuery = function(){

    this.account = root.account_id;
}

var TransferOriginHistoryQuery = function(){

    this.origin = root.account_id;
}

var TransferDestinationHistoryQuery = function(){

    this.destination = root.account_id;
}

function setupTransactions(){

    var transactions = history.transactions;

    accountsViewDashboardLastTransactions.listView.model = null;

    var expenses = transactions.expenses;
    if(expenses !== undefined)
        processExpenses(expenses);

    var incomes = transactions.incomes;
    if(incomes !== undefined)
        processIncomes(incomes);

    var deposits = transactions.deposits;
    if(deposits !== undefined)
        processDeposits(deposits);

    var transfers = transactions.transfers;

    if(transfers !== undefined){

        var origintransfers = transfers.origin;
        var destinationtransfers = transfers.destination;

        processTransfers(origintransfers);
        processTransfers(destinationtransfers);
    }

    accountsViewDashboardLastTransactions.listView.model = accountsViewDashboardLastTransactions.proxyModel;
}

function fetchHistory(){

    history = HistoryUtility.getAccountHistory(ProfileUtility.currentProfile,account_id);
}

function openTransactionDetails(){


}

function startAddTransaction(){

    selectTransationTypeDialog.contentwidth = root.width - 100;
    selectTransationTypeDialog.contentheight = root.height - 100;
    selectTransationTypeDialog.resetDefaults();
    selectTransationTypeDialog.open();
}

function onTransactionTypeSelected(){

    busyIndicator.running = true;

    var selection = selectTransationTypeDialog.getSelectionOfTransactionTypeDetails();

    var transactiontype = selection.transactiontype;
    if(transactiontype === 1){ // expense or income

        startAddExpenseIncomeDialog();

    }else if( transactiontype === 2){ // deposit

        startAddDepositDialog();

    }else{ // transfer

        startAddTransferDialog();
    }

    busyIndicator.running = false;
}

function startEditTransaction(){

    var ttype = parseInt(accountsViewDashboardLastTransactions.currenthistorylistitem.ttype);
    console.log('Start edit transaction ttype('+ttype+')');

    if(ttype === 1 || ttype === 2){ // expense or income

        startUpdateExpenseIncomeDialog();

    }else if(ttype === 3){ // deposits

        startUpdateDepositDialog();

    }else if(ttype === 4){ // transfer

        startUpdateTransferDialog();
    }
}

function onAddExpenseIncomeDialogAccepted(){

    busyIndicator.running = true;

    var expenseincomedata = expenseIncomeDialog.getExpenseIncomeDetails();
    var profile_id = ProfileUtility.currentProfile;
    expenseincomedata.profile_id = profile_id;
    var eitype = expenseIncomeDialog.eitype;
    var expenseincomedatanew = null;
    var typeString_ = typeExpenses;
    if(eitype === 1){ // expense

        expenseincomedatanew = ExpenseUtility.addExpense(expenseincomedata);

    }else{ // income

        expenseincomedatanew = IncomeUtility.addIncome(expenseincomedata);
        typeString_ = typeIncome;
    }

    if(expenseincomedatanew !== null){

        showInformationMessage(qsTr("Transaction added successfully"));

        if(expenseincomedatanew.account === account_id){

            addExpenseIncomeRecordToModel(expenseincomedatanew,typeString_,eitype);
        }
    }

    busyIndicator.running = false;
}

function onUpdateExpenseIncomeDialogAccepted(){

    busyIndicator.running = true;

    var expenseincomedata = expenseIncomeDialog.getExpenseIncomeDetails();
    var _id = accountsViewDashboardLastTransactions.currenthistorylistitem._id;
    var profile_id = ProfileUtility.currentProfile;
    expenseincomedata.profile_id = profile_id;
    var eitype = expenseIncomeDialog.eitype;
    var expenseincomedatanew = null;
    var typeString_ = typeExpenses;
    var previous_ttype = parseInt(accountsViewDashboardLastTransactions.currenthistorylistitem.ttype);
    console.log('previous_ttype('+previous_ttype+') eitype('+eitype+')');

    var newrecord = false;

    if(eitype === 1){ // expense

        if(eitype === previous_ttype)
            expenseincomedatanew = ExpenseUtility.updateExpense(_id,expenseincomedata);
        else{

            // delete the income record
            var res = IncomeUtility.deleteIncome(_id);
            if(res){

                // remove the record from the model
                var idx = findHistoryIndex(_id);
                if(idx !== -1)
                    historyModel.remove(idx);

                // add new expense record
                expenseincomedatanew = ExpenseUtility.addExpense(expenseincomedata);

                newrecord = true;
            }
        }

    }else{ // income

        if(eitype === previous_ttype){

            expenseincomedatanew = IncomeUtility.updateIncome(_id,expenseincomedata);

        }else{

            var res2 = ExpenseUtility.deleteExpense(_id);
            if(res2){

                console.log('Expense deleted successfully');

                // remove the record from the model
                var idx2 = findHistoryIndex(_id);
                if(idx2 !== -1)
                    historyModel.remove(idx2);

                // add new expense record
                expenseincomedatanew = IncomeUtility.addIncome(expenseincomedata);

                newrecord = true;
            }
        }

        typeString_ = typeIncome;
    }

    if(expenseincomedatanew !== null){

        showInformationMessage(qsTr("Transaction updated successfully"));

        console.log('expenseincomedatanew.account._id('+expenseincomedatanew.account._id+') account_id('+account_id+') newrecord('+newrecord+')');

        if(expenseincomedatanew.account._id === account_id){

            if(!newrecord)
                updateExpenseIncomeRecordToModel(expenseincomedatanew,typeString_,eitype);
            else
                addExpenseIncomeRecordToModel(expenseincomedatanew,typeString_,eitype);
        }
        else{

            var idx3 = findHistoryIndex(record_._id);
            if(idx3 !== -1)
                historyModel.remove(idx3);
        }
    }

    busyIndicator.running = false;
}

function startAddExpenseIncomeDialog(){

    busyIndicator.running = true;

    expenseIncomeDialog.contentheight = stackView.height-100;
    expenseIncomeDialog.contentwidth = stackView.width-100;
    expenseIncomeDialog.expenseincomedata = null;
    expenseIncomeDialog.resetDefaults();
    expenseIncomeDialog.title = qsTr("Add Expense/Income Dialog");
    expenseIncomeDialog.accepted.disconnect(onAddExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.disconnect(onUpdateExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.connect(onAddExpenseIncomeDialogAccepted);
    expenseIncomeDialog.open();

    busyIndicator.running = false;
}

function startUpdateExpenseIncomeDialog(){

    busyIndicator.running = true;

    expenseIncomeDialog.contentheight = stackView.height-100;
    expenseIncomeDialog.contentwidth = stackView.width-100;
    expenseIncomeDialog.expenseincomedata = accountsViewDashboardLastTransactions.currenthistorylistitem;
    expenseIncomeDialog.resetDefaults();
    expenseIncomeDialog.title = qsTr("Update Expense/Income Dialog");
    expenseIncomeDialog.accepted.disconnect(onAddExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.disconnect(onUpdateExpenseIncomeDialogAccepted);
    expenseIncomeDialog.accepted.connect(onUpdateExpenseIncomeDialogAccepted);
    expenseIncomeDialog.open();

    busyIndicator.running = false;
}

function onAddTransferDialogAccepted(){

    busyIndicator.running = true;

    var transferdata = transferDialog.getTransferDetails();
    var profile_id = ProfileUtility.currentProfile;
    transferdata.profile_id = profile_id;
    var transferdatanew = TransferUtility.addTransfer(transferdata);

    if(transferdatanew !== null && transferdatanew !== undefined){

        showInformationMessage(qsTr("Transfer added successfully"));

        if(transferdatanew.origin === account_id || transferdatanew.destination === account_id)
            addTransferRecordToModel(transferdatanew);
    }

    busyIndicator.running = false;
}

function onUpdateTransferDialogAccepted(){

    busyIndicator.running = true;

    var transferdata = transferDialog.getTransferDetails();
    var _id = accountsViewDashboardLastTransactions.currenthistorylistitem._id;
    var profile_id = ProfileUtility.currentProfile;
    transferdata.profile_id = profile_id;
    var transferdatanew = TransferUtility.updateTransfer(_id,transferdata);

    if(transferdatanew !== null && transferdatanew !== undefined){

        showInformationMessage(qsTr("Transfer updated successfully"));

        if(transferdatanew.origin._id === account_id || transferdatanew.destination._id === account_id)
            updateTransferRecordToModel(transferdatanew);
        else{

            var idx = findHistoryIndex(record_._id);
            if(idx !== -1)
                historyModel.remove(idx);
        }
    }

    busyIndicator.running = false;
}

function startAddTransferDialog(){

    busyIndicator.running = true;

    transferDialog.contentheight = stackView.height-100;
    transferDialog.contentwidth = stackView.width-100;
    transferDialog.transferdata = null;
    transferDialog.resetDefaults();
    transferDialog.title = qsTr("Transfer Dialog");
    transferDialog.accepted.disconnect(onAddTransferDialogAccepted);
    transferDialog.accepted.disconnect(onUpdateTransferDialogAccepted);
    transferDialog.accepted.connect(onAddTransferDialogAccepted);
    transferDialog.open();

    busyIndicator.running = false;
}

function startUpdateTransferDialog(){

    busyIndicator.running = true;

    transferDialog.contentheight = stackView.height-100;
    transferDialog.contentwidth = stackView.width-100;
    transferDialog.transferdata = accountsViewDashboardLastTransactions.currenthistorylistitem;
    transferDialog.resetDefaults();
    transferDialog.title = qsTr("Transfer Dialog");
    transferDialog.accepted.disconnect(onAddTransferDialogAccepted);
    transferDialog.accepted.disconnect(onUpdateTransferDialogAccepted);
    transferDialog.accepted.connect(onUpdateTransferDialogAccepted);
    transferDialog.open();

    busyIndicator.running = false;
}


function onAddDepositDialogAccepted(){

    busyIndicator.running = true;

    var depositdata = depositDialog.getDepositDetails();
    var profile_id = ProfileUtility.currentProfile;
    depositdata.profile_id = profile_id;
    var depositdatanew = DepositUtility.addDeposit(depositdata);

    if(depositdatanew !== null && depositdatanew !== undefined){

        showInformationMessage(qsTr("Deposit added successfully"));

        if(depositdatanew.account._id === account_id)
            addDepositRecordToModel(depositdatanew);
    }

    busyIndicator.running = false;
}

function onUpdateDepositDialogAccepted(){

    busyIndicator.running = true;

    var depositdata = depositDialog.getDepositDetails();
    var _id = accountsViewDashboardLastTransactions.currenthistorylistitem._id;
    var profile_id = ProfileUtility.currentProfile;
    depositdata.profile_id = profile_id;
    var depositdatanew = DepositUtility.updateDeposit(_id,depositdata);

    if(depositdatanew !== null && depositdatanew !== undefined){

        showInformationMessage(qsTr("Deposit updated successfully"));

        if(depositdatanew.account._id === account_id)
            updateDepositRecordToModel(depositdatanew);
        else{

            var idx = findHistoryIndex(record_._id);
            if(idx !== -1)
                historyModel.remove(idx);
        }
    }

    busyIndicator.running = false;
}

function startAddDepositDialog(){

    busyIndicator.running = true;

    depositDialog.contentheight = stackView.height-100;
    depositDialog.contentwidth = stackView.width-100;
    depositDialog.depositdata = null;
    depositDialog.resetDefaults();
    depositDialog.title = qsTr("Transfer Dialog");
    depositDialog.accepted.disconnect(onAddDepositDialogAccepted);
    depositDialog.accepted.disconnect(onUpdateDepositDialogAccepted);
    depositDialog.accepted.connect(onAddDepositDialogAccepted);
    depositDialog.open();

    busyIndicator.running = false;
}

function startUpdateDepositDialog(){

    busyIndicator.running = true;

    depositDialog.depositdata = accountsViewDashboardLastTransactions.currenthistorylistitem;
    depositDialog.resetDefaults();
    depositDialog.title = qsTr("Deposit Dialog");
    depositDialog.contentheight = stackView.height-100;
    depositDialog.contentwidth = stackView.width-100;
    depositDialog.accepted.disconnect(onAddDepositDialogAccepted);
    depositDialog.accepted.disconnect(onUpdateDepositDialogAccepted);
    depositDialog.accepted.connect(onUpdateDepositDialogAccepted);
    depositDialog.open();

    busyIndicator.running = false;
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = accountsViewDashboardLastTransactions.currenthistorylistitem;

    var _id = recorddata._id;

    var ttype = 1;
    ttype = recorddata.ttype;

    var res = false;

    if(ttype == 1){ // expense

        res = ExpenseUtility.deleteExpense(_id);

    }else if(ttype == 2){ // income

        res = IncomeUtility.deleteIncome(_id);

    }else if(ttype == 3){ // deposit

        res = DepositUtility.deleteDeposit(_id);

    }else if(ttype == 4){ // transfer

        res = TransferUtility.deleteTransfer(_id);
    }

    if(res){

        var idx = findHistoryIndex(_id);
        if(idx !== -1)
            historyModel.remove(idx);

        showInformationMessage(qsTr("Transaction deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeleteTransaction(){

    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete the transaction. Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.");
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}

function openDetailsPage(){

    var idx = findHistoryIndex(accountsViewDashboardLastTransactions.currenthistorylistitem._id);
    var currenthistorydata = historyModel.get(idx);

    var ttype = parseInt(accountsViewDashboardLastTransactions.currenthistorylistitem.ttype);
    console.log('item.ttype('+ttype+') currentIndex('+accountsViewDashboardLastTransactions.currenthistoryindex+') idx('+idx+') currenthistorylistitem._id('+accountsViewDashboardLastTransactions.currenthistorylistitem._id+")");

    if(ttype === 1 || ttype === 2){ // expense or income

        stackView.push({item:expenseIncomeComponent,properties:{expenseincomedata:currenthistorydata}});

    }else if(ttype === 3){ // deposits

        stackView.push({item:depositComponent,properties:{depositdata:currenthistorydata}});

    }else if(ttype === 4){ // transfer

        stackView.push({item:transferComponent,properties:{transferdata:currenthistorydata}});
    }
}

function resetBalance(){

//    var balancedata = AccountUtility.getAccountBalances(account_id,ProfileUtility.currentProfile);
    var balancedata = history.balance;

//    accountsViewDashboardBalance.balancedata = balancedata;
}

function findHistoryIndex(check_id){

    for(var i = 0; i < historyModel.count; i++){

        var record_id = historyModel.get(i)._id;

        if(record_id === check_id){

            return i;
        }
    }

    return -1;
}
