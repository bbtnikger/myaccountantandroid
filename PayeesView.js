function func() {

}

function onAddPayeeDialogAccepted(){

    busyIndicator.running = true;

    var payeedata = payeeDialog.getPayeeDetails();
    var profile_id = ProfileUtility.currentProfile;
    payeedata.profile_id = profile_id;
    var payeedatanew = PayeeUtility.addPayee(payeedata);

    if(payeedatanew !== null){

        payeesViewList.model.append(payeedatanew);

        showInformationMessage(qsTr("Payee added successfully"));
    }

    busyIndicator.running = false;
}

function onUpdatePayeeDialogAccepted(){

    busyIndicator.running = true;

    var payeedetails = payeeDialog.getPayeeDetails();
    var _id = payeesViewList.currentpayeelistitem._id;
    var payeedatanew = PayeeUtility.updatePayee(_id,payeedetails);

    if(payeedatanew !== null){

        payeesViewList.model.set(payeesViewList.currentpayeeindex,payeedatanew);

        showInformationMessage(qsTr("Payee updated successfully"));
    }

    busyIndicator.running = false;
}

function startAddPayeeDialog(){

    payeeDialog.contentheight = stackView.height-100;
    payeeDialog.contentwidth = stackView.width-100;
    payeeDialog.payeedata = undefined;
    payeeDialog.resetDefaults();
    payeeDialog.title = qsTr("New Payee Dialog");
    payeeDialog.accepted.disconnect(onAddPayeeDialogAccepted);
    payeeDialog.accepted.disconnect(onUpdatePayeeDialogAccepted);
    payeeDialog.accepted.connect(onAddPayeeDialogAccepted);
    payeeDialog.open();
}

//var PayeeData = function(payeelistitem){

//    this.
//}

function startUpdatePayeeDialog(){

//    var payeedata = new PayeeData(payeesViewList.currentpayeelistitem);
//    payeeDialog.payeedata

    payeeDialog.contentheight = stackView.height-100;
    payeeDialog.contentwidth = stackView.width-100;
    payeeDialog.payeedata = payeesViewList.currentpayeelistitem
    payeeDialog.resetDefaults();
    payeeDialog.title = qsTr("Update Payee Dialog");
    payeeDialog.accepted.disconnect(onAddPayeeDialogAccepted);
    payeeDialog.accepted.disconnect(onUpdatePayeeDialogAccepted);
    payeeDialog.accepted.connect(onUpdatePayeeDialogAccepted);
    payeeDialog.open();
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = payeesViewList.currentpayeelistitem;
    var _id = recorddata._id;
    var res = PayeeUtility.deletePayee(_id);

    if(res){

        payeesViewList.model.remove(payeesViewList.currentpayeeindex);

        showInformationMessage(qsTr("Payee deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeletePayee(){

    var title_ = payeesViewList.currentpayeelistitem.name;
    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete ("+title_+'). Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.');
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}
