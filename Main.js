function func() {

}

function onAddProfileDialogAccepted(){

    busyIndicator.running = true;

    var profiledata = profileDialog.getProfileDetails();
    var profile_id = ProfileUtility.currentProfile;
    profiledata.profile_id = profile_id;
    var profiledatanew = ProfileUtility.addProfile(profiledata);

    if(profiledatanew !== null){

        profileList.model.append(profiledatanew);

        showInformationMessage(qsTr("Profile added successfully"));
    }

    busyIndicator.running = false;
}

function onUpdateProfileDialogAccepted(){

    busyIndicator.running = true;

    var profiledetails = profileDialog.getProfileDetails();
    var _id = profileList.currentprofilelistitem._id;
    var profiledatanew = ProfileUtility.updateProfile(_id,profiledetails);

    if(profiledatanew !== null){

        profileList.model.set(profileList.currentprofilelistitem,profiledatanew);

        showInformationMessage(qsTr("Profile updated successfully"));
    }

    busyIndicator.running = false;
}

function startAddProfileDialog(){

    profileDialog.profiledata = undefined;
    profileDialog.resetDefaults();
    profileDialog.title = qsTr("New Profile Dialog");
    profileDialog.accepted.disconnect(onAddProfileDialogAccepted);
    profileDialog.accepted.disconnect(onUpdateProfileDialogAccepted);
    profileDialog.accepted.connect(onAddProfileDialogAccepted);
    profileDialog.open();
}

//var ProfileData = function(profilelistitem){

//    this.
//}

function startUpdateProfileDialog(){

//    var profiledata = new ProfileData(profileList.currentprofilelistitem);
//    profileDialog.profiledata

    profileDialog.profiledata = profileList.currentprofilelistitem;
    profileDialog.resetDefaults();
    profileDialog.title = qsTr("Update Profile Dialog");
    profileDialog.accepted.disconnect(onAddProfileDialogAccepted);
    profileDialog.accepted.disconnect(onUpdateProfileDialogAccepted);
    profileDialog.accepted.connect(onUpdateProfileDialogAccepted);
    profileDialog.open();
}

function onDeleteCurrentRecordAccepted(){

    busyIndicator.running = true;

    var recorddata = profileList.currentprofilelistitem;
    var _id = recorddata._id;
    var res = ProfileUtility.deleteProfile(_id);

    if(res){

        profileList.model.remove(profileList.currentprofileindex);

        showInformationMessage(qsTr("Profile deleted successfully"));

    }

    busyIndicator.running = false;
}

function startDeleteProfile(){

    var title_ = profilesViewList.currentpr.name;
    messageDialog.title = qsTr("Question Dialog");
    messageDialog.icon = StandardIcon.Question;
    messageDialog.text = qsTr("You are about to delete ("+title_+'). Are you sure you want to delete this record? To delete the record click Yes, to cancel click No.');
    messageDialog.modality = Qt.ApplicationModal;
    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No;
    messageDialog.yes.disconnect(onDeleteCurrentRecordAccepted);
    messageDialog.yes.connect(onDeleteCurrentRecordAccepted);
    messageDialog.open();
}
