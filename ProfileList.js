function func() {

}

function resetProfiles(){

    clearProfiles();
    setupProfiles();
}

function clearProfiles(){

    listModel.clear();
}

function setupProfiles(){

    var list = ProfileUtility.getProfiles();
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        listModel.append(record_);
    }
}

function onCurrentIndexChanged(currentIndex){

    console.log('[Profile ListView]-Current Index('+currentIndex+')');

    currentprofileitem = listModel.get(currentIndex);
    currentprofile_id = currentprofileitem._id;

    ProfileUtility.currentProfile = currentprofile_id;
}
