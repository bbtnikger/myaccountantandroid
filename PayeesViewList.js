function func() {

}

function resetPayees(){

    columnLayout.visible = false;

    clearPayees();
    setupPayees();

    columnLayout.visible = true;
}

function clearPayees(){

    listModel.clear();
}

function setupPayees(){

    var list = PayeeUtility.getPayees(ProfileUtility.currentProfile);
    if(list === undefined)
        return;

    var len = list.length;
    for(var i = 0; i < len; i++){

        var record_ = list[i];
        listModel.append(record_);
    }
}
