import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import org.qtproject.example 1.0
import "HistoryView.js" as HistoryViewScript
import "dialogs_forms"


Item {
    id: root

    property variant currenthistorylistitem
//    property alias currenthistoryindex: tableView.currentRow
    property alias currenthistoryindex: listView.currentIndex
    property alias model: historyModel

    property variant history
    property real balanceval: 0.0
    property string balancestring

    property string typeExpenses: qsTr("Expense")
    property string typeIncome: qsTr("Income")
    property string typeTransfers: qsTr("Transfer")
    property string typeDeposit: qsTr("Deposit")

    function resetPage(){

        HistoryViewScript.resetHistory();
    }

    ListModel{
        id: historyModel
//        dynamicRoles: true

//        ListElement{
//            date: "text"
//        }
    }

    SortFilterProxyModel {
        id: proxyModel
        source: historyModel.count > 0 ? historyModel : null

        sortOrder: Qt.AscendingOrder
        sortCaseSensitivity: Qt.CaseInsensitive
//                sortRole: historyModel.count > 0 ? tableView.getColumn(tableView.sortIndicatorColumn).role : ""
        sortRole: historyModel.count > 0 ? "date" : ""

//                filterString: "*" + searchBox.text + "*"
//                filterSyntax: SortFilterProxyModel.Wildcard
//                filterCaseSensitivity: Qt.CaseInsensitive
    }

    Component{
        id: itemDelegate
        Item{
            width: listView.width
            height: 100

            Rectangle{
                anchors.fill: parent
                color: "#000000FF"
                border.color: "grey"
            }

            ColumnLayout{
                anchors.fill: parent
                anchors.margins: 6
                spacing: 3

                RowLayout{
                    Layout.preferredHeight: parent.height / 1.7
                    anchors.left: parent.left
                    anchors.right: parent.right

                    Label{
                        anchors.verticalCenter: parent.verticalCenter
                        text: accountname
                        Layout.fillWidth: true
                        font.pointSize: 15
                    }

                    Label{
                        anchors.bottom: parent.bottom
                        horizontalAlignment: Qt.AlignRight
                        text: type
                        font.pointSize: 11
                    }
                }

                Label{
                    text: datename + ', ' +  amounttext + ', ' + description
                    font.pointSize: 13
                }
            }

            MouseArea{
                anchors.fill: parent
                onPressAndHold: { listView.currentIndex = index; menu.popup(); }
                onClicked: { listView.currentIndex = index; listView.openDetailsPage(); }
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: listView.width-2; height: 100
            y: listView.currentItem.y
            x: 1
            color: "lightsteelblue";
        }
    }

    Menu{
        id: menu
        MenuItem{
            text: qsTr("Edit")
            onTriggered: HistoryViewScript.startEditTransaction()
        }
        MenuItem{
            text: qsTr("Delete")
            onTriggered: HistoryViewScript.startDeleteTransaction()
        }
    }

    ExpenseIncomeDialog{
        id: expenseIncomeDialog
        visible: false
    }

    TransferDialog{
        id: transferDialog
        visible: false

    }

    DepositDialog{
        id: depositDialog
        visible: false
    }

    SelectTransactionTypeDialog{
        id: selectTransationTypeDialog
        visible: false
        onAccepted: HistoryViewScript.onTransactionTypeSelected()
    }

    Component{
        id: expenseIncomeComponent
        ViewExpenseIncomeDetails{ }
    }

    Component{
        id: depositComponent
        ViewDepositDetails{ }
    }

    Component{
        id: transferComponent
        ViewTransferDetails{ }
    }

    Component {
        id: headerComponent

        Item {
            width: listView.width
            height: 40
            y: ListView.view.y-height

            Label{
                text: qsTr("History")
                font.pointSize: 13
                font.bold: true
                font.italic: true
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 12

            }
        }
    }

    ColumnLayout{
        anchors.fill: parent
        spacing: 3

        ListView{
            id: listView
            anchors.left: parent.left
            anchors.right: parent.right
            Layout.fillHeight: true
            delegate: itemDelegate
            highlight: highlight
            header: headerComponent
            highlightFollowsCurrentItem: false
            focus: true
            onCountChanged: {

//                currenthistorylistitem = proxyModel.get(currentRow);
                currenthistorylistitem = proxyModel.get(currentIndex);
            }

            onCurrentIndexChanged: {

                currenthistorylistitem = proxyModel.get(currentIndex);
//                currenthistorylistitem = historyModel.get(currentRow);
            }

            function startEditProcess(){

                HistoryViewScript.startEditTransaction()
            }

            function startDeleteProcess(){

                HistoryViewScript.startDeleteTransaction()
            }

            function startOpenProcess(){

//                root.openTransaction();
            }

            function openDetailsPage(){

                HistoryViewScript.openDetailsPage();
            }
        }

        ToolBar {
            id: toolbar
            anchors.left: parent.left
            anchors.right: parent.right
            RowLayout {
                anchors.fill: parent
                ToolButton {
                    tooltip: qsTr("Start Transaction")
                    iconSource: "qrc:/images/ic_add_64x64.png"
                    onClicked: HistoryViewScript.startAddTransaction()
                }
            }
        }
    }

    Component.onCompleted: HistoryViewScript.resetHistory()
}

